package bean;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class DanhMucBean {

        private String kind62;
        private JPanel jpn62;
        private JLabel jlb62;

        public DanhMucBean() {
        }

        public DanhMucBean(String kind62, JPanel jpn62, JLabel jlb62) {
                this.kind62 = kind62;
                this.jpn62 = jpn62;
                this.jlb62 = jlb62;
        }

        public String getKind() {
                return kind62;
        }

        public void setKind(String kind62) {
                this.kind62 = kind62;
        }

        public JPanel getJpn() {
                return jpn62;
        }

        public void setJpn(JPanel jpn62) {
                this.jpn62 = jpn62;
        }

        public JLabel getJlb() {
                return jlb62;
        }

        public void setJlb(JLabel jlb62) {
                this.jlb62 = jlb62;
        }
}
