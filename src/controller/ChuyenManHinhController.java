package controller;

import bean.DanhMucBean;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JPanel;
import view.BanHang.BanHangJPanel;
import view.BanHang.BanHangMenuJPanel;
import view.HoaDon.HoaDonJPanel;
import view.JThongKe.BieuDoJPanel;
import view.JThongKe.ThongKeJPanel;
import view.JThongKe.ThongKeMenuJpanel;
import view.KhachHang.KhachHangJPanel;
import view.NhanVien.NhanVienJPanel;
import view.SanPham.SanPhamJPanel;

public class ChuyenManHinhController {

        private JPanel root62;
        private String kind62Selected62 = "";

        private List<DanhMucBean> listItem62 = null;

        public ChuyenManHinhController(JPanel jPanelRoot62) {
                this.root62 = jPanelRoot62;
        }

        public void setView(JPanel jPanelItem62, JLabel jLabelItem62) {
                kind62Selected62 = "BanHang";
                jPanelItem62.setBackground(new Color(51, 202, 187));
                jLabelItem62.setBackground(new Color(51, 202, 187));

                root62.removeAll();
                root62.setLayout(new BorderLayout());
                root62.add(new BanHangJPanel());
                root62.validate();
                root62.repaint();
        }

        public void setViewMenu(JPanel jPanelItem62, JLabel jLabelItem62) {
                kind62Selected62 = "BanHangMenu";

                root62.removeAll();
                root62.setLayout(new BorderLayout());
                root62.add(new BanHangMenuJPanel());
                root62.validate();
                root62.repaint();
        }
        
        public void setViewMenuChart(JPanel jPanelItem62, JLabel jLabelItem62) {
                kind62Selected62 = "ThongKe";

                root62.removeAll();
                root62.setLayout(new BorderLayout());
                root62.add(new ThongKeJPanel());
                root62.validate();
                root62.repaint();
        }

        public void setEvent(List<DanhMucBean> listItem62) {
                this.listItem62 = listItem62;
                for (DanhMucBean item62 : listItem62) {
                        item62.getJlb().addMouseListener(new LabelEvent(item62.getKind(), item62.getJpn(), item62.getJlb()));
                }
        }

        class LabelEvent implements MouseListener {

                private JPanel node62;
                private String kind62;
                private JPanel jPanelItem62;
                private JLabel jLabelItem62;

                public LabelEvent(String kind62, JPanel jPanelItem62, JLabel jLabelItem62) {
                        this.kind62 = kind62;
                        this.jPanelItem62 = jPanelItem62;
                        this.jLabelItem62 = jLabelItem62;
                }

                @Override
                public void mouseClicked(MouseEvent e) {
                        switch (kind62) {
                                case "BanHang":
                                        node62 = new BanHangJPanel();
                                        break;
                                case "BanHangMenu":
                                        node62 = new BanHangMenuJPanel();
                                        break;
                                case "HoaDon":
                                        node62 = new HoaDonJPanel();
                                        break;
                                case "SanPham":
                                        node62 = new SanPhamJPanel();
                                        break;
                                case "NhanVien":
                                        node62 = new NhanVienJPanel();
                                        break;
                                case "KhachHang":
                                        node62 = new KhachHangJPanel();
                                        break;
                                case "ThongKeMenu":
                                        node62 = new ThongKeMenuJpanel();
                                        break;
                                case "ThongKe":
                                        node62 = new ThongKeJPanel();
                                        break;
                                case "ViewChart":
                                        node62 = new BieuDoJPanel();
                                        break;
                                // more
                                default:
                                        node62 = new BanHangJPanel();
                                        break;
                        }
                        root62.removeAll();
                        root62.setLayout(new BorderLayout());
                        root62.add(node62);
                        root62.validate();
                        root62.repaint();
                        setChangeBackground(kind62);
                }

                @Override
                public void mousePressed(MouseEvent e) {
                        kind62Selected62 = kind62;
                        jPanelItem62.setBackground(new Color(51, 202, 187));
                        jLabelItem62.setBackground(new Color(51, 202, 187));
                }

                @Override
                public void mouseReleased(MouseEvent e) {

                }

                @Override
                public void mouseEntered(MouseEvent e) {
                        if (!jPanelItem62.getBackground().equals(new Color(51, 202, 187)) && !jLabelItem62.getBackground().equals(new Color(51, 202, 187))) {
                                jPanelItem62.setBackground(new Color(72, 88, 107));
                                jLabelItem62.setBackground(new Color(72, 88, 107));
                        }
                }

                @Override
                public void mouseExited(MouseEvent e) {
                        if (!kind62Selected62.equalsIgnoreCase(kind62)) {
                                jPanelItem62.setBackground(new Color(255, 153, 0));
                                jLabelItem62.setBackground(new Color(255, 153, 0));
                        }
                }

        }

        private void setChangeBackground(String kind62) {
                for (DanhMucBean item62 : listItem62) {
                        if (item62.getKind().equalsIgnoreCase(kind62)) {
                                item62.getJpn().setBackground(new Color(51, 202, 187));
                                item62.getJlb().setBackground(new Color(51, 202, 187));
                        } else {
                                item62.getJpn().setBackground(new Color(255, 153, 0));
                                item62.getJlb().setBackground(new Color(255, 153, 0));
                        }
                }
        }

}
