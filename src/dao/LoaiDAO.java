package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.LoaiSP;

public class LoaiDAO {

        public ArrayList<LoaiSP> getDanhSachLoai() {
                try {
                        String sql = "SELECT * FROM Loai";
                        PreparedStatement pre = MyConnect.getJDBCConnection().prepareStatement(sql);
                        ResultSet rs32 = pre.executeQuery();
                        ArrayList<LoaiSP> dsl32 = new ArrayList<>();
                        while (rs32.next()) {
                                LoaiSP loai32 = new LoaiSP();
                                loai32.setMaLoai(rs32.getInt(1));
                                loai32.setTenLoai(rs32.getString(2));
                                dsl32.add(loai32);
                        }
                        return dsl32;
                } catch (SQLException e) {
                }
                return null;
        }

        public boolean themLoai(LoaiSP loai32) {
                try {
                        String sql = "insert into loai(TenLoai) "
                                + "values ("
                                + "N'" + loai32.getTenLoai() + "')";
                        Statement st = MyConnect.getJDBCConnection().createStatement();
                        int x = st.executeUpdate(sql);
                        return x > 0 ? true : false;
                } catch (SQLException ex) {
                        Logger.getLogger(LoaiDAO.class.getName()).log(Level.SEVERE, null, ex);
                }

                return false;
        }

        public boolean xoaLoai(int maLoai32) {
                try {
                        String sql = "DELETE FROM Loai WHERE MaLoai=" + maLoai32;
                        Statement st = MyConnect.getJDBCConnection().createStatement();
                        int x = st.executeUpdate(sql);
                        return x > 0 ? true : false;
                } catch (SQLException e) {
                }
                return false;
        }

        public boolean suaLoai(int maLoai32, String ten32) {
                try {
                        String sql = "UPDATE Loai SET TenLoai=N'" + ten32 + "' WHERE MaLoai=" + maLoai32;
                        Statement st = MyConnect.getJDBCConnection().createStatement();
                        int x = st.executeUpdate(sql);
                        return x > 0 ? true : false;
                } catch (SQLException e) {
                }
                return false;
        }

}
