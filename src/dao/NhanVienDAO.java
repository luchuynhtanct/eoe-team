package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import model.NhanVien;

public class NhanVienDAO {

        public ArrayList<NhanVien> getDanhSachNhanVien() {
                try {
                        String sql = "SELECT * FROM NhanVien";
                        PreparedStatement pre = MyConnect.getJDBCConnection().prepareStatement(sql);
                        ResultSet rs30 = pre.executeQuery();
                        ArrayList<NhanVien> dssv30 = new ArrayList<>();
                        while (rs30.next()) {
                                NhanVien nv30 = new NhanVien();

                                nv30.setMaNV(rs30.getInt(1));
                                nv30.setHo(rs30.getString(2));
                                nv30.setTen(rs30.getString(3));
                                nv30.setGioiTinh(rs30.getString(4));
                                nv30.setChucVu(rs30.getString(5));

                                dssv30.add(nv30);
                        }
                        return dssv30;
                } catch (SQLException e) {
                }

                return null;
        }

        public NhanVien getNhanVien(int maNV30) {
                NhanVien nv30 = null;
                try {
                        String sql = "SELECT * FROM NhanVien WHERE MaNV=?";
                        PreparedStatement pre = MyConnect.getJDBCConnection().prepareStatement(sql);
                        pre.setInt(0, maNV30);
                        ResultSet rs30 = pre.executeQuery();
                        while (rs30.next()) {
                                nv30 = new NhanVien();
                                nv30.setMaNV(rs30.getInt(1));
                                nv30.setHo(rs30.getString(2));
                                nv30.setTen(rs30.getString(3));
                                nv30.setGioiTinh(rs30.getString(4));
                                nv30.setChucVu(rs30.getString(5));
                        }
                } catch (SQLException e) {
                        return null;
                }

                return nv30;
        }

        public boolean updateNhanVien(NhanVien nv30) {
                boolean result30 = false;
                try {
                        String sql = "UPDATE nhanvien SET Ho=?, Ten=?, GioiTinh=?, ChucVu=? WHERE MaNV=?";
                        PreparedStatement pre = MyConnect.getJDBCConnection().prepareStatement(sql);
                        pre.setString(1, nv30.getHo());
                        pre.setString(2, nv30.getTen());
                        pre.setString(3, nv30.getGioiTinh());
                        pre.setString(4, nv30.getChucVu());
                        pre.setInt(5, nv30.getMaNV());
                        result30 = pre.executeUpdate() > 0;
                } catch (SQLException ex) {
                        return false;
                }
                return result30;
        }

        public boolean deleteNhanVien(int maNV30) {
                boolean result30 = false;
                try {
                        String sql = "DELETE FROM nhanvien WHERE MaNV=?";
                        PreparedStatement pre = MyConnect.getJDBCConnection().prepareStatement(sql);
                        pre.setInt(1, maNV30);
                        result30 = pre.executeUpdate() > 0;
                } catch (SQLException ex) {
                        return false;
                }
                return result30;
        }

        public boolean themNhanVien(NhanVien nv30) {
                boolean result30 = false;
                try {
                        String sql = "INSERT INTO NhanVien(Ho, Ten, GioiTinh, ChucVu) "
                                + "VALUES(?, ?, ?, ?)";
                        PreparedStatement pre = MyConnect.getJDBCConnection().prepareStatement(sql);
                        pre.setString(1, nv30.getHo());
                        pre.setString(2, nv30.getTen());
                        pre.setString(3, nv30.getGioiTinh());
                        pre.setString(4, nv30.getChucVu());
                        result30 = pre.executeUpdate() > 0;
                } catch (SQLException ex) {
                        return false;
                }
                return result30;
        }
}
