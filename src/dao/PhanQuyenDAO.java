package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import model.PhanQuyen;

public class PhanQuyenDAO {

        public ArrayList<PhanQuyen> getListQuyen() {
                try {
                        String sql = "SELECT * FROM PhanQuyen";
                        Statement st50 = MyConnect.getJDBCConnection().createStatement();
                        ResultSet rs50 = st50.executeQuery(sql);
                        ArrayList<PhanQuyen> dspq50 = new ArrayList<>();
                        while (rs50.next()) {
                                PhanQuyen phanQuyen50 = new PhanQuyen();
                                phanQuyen50.setQuyen(rs50.getString(1));
                                phanQuyen50.setQlSanPham(rs50.getInt(2));
                                phanQuyen50.setQlNhanVien(rs50.getInt(3));
                                phanQuyen50.setQlKhachHang(rs50.getInt(4));
                                phanQuyen50.setThongKe(rs50.getInt(5));
                                dspq50.add(phanQuyen50);
                        }
                        return dspq50;
                } catch (Exception e) {
                }
                return null;
        }

        public PhanQuyen getQuyen(String quyen50) {
                try {
                        String sql = "SELECT * FROM PhanQuyen WHERE quyen=N'" + quyen50 + "'";
                        Statement st50 = MyConnect.getJDBCConnection().createStatement();
                        ResultSet rs50 = st50.executeQuery(sql);
                        if (rs50.next()) {
                                PhanQuyen phanQuyen50 = new PhanQuyen();
                                phanQuyen50.setQuyen(quyen50);
                                phanQuyen50.setQlSanPham(rs50.getInt(2));
                                phanQuyen50.setQlNhanVien(rs50.getInt(3));
                                phanQuyen50.setQlKhachHang(rs50.getInt(4));
                                phanQuyen50.setThongKe(rs50.getInt(5));
                                return phanQuyen50;
                        }
                } catch (Exception e) {
                }
                return null;
        }

        public boolean suaQuyen(PhanQuyen phanQuyen50) {
                try {
                        String sql = "UPDATE phanquyen SET QLSanPham=?,QLNhanVien=?,QLKhachHang=?,ThongKe=? WHERE Quyen=?";
                        PreparedStatement pre = MyConnect.getJDBCConnection().prepareStatement(sql);
                        pre.setInt(1, phanQuyen50.getQlSanPham());
                        pre.setInt(2, phanQuyen50.getQlNhanVien());
                        pre.setInt(3, phanQuyen50.getQlKhachHang());
                        pre.setInt(4, phanQuyen50.getThongKe());
                        pre.setString(5, phanQuyen50.getQuyen());
                        return pre.executeUpdate() > 0;
                } catch (Exception e) {
                }
                return false;
        }

        public boolean themQuyen(PhanQuyen phanQuyen50) {
                try {
                        String sql = "INSERT INTO phanquyen VALUES (?,?,?,?,?)";
                        PreparedStatement pre = MyConnect.getJDBCConnection().prepareStatement(sql);
                        pre.setString(1, phanQuyen50.getQuyen());
                        pre.setInt(2, phanQuyen50.getQlSanPham());
                        pre.setInt(3, phanQuyen50.getQlNhanVien());
                        pre.setInt(4, phanQuyen50.getQlKhachHang());
                        pre.setInt(5, phanQuyen50.getThongKe());
                        return pre.executeUpdate() > 0;
                } catch (Exception e) {
                }
                return false;
        }

        public boolean xoaQuyen(String phanQuyen50) {
                try {
                        String sql1 = "UPDATE TaiKhoan SET Quyen='Default' WHERE Quyen=N'" + phanQuyen50 + "'";
                        Statement st501 = MyConnect.getJDBCConnection().createStatement();
                        st501.executeUpdate(sql1);
                        String sql = "DELETE FROM PhanQuyen WHERE Quyen=N'" + phanQuyen50 + "'";
                        Statement st50 = MyConnect.getJDBCConnection().createStatement();
                        return st50.executeUpdate(sql) > 0;
                } catch (Exception e) {
                        e.printStackTrace();
                }
                return false;
        }
}
