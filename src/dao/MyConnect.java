package dao;

import custom.MyDialog;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MyConnect {

        private static String severName62;
        private static String userName62;
        private static String password62;

        public static Connection getJDBCConnection() throws SQLException {
                docFileText();
                final String url62 = "jdbc:sqlserver://" + severName62 + ":1433;databaseName = EOE";

                try {
                        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
                        return DriverManager.getConnection(url62, userName62, password62);
                } catch (ClassNotFoundException | SQLException e) {
                        new MyDialog("Không kết nối được tới CSDL!", MyDialog.ERROR_DIALOG);
                }

                return null;
        }

        private static void docFileText() {
                // Xử lý đọc file để lấy ra 4 tham số
                severName62 = "";
                userName62 = "";
                password62 = "";

                try {
                        FileInputStream fis62 = new FileInputStream("src/ConnectVariable.txt");
                        InputStreamReader isr62 = new InputStreamReader(fis62);
                        BufferedReader br62 = new BufferedReader(isr62);

                        severName62 = br62.readLine();
                        userName62 = br62.readLine();
                        password62 = br62.readLine();

                        if (password62 == null) {
                                password62 = "";
                        }

                } catch (Exception e) {
                }
        }
}
