package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import model.CTHoaDon;

public class CTHoaDonDAO {

        public ArrayList<CTHoaDon> getListCTHoaDon() {
                ArrayList<CTHoaDon> dscthd3636 = new ArrayList<>();
                try {
                        String sql36 = "SELECT * FROM cthoadon";
                        Statement stmt36 = MyConnect.getJDBCConnection().createStatement();
                        ResultSet rs36 = stmt36.executeQuery(sql36);
                        while (rs36.next()) {
                                CTHoaDon cthd36 = new CTHoaDon();
                                cthd36.setMaHD(rs36.getInt(1));
                                cthd36.setMaSP(rs36.getInt(2));
                                cthd36.setSoLuong(rs36.getInt(3));
                                cthd36.setDonGia(rs36.getInt(4));
                                cthd36.setThanhTien(rs36.getInt(5));
                                dscthd3636.add(cthd36);
                        }
                } catch (SQLException ex) {
                }
                return dscthd3636;
        }

        public ArrayList<CTHoaDon> getListCTHoaDonTheoMaHD(int maHD36) {
                ArrayList<CTHoaDon> dscthd3636 = new ArrayList<>();
                try {
                        String sql36 = "SELECT * FROM cthoadon WHERE MaHD=" + maHD36;
                        Statement stmt36 = MyConnect.getJDBCConnection().createStatement();
                        ResultSet rs36 = stmt36.executeQuery(sql36);
                        while (rs36.next()) {
                                CTHoaDon cthd36 = new CTHoaDon();
                                cthd36.setMaHD(rs36.getInt(1));
                                cthd36.setMaSP(rs36.getInt(2));
                                cthd36.setSoLuong(rs36.getInt(3));
                                cthd36.setDonGia(rs36.getInt(4));
                                cthd36.setThanhTien(rs36.getInt(5));
                                dscthd3636.add(cthd36);
                        }
                } catch (SQLException ex) {
                        return null;
                }
                return dscthd3636;
        }

        public ArrayList<CTHoaDon> getListCTHoaDonTheoMaSP(int maSP36) {
                ArrayList<CTHoaDon> dscthd3636 = new ArrayList<>();
                try {
                        String sql36 = "SELECT * FROM cthoadon WHERE MaSP=" + maSP36;
                        Statement stmt36 = MyConnect.getJDBCConnection().createStatement();
                        ResultSet rs36 = stmt36.executeQuery(sql36);
                        while (rs36.next()) {
                                CTHoaDon cthd36 = new CTHoaDon();
                                cthd36.setMaHD(rs36.getInt(1));
                                cthd36.setMaSP(rs36.getInt(2));
                                cthd36.setSoLuong(rs36.getInt(3));
                                cthd36.setDonGia(rs36.getInt(4));
                                cthd36.setThanhTien(rs36.getInt(5));
                                dscthd3636.add(cthd36);
                        }
                } catch (SQLException ex) {
                        return null;
                }
                return dscthd3636;
        }

        public boolean addCTHoaDon(CTHoaDon cthd36) {
                boolean result36 = false;
                try {
                        String sql36 = "INSERT INTO cthoadon VALUES(?,?,?,?,?)";
                        PreparedStatement prep36 = MyConnect.getJDBCConnection().prepareStatement(sql36);
                        prep36.setInt(1, cthd36.getMaHD());
                        prep36.setInt(2, cthd36.getMaSP());
                        prep36.setInt(3, cthd36.getSoLuong());
                        prep36.setInt(4, cthd36.getDonGia());
                        prep36.setInt(5, cthd36.getThanhTien());
                        result36 = prep36.executeUpdate() > 0;
                } catch (SQLException ex) {
                        ex.printStackTrace();
                        return false;
                }
                return result36;
        }

        public boolean deleteCTHoaDon(int maHD36, int maSP36) {
                boolean result36 = false;
                try {
                        String sql36 = "DELETE FROM cthoadon WHERE MaHD=" + maHD36 + " AND MaSP=" + maSP36;
                        Statement stmt36 = MyConnect.getJDBCConnection().createStatement();
                        result36 = stmt36.executeUpdate(sql36) > 0;
                } catch (SQLException ex) {
                        return false;
                }
                return result36;
        }

        public boolean deleteCTHoaDon(int maHD36) {
                boolean result36 = false;
                try {
                        String sql36 = "DELETE FROM cthoadon WHERE MaHD=" + maHD36;
                        Statement stmt36 = MyConnect.getJDBCConnection().createStatement();
                        result36 = stmt36.executeUpdate(sql36) > 0;
                } catch (SQLException ex) {
                        return false;
                }
                return result36;
        }

        public boolean updateCTHoaDon(int maHD36, int maSP36, CTHoaDon cthd36) {
                boolean result36 = false;
                try {
                        String sql36 = "UPDATE cthoadon SET MaHD=?, MaSP=?, SoLuong=?, DonGia=? ThanhTien=? "
                                + "WHERE MaHD=? AND MaSP=?";
                        PreparedStatement prep36 = MyConnect.getJDBCConnection().prepareStatement(sql36);
                        prep36.setInt(1, cthd36.getMaHD());
                        prep36.setInt(2, cthd36.getMaSP());
                        prep36.setInt(3, cthd36.getSoLuong());
                        prep36.setInt(4, cthd36.getDonGia());
                        prep36.setInt(5, cthd36.getThanhTien());
                        prep36.setInt(6, maHD36);
                        prep36.setInt(7, maSP36);
                        result36 = prep36.executeUpdate() > 0;
                } catch (SQLException ex) {
                        return false;
                }
                return result36;
        }
}
