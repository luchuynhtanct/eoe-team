package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import model.TaiKhoan;

public class DangNhapDAO {

        public TaiKhoan dangNhap(TaiKhoan tk62) {
                try {
                        String sql = "SELECT * FROM taikhoan WHERE TenDangNhap=? AND MatKhau=? AND TrangThai=1";
                        PreparedStatement pre = MyConnect.getJDBCConnection().prepareStatement(sql);
                        pre.setString(1, tk62.getTenDangNhap());
                        pre.setString(2, tk62.getMatKhau());
                        ResultSet rs62 = pre.executeQuery();
                        TaiKhoan tkLogin62 = null;
                        if (rs62.next()) {
                                tkLogin62 = tk62;
                                tkLogin62.setMaNhanVien(rs62.getInt("MaNV"));
                                tkLogin62.setQuyen(rs62.getString("Quyen"));
                        }
                        return tkLogin62;
                } catch (SQLException e) {
                        e.printStackTrace();
                }
                return tk62;
        }
}
