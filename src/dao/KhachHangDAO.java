package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import model.KhachHang;

public class KhachHangDAO {

        public ArrayList<KhachHang> getListKhachHang() {
                try {
                        String sql = "SELECT * FROM KhachHang WHERE TinhTrang=1";
                        PreparedStatement pre = MyConnect.getJDBCConnection().prepareStatement(sql);
                        ResultSet rs62 = pre.executeQuery();
                        ArrayList<KhachHang> dskh62 = new ArrayList<>();
                        while (rs62.next()) {
                                KhachHang kh62 = new KhachHang();
                                kh62.setMaKH(rs62.getInt(1));
                                kh62.setHo(rs62.getString(2));
                                kh62.setTen(rs62.getString(3));
                                kh62.setGioiTinh(rs62.getString(4));
                                kh62.setTongChiTieu(rs62.getInt(5));
                                dskh62.add(kh62);
                        }
                        return dskh62;
                } catch (SQLException ex) {
                }
                return null;
        }

        public KhachHang getKhachHang(int maKH62) {
                KhachHang kh62 = null;
                try {
                        String sql = "SELECT * FROM khachhang WHERE MaKH=? AND TinhTrang=1";
                        PreparedStatement prep = MyConnect.getJDBCConnection().prepareStatement(sql);
                        prep.setInt(1, maKH62);
                        ResultSet rs62 = prep.executeQuery();
                        while (rs62.next()) {
                                kh62 = new KhachHang();
                                kh62.setMaKH(rs62.getInt(1));
                                kh62.setHo(rs62.getString(2));
                                kh62.setTen(rs62.getString(3));
                                kh62.setGioiTinh(rs62.getString(4));
                                kh62.setTongChiTieu(rs62.getInt(5));
                        }
                } catch (SQLException ex) {
                        return null;
                }
                return kh62;
        }

        public boolean addKhachHang(KhachHang kh62) {
                boolean result62 = false;
                try {
                        String sql = "INSERT INTO khachhang VALUES(?,?,?,?,1)";
                        PreparedStatement prep = MyConnect.getJDBCConnection().prepareStatement(sql);
                        prep.setString(1, kh62.getHo());
                        prep.setString(2, kh62.getTen());
                        prep.setString(3, kh62.getGioiTinh());
                        prep.setInt(4, kh62.getTongChiTieu());
                        result62 = prep.executeUpdate() > 0;
                } catch (SQLException ex) {
                        ex.printStackTrace();
                        return false;
                }
                return result62;
        }

        public boolean deleteKhachHang(int maKH62) {
                boolean result62 = false;
                try {
                        String sql = "UPDATE khachhang SET TinhTrang=0 WHERE MaKH=?";
                        PreparedStatement prep = MyConnect.getJDBCConnection().prepareStatement(sql);
                        prep.setInt(1, maKH62);
                        result62 = prep.executeUpdate() > 0;
                } catch (SQLException ex) {
                        return false;
                }
                return result62;
        }

        public boolean updateKhachHang(int maKH62, KhachHang kh62) {
                boolean result62 = false;
                try {
                        String sql = "UPDATE khachhang SET Ho=?, Ten=?, GioiTinh=? WHERE MaKH=?";
                        PreparedStatement prep = MyConnect.getJDBCConnection().prepareStatement(sql);
                        prep.setString(1, kh62.getHo());
                        prep.setString(2, kh62.getTen());
                        prep.setString(3, kh62.getGioiTinh());
                        prep.setInt(4, maKH62);
                        result62 = prep.executeUpdate() > 0;
                } catch (SQLException ex) {
                        return false;
                }
                return result62;
        }

        public boolean updateTongChiTieu(int maKH62, int tongChiTieu62) {
                boolean result62 = false;
                try {
                        String sql = "UPDATE khachhang SET TongChiTieu=" + tongChiTieu62 + " WHERE MaKH=" + maKH62;
                        Statement stmt = MyConnect.getJDBCConnection().createStatement();
                        result62 = stmt.executeUpdate(sql) > 0;
                } catch (SQLException ex) {
                        return false;
                }
                return result62;
        }
}
