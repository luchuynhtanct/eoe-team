package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import model.SanPham;
import model.ThongKe;
import bus.SanPhamBUS;

public class ThongKeDAO {

        public ThongKe getThongKe(int nam) {
                ThongKe thongKe62 = new ThongKe();
                int[] tongThuQuy62 = new int[4];
                thongKe62.setSoLuongSP(getTongSoLuongSP());
                thongKe62.setSoLuongKH(getSoLuongKhachHang());
                thongKe62.setSoLuongNV(getSoLuongNhanVien());
                tongThuQuy62[0] = getTongThuQuy(nam, 1);
                tongThuQuy62[1] = getTongThuQuy(nam, 2);
                tongThuQuy62[2] = getTongThuQuy(nam, 3);
                tongThuQuy62[3] = getTongThuQuy(nam, 4);
                thongKe62.setTongThuQuy(tongThuQuy62);
                thongKe62.setTopSanPhamBanChay(getTopBanChay());
                return thongKe62;
        }

        private ArrayList<SanPham> getTopBanChay() {
                try {
                        String sql = "SELECT TOP 5 MaSP, DaBan FROM ("
                                + "SELECT MaSP, SUM(SoLuong) AS DaBan FROM "
                                + "cthoadon GROUP BY MaSP"
                                + ") temp "
                                + "ORDER BY DaBan "
                                + "DESC";
                        Statement st = MyConnect.getJDBCConnection().createStatement();
                        ResultSet rs = st.executeQuery(sql);
                        ArrayList<SanPham> dssp = new ArrayList<>();
                        SanPhamBUS spBUS62 = new SanPhamBUS();
                        while (rs.next()) {
                                SanPham sp = new SanPham();
                                sp.setMaSP(rs.getInt(1));
                                sp.setSoLuong(rs.getInt(2));
                                sp.setTenSP(spBUS62.getTenSP(sp.getMaSP()));
                                dssp.add(sp);
                        }
                        return dssp;
                } catch (Exception e) {
                }
                return null;
        }

        private int getTongSoLuongSP() {
                try {
                        Statement stmt = MyConnect.getJDBCConnection().createStatement();
                        ResultSet rs = stmt.executeQuery("SELECT COUNT(*) FROM sanpham");
                        while (rs.next()) {
                                return rs.getInt(1);
                        }
                } catch (SQLException ex) {
                        return -1;
                }
                return 0;
        }

        private String[] getDateString(int nam, int quy) {
                int namBatDau62 = nam;
                int namKetThuc62 = nam;
                String thangBatDau62 = "01"; //kiểu String do có số 0 ở phía trước
                String thangKetThuc62 = "04"; //kiểu String do có số 0 ở phía trước
                String[] kq = new String[2];
                switch (quy) {
                        case 1:
                                thangBatDau62 = "01";
                                thangKetThuc62 = "03";
                                break;
                        case 2:
                                thangBatDau62 = "04";
                                thangKetThuc62 = "06";
                                break;
                        case 3:
                                thangBatDau62 = "07";
                                thangKetThuc62 = "09";
                                break;
                        case 4:
                                thangBatDau62 = "10";
                                thangKetThuc62 = "12";
                                break;
                }
                String strBatDau62 = Integer.toString(namBatDau62) + thangBatDau62 + "01";
                String strKetThuc62 = Integer.toString(namKetThuc62) + thangKetThuc62 + "28";
                kq[0] = strBatDau62;
                kq[1] = strKetThuc62;
                return kq;
        }

        private int getTongThuQuy(int nam, int quy) {
                String[] dateString62 = getDateString(nam, quy);
                try {
                        PreparedStatement prep = MyConnect.getJDBCConnection().prepareStatement("SELECT SUM(TongTien) FROM hoadon "
                                + "WHERE NgayLap >= ? AND NgayLap < ?");
                        prep.setString(1, dateString62[0]);
                        prep.setString(2, dateString62[1]);
                        ResultSet rs = prep.executeQuery();
                        while (rs.next()) {
                                return rs.getInt(1);
                        }
                } catch (SQLException ex) {
                        return -1;
                }
                return 0;
        }

        private int getSoLuongNhanVien() {
                try {
                        Statement stmt = MyConnect.getJDBCConnection().createStatement();
                        ResultSet rs = stmt.executeQuery("SELECT COUNT(*) FROM nhanvien");
                        while (rs.next()) {
                                return rs.getInt(1);
                        }
                } catch (SQLException ex) {
                        return -1;
                }
                return 0;
        }

        private int getSoLuongKhachHang() {
                try {
                        Statement stmt = MyConnect.getJDBCConnection().createStatement();
                        ResultSet rs = stmt.executeQuery("SELECT COUNT(*) FROM khachhang WHERE TinhTrang=1");
                        while (rs.next()) {
                                return rs.getInt(1);
                        }
                } catch (SQLException ex) {
                        return -1;
                }
                return 0;
        }

        public double getDoanhThuThang(int thang, int nam) {
                try {
           
                        String thangBD62 = nam + "-" + thang + "-01";
                        String thangKT62 = nam + "-" + (thang + 1) + "-01";
                        if (thang == 12) {
                                thangKT62 = thangBD62;
                        }
                        String sql = "SELECT SUM(TongTien) FROM HoaDon WHERE NgayLap BETWEEN CAST(? AS DATE) AND CAST(? AS DATE)";
                        PreparedStatement pre = MyConnect.getJDBCConnection().prepareStatement(sql);
                        pre.setString(1, thangBD62);
                        pre.setString(2, thangKT62);
                        ResultSet rs = pre.executeQuery();
                        while (rs.next()) {
                                return Double.parseDouble(rs.getInt(1) + "");
                        }
                } catch (Exception e) {
                        e.printStackTrace();
                }
                return nam;
        }
}
