package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import model.SanPham;

public class SanPhamDAO {

        public ArrayList<SanPham> getListSanPham() {
                try {
                        String sql = "SELECT * FROM SanPham";
                        PreparedStatement pre = MyConnect.getJDBCConnection().prepareStatement(sql);
                        ResultSet rs32 = pre.executeQuery();
                        ArrayList<SanPham> dssp32 = new ArrayList<>();
                        while (rs32.next()) {
                                SanPham sp32 = new SanPham();

                                sp32.setMaSP(rs32.getInt(1));
                                sp32.setTenSP(rs32.getString(2));
                                sp32.setMaLoai(rs32.getInt(3));
                                sp32.setSoLuong(rs32.getInt(4));
                                sp32.setDonViTinh(rs32.getString(5));
                                sp32.setHinhAnh(rs32.getString(6));
                                sp32.setDonGia(rs32.getInt(7));

                                dssp32.add(sp32);
                        }
                        return dssp32;
                } catch (SQLException e) {
                }

                return null;
        }

        public SanPham getSanPham(int ma32) {
                try {
                        String sql = "SELECT *FROM SanPham WHERE MaSP=?";
                        PreparedStatement pre = MyConnect.getJDBCConnection().prepareStatement(sql);
                        pre.setInt(1, ma32);
                        ResultSet rs32 = pre.executeQuery();
                        if (rs32.next()) {
                                SanPham sp32 = new SanPham();

                                sp32.setMaSP(rs32.getInt(1));
                                sp32.setTenSP(rs32.getString(2));
                                sp32.setMaLoai(rs32.getInt(3));
                                sp32.setSoLuong(rs32.getInt(4));
                                sp32.setDonViTinh(rs32.getString(5));
                                sp32.setHinhAnh(rs32.getString(6));
                                sp32.setDonGia(rs32.getInt(7));

                                return sp32;
                        }
                } catch (SQLException e) {
                }

                return null;
        }

        public ArrayList<SanPham> getSanPhamTheoLoai(int maLoai32) {
                try {
                        String sql = "SELECT * FROM SanPham WHERE MaLoai=?";
                        PreparedStatement pre = MyConnect.getJDBCConnection().prepareStatement(sql);
                        pre.setInt(1, maLoai32);
                        ResultSet rs32 = pre.executeQuery();
                        ArrayList<SanPham> dssp32 = new ArrayList<>();
                        while (rs32.next()) {
                                SanPham sp32 = new SanPham();

                                sp32.setMaSP(rs32.getInt(1));
                                sp32.setTenSP(rs32.getString(2));
                                sp32.setMaLoai(rs32.getInt(3));
                                sp32.setSoLuong(rs32.getInt(4));
                                sp32.setDonViTinh(rs32.getString(5));
                                sp32.setHinhAnh(rs32.getString(6));
                                sp32.setDonGia(rs32.getInt(7));

                                dssp32.add(sp32);
                        }
                        return dssp32;
                } catch (SQLException e) {
                }

                return null;
        }

        public String getAnh(int ma32) {
                try {
                        String sql = "SELECT HinhAnh FROM SanPham WHERE MaSP=?";
                        PreparedStatement pre = MyConnect.getJDBCConnection().prepareStatement(sql);
                        pre.setInt(1, ma32);
                        ResultSet rs32 = pre.executeQuery();
                        if (rs32.next()) {
                                return rs32.getString("HinhAnh");
                        }
                } catch (SQLException e) {
                }
                return "";
        }

        public void capNhatSoLuongSP(int ma32, int soLuongMat32) {
                SanPham sp32 = getSanPham(ma32);
                int soLuong = sp32.getSoLuong();
                sp32.setSoLuong(soLuong + soLuongMat32);
                try {
                        String sql = "UPDATE SanPham SET SoLuong=? WHERE MaSP=" + ma32;
                        PreparedStatement pre = MyConnect.getJDBCConnection().prepareStatement(sql);
                        pre.setInt(1, sp32.getSoLuong());
                        pre.executeUpdate();
                } catch (SQLException e) {
                }

        }

        public boolean themSanPham(SanPham sp32) {
                try {
                        String sql = "INSERT INTO SanPham(TenSP, MaLoai, SoLuong, DonViTinh, HinhAnh, DonGia) "
                                + "VALUES (?, ?, ?, ?, ?, ?)";
                        PreparedStatement pre = MyConnect.getJDBCConnection().prepareStatement(sql);
                        pre.setString(1, sp32.getTenSP());
                        pre.setInt(2, sp32.getMaLoai());
                        pre.setInt(3, sp32.getSoLuong());
                        pre.setString(4, sp32.getDonViTinh());
                        pre.setString(5, sp32.getHinhAnh());
                        pre.setInt(6, sp32.getDonGia());

                        pre.execute();
                        return true;
                } catch (SQLException e) {
                }
                return false;
        }

        public boolean xoaSanPham(int maSP32) {
                try {
                        String sql = "DELETE FROM SanPham WHERE MaSP=" + maSP32;
                        Statement st = MyConnect.getJDBCConnection().createStatement();
                        st.execute(sql);
                        return true;
                } catch (SQLException e) {
                }
                return false;
        }

        public boolean suaSanPham(SanPham sp32) {
                try {
                        String sql = "UPDATE SanPham SET "
                                + "TenSP=?, "
                                + "MaLoai=?, SoLuong=?, DonViTinh=?, HinhAnh=?, DonGia=? "
                                + "WHERE MaSP=?";
                        PreparedStatement pre = MyConnect.getJDBCConnection().prepareStatement(sql);
                        pre.setString(1, sp32.getTenSP());
                        pre.setInt(2, sp32.getMaLoai());
                        pre.setInt(3, sp32.getSoLuong());
                        pre.setString(4, sp32.getDonViTinh());
                        pre.setString(5, sp32.getHinhAnh());
                        pre.setInt(6, sp32.getDonGia());
                        pre.setInt(7, sp32.getMaSP());

                        pre.execute();
                        return true;
                } catch (SQLException e) {
                        e.printStackTrace();
                }
                return false;
        }
}
