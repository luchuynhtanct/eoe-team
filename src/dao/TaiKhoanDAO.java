package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import bus.DangNhapBUS;

public class TaiKhoanDAO {

        public boolean themTaiKhoan(int maNV50, String tenDangNhap50, String quyen50) {
                try {
                        String sql = "INSERT INTO taikhoan(MaNV, TenDangNhap, MatKhau, Quyen) "
                                + "VALUES (?, ?, ?, ?)";
                        PreparedStatement pre = MyConnect.getJDBCConnection().prepareStatement(sql);
                        pre.setInt(1, maNV50);
                        pre.setString(2, tenDangNhap50);
                        pre.setString(3, tenDangNhap50);
                        pre.setString(4, quyen50);
                        return pre.executeUpdate() > 0;
                } catch (Exception e) {
                }
                return false;
        }

        public boolean kiemTraTrungTenDangNhap(String tenDangNhap50) {
                try {
                        String sql = "SELECT * FROM TaiKhoan WHERE TenDangNhap = N'" + tenDangNhap50 + "'";
                        Statement st50 = MyConnect.getJDBCConnection().createStatement();
                        ResultSet rs50 = st50.executeQuery(sql);
                        return rs50.next();
                } catch (Exception e) {
                        e.printStackTrace();
                }
                return false;
        }

        public String getTenDangNhapTheoMa(int maNV50) {
                try {
                        String sql = "SELECT TenDangNhap FROM TaiKhoan WHERE MaNV=" + maNV50;
                        Statement st50 = MyConnect.getJDBCConnection().createStatement();
                        ResultSet rs50 = st50.executeQuery(sql);
                        if (rs50.next()) {
                                return rs50.getString(1);
                        }
                } catch (Exception e) {
                }
                return "";
        }

        public boolean datLaiMatKhau(int maNV50, String tenDangNhap50) {
                try {
                        String sql = "UPDATE TaiKhoan SET MatKhau=? WHERE MaNV=?";
                        PreparedStatement pre = MyConnect.getJDBCConnection().prepareStatement(sql);
                        pre.setString(1, tenDangNhap50);
                        pre.setInt(2, maNV50);
                        return pre.executeUpdate() > 0;
                } catch (Exception e) {
                }
                return false;
        }

        public boolean datLaiQuyen(int maNV50, String quyen50) {
                try {
                        String sql = "UPDATE TaiKhoan SET Quyen=? WHERE MaNV=?";
                        PreparedStatement pre = MyConnect.getJDBCConnection().prepareStatement(sql);
                        pre.setString(1, quyen50);
                        pre.setInt(2, maNV50);
                        return pre.executeUpdate() > 0;
                } catch (Exception e) {
                }
                return false;
        }

        public String getQuyenTheoMa(int maNV50) {
                try {
                        String sql = "SELECT Quyen FROM TaiKhoan WHERE MaNV=" + maNV50;
                        Statement st50 = MyConnect.getJDBCConnection().createStatement();
                        ResultSet rs50 = st50.executeQuery(sql);
                        if (rs50.next()) {
                                return rs50.getString(1);
                        }
                } catch (Exception e) {
                }
                return "";
        }

        public boolean khoaTaiKhoan(int maNV50) {
                try {
                        String sql = "UPDATE TaiKhoan SET TrangThai=0 WHERE MaNV=?";
                        PreparedStatement pre = MyConnect.getJDBCConnection().prepareStatement(sql);
                        pre.setInt(1, maNV50);
                        return pre.executeUpdate() > 0;
                } catch (Exception e) {
                }
                return false;
        }

        public boolean moKhoaTaiKhoan(int maNV50) {
                try {
                        String sql = "UPDATE TaiKhoan SET TrangThai=1 WHERE MaNV=?";
                        PreparedStatement pre = MyConnect.getJDBCConnection().prepareStatement(sql);
                        pre.setInt(1, maNV50);
                        return pre.executeUpdate() > 0;
                } catch (Exception e) {
                }
                return false;
        }

        public boolean doiMatKhau(String matKhauCu50, String matKhauMoi50) {
                try {
                        String sql = "UPDATE TaiKhoan SET MatKhau=? WHERE MaNV=? AND MatKhau=?";
                        PreparedStatement pre = MyConnect.getJDBCConnection().prepareStatement(sql);
                        pre.setString(1, matKhauMoi50);
                        pre.setInt(2, DangNhapBUS.taiKhoanLogin62.getMaNhanVien());
                        pre.setString(3, matKhauCu50);
                        return pre.executeUpdate() > 0;
                } catch (Exception e) {
                }
                return false;
        }

        public int getTrangThai(int ma50) {
                try {
                        String sql = "SELECT TrangThai FROM TaiKhoan WHERE MaNV=" + ma50;
                        Statement st50 = MyConnect.getJDBCConnection().createStatement();
                        ResultSet rs50 = st50.executeQuery(sql);
                        while (rs50.next()) {
                                return rs50.getInt(1);
                        }
                } catch (Exception e) {
                }
                return -1;
        }
}
