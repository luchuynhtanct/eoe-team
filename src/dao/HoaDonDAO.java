package dao;

import java.sql.*;
import java.util.ArrayList;
import model.HoaDon;

public class HoaDonDAO {

        public ArrayList<HoaDon> getListHoaDon() {
                ArrayList<HoaDon> dshd3636 = new ArrayList<>();
                try {
                        String sql = "SELECT * FROM hoadon";
                        Statement stmt36 = MyConnect.getJDBCConnection().createStatement();
                        ResultSet rs36 = stmt36.executeQuery(sql);
                        while (rs36.next()) {
                                HoaDon hd36 = new HoaDon();
                                hd36.setMaHD(rs36.getInt(1));
                                hd36.setMaKH(rs36.getInt(2));
                                hd36.setMaNV(rs36.getInt(3));
                                hd36.setNgayLap(rs36.getDate(4));
                                hd36.setTongTien(rs36.getInt(5));
                                hd36.setGhiChu(rs36.getString(6));
                                dshd3636.add(hd36);
                        }
                } catch (SQLException ex) {
                        return null;
                }
                return dshd3636;
        }

        public boolean addHoaDon(HoaDon hd36) {
                boolean result36 = false;
                try {
                        String sql1 = "UPDATE KhachHang SET TongChiTieu=TongChiTieu+" + hd36.getTongTien() + " WHERE MaKH=" + hd36.getMaKH();
                        Statement st = MyConnect.getJDBCConnection().createStatement();
                        st.executeUpdate(sql1);
                        String sql = "INSERT INTO hoadon(MaKH, MaNV, NgayLap, TongTien, GhiChu) VALUES(?, ?, ?, ?, ?)";
                        PreparedStatement prep = MyConnect.getJDBCConnection().prepareStatement(sql);
                        prep.setInt(1, hd36.getMaKH());
                        prep.setInt(2, hd36.getMaNV());
                        prep.setTimestamp(3, new java.sql.Timestamp(new java.util.Date().getTime()));
                        prep.setInt(4, hd36.getTongTien());
                        prep.setString(5, hd36.getGhiChu());
                        result36 = prep.executeUpdate() > 0;
                } catch (SQLException ex) {
                        ex.printStackTrace();
                        return false;
                }
                return result36;
        }

        public int getMaHoaDonMoiNhat() {
                try {
                        String sql = "SELECT MAX(maHD) FROM hoadon";
                        Statement st = MyConnect.getJDBCConnection().createStatement();
                        ResultSet rs36 = st.executeQuery(sql);
                        if (rs36.next()) {
                                return rs36.getInt(1);
                        }
                } catch (Exception e) {
                        e.printStackTrace();
                }
                return -1;
        }

        public ArrayList<HoaDon> getListHoaDon(Date dateMin36, Date dateMax36) {
                try {
                        String sql = "SELECT * FROM hoadon WHERE NgayLap BETWEEN CAST(? AS DATE) AND CAST(? AS DATE)";
                        PreparedStatement pre = MyConnect.getJDBCConnection().prepareStatement(sql);
                        pre.setDate(1, dateMin36);
                        pre.setDate(2, dateMax36);
                        ResultSet rs36 = pre.executeQuery();

                        ArrayList<HoaDon> dshd3636 = new ArrayList<>();

                        while (rs36.next()) {
                                HoaDon hd36 = new HoaDon();
                                hd36.setMaHD(rs36.getInt(1));
                                hd36.setMaKH(rs36.getInt(2));
                                hd36.setMaNV(rs36.getInt(3));
                                hd36.setNgayLap(rs36.getDate(4));
                                hd36.setTongTien(rs36.getInt(5));
                                hd36.setGhiChu(rs36.getString(6));
                                dshd3636.add(hd36);
                        }
                        return dshd3636;
                } catch (Exception e) {
                        e.printStackTrace();
                }
                return null;
        }
}
