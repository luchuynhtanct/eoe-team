package model;

import java.util.ArrayList;

public class ThongKe {

        public int soLuongSP30;
        public int soLuongKH30;
        public int soLuongNV30;
        public int[] tongThuQuy30;
        public ArrayList<SanPham> topSanPhamBanChay30;

        public ThongKe() {
        }

        public ThongKe(int soLuongSP30, int soLuongKH30, int soLuongNV30, int[] tongThuQuy30, ArrayList<SanPham> topSanPhamBanChay30) {
                this.soLuongSP30 = soLuongSP30;
                this.soLuongKH30 = soLuongKH30;
                this.soLuongNV30 = soLuongNV30;
                this.tongThuQuy30 = tongThuQuy30;
                this.topSanPhamBanChay30 = topSanPhamBanChay30;
        }

        public int getSoLuongSP() {
                return soLuongSP30;
        }

        public void setSoLuongSP(int soLuongSP30) {
                this.soLuongSP30 = soLuongSP30;
        }

        public int getSoLuongKH() {
                return soLuongKH30;
        }

        public void setSoLuongKH(int soLuongKH30) {
                this.soLuongKH30 = soLuongKH30;
        }

        public int getSoLuongNV() {
                return soLuongNV30;
        }

        public void setSoLuongNV(int soLuongNV30) {
                this.soLuongNV30 = soLuongNV30;
        }

        public int[] getTongThuQuy() {
                return tongThuQuy30;
        }

        public int getTongThuQuy(int quy) {
                return tongThuQuy30[quy - 1];
        }

        public void setTongThuQuy(int[] tongThuQuy30) {
                this.tongThuQuy30 = tongThuQuy30;
        }

        public int getTongDoanhThu() {
                int tong = 0;
                for (int i = 0; i < 4; i++) {
                        tong += tongThuQuy30[i];
                }
                return tong;
        }

        public ArrayList<SanPham> getTopSanPhamBanChay() {
                return topSanPhamBanChay30;
        }

        public void setTopSanPhamBanChay(ArrayList<SanPham> topSanPhamBanChay30) {
                this.topSanPhamBanChay30 = topSanPhamBanChay30;
        }
}
