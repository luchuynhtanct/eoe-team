package model;

import java.util.Date;

public class HoaDon {

        private int maHD36;
        private int maKH36;
        private int maNV36;
        private Date ngayLap36;
        private int tongTien36;
        private String ghiChu36;

        public HoaDon() {
        }

        public HoaDon(int maHD36, int maKH36, int maNV36, Date ngayLap36, int tongTien36, String ghiChu36) {
                this.maHD36 = maHD36;
                this.maKH36 = maKH36;
                this.maNV36 = maNV36;
                this.ngayLap36 = ngayLap36;
                this.tongTien36 = tongTien36;
                this.ghiChu36 = ghiChu36;
        }

        public int getMaHD() {
                return maHD36;
        }

        public void setMaHD(int maHD36) {
                this.maHD36 = maHD36;
        }

        public int getMaKH() {
                return maKH36;
        }

        public void setMaKH(int maKH36) {
                this.maKH36 = maKH36;
        }

        public int getMaNV() {
                return maNV36;
        }

        public void setMaNV(int maNV36) {
                this.maNV36 = maNV36;
        }

        public Date getNgayLap() {
                return ngayLap36;
        }

        public void setNgayLap(Date ngayLap36) {
                this.ngayLap36 = ngayLap36;
        }

        public int getTongTien() {
                return tongTien36;
        }

        public void setTongTien(int tongTien36) {
                this.tongTien36 = tongTien36;
        }

        public String getGhiChu() {
                return ghiChu36;
        }

        public void setGhiChu(String ghiChu36) {
                this.ghiChu36 = ghiChu36;
        }
}
