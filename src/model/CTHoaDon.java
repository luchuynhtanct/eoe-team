package model;

public class CTHoaDon {

        private int maHD36;
        private int maSP36;
        private int soLuong36;
        private int donGia36;
        private int thanhTien36;

        public CTHoaDon() {
        }

        public CTHoaDon(int maHD36, int maSP36, int soLuong36, int donGia36, int thanhTien36) {
                this.maHD36 = maHD36;
                this.maSP36 = maSP36;
                this.soLuong36 = soLuong36;
                this.donGia36 = donGia36;
                this.thanhTien36 = thanhTien36;
        }

        public int getMaHD() {
                return maHD36;
        }

        public void setMaHD(int maHD36) {
                this.maHD36 = maHD36;
        }

        public int getMaSP() {
                return maSP36;
        }

        public void setMaSP(int maSP36) {
                this.maSP36 = maSP36;
        }

        public int getSoLuong() {
                return soLuong36;
        }

        public void setSoLuong(int soLuong36) {
                this.soLuong36 = soLuong36;
        }

        public int getDonGia() {
                return donGia36;
        }

        public void setDonGia(int donGia36) {
                this.donGia36 = donGia36;
        }

        public int getThanhTien() {
                return thanhTien36;
        }

        public void setThanhTien(int thanhTien36) {
                this.thanhTien36 = thanhTien36;
        }
}
