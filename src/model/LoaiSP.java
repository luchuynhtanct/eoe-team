package model;

public class LoaiSP {

        private int maLoai32;
        private String tenLoai32;

        public LoaiSP() {
        }

        public LoaiSP(int maLoai32, String tenLoai32) {
                this.maLoai32 = maLoai32;
                this.tenLoai32 = tenLoai32;
        }

        public int getMaLoai() {
                return maLoai32;
        }

        public void setMaLoai(int maLoai32) {
                this.maLoai32 = maLoai32;
        }

        public String getTenLoai() {
                return tenLoai32;
        }

        public void setTenLoai(String tenLoai32) {
                this.tenLoai32 = tenLoai32;
        }

}
