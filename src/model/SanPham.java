package model;

public class SanPham {

        private int maSP32;
        private String tenSP32;
        private int maLoai32;
        private int soLuong32;
        private String donViTinh32;
        private String hinhAnh32;
        private int donGia32;

        public SanPham() {

        }

        public SanPham(int maSP32, String tenSP32, int maLoai32, int soLuong32, String donViTinh32, String hinhAnh32, int donGia32) {
                this.maSP32 = maSP32;
                this.tenSP32 = tenSP32;
                this.maLoai32 = maLoai32;
                this.soLuong32 = soLuong32;
                this.donViTinh32 = donViTinh32;
                this.hinhAnh32 = hinhAnh32;
                this.donGia32 = donGia32;
        }

        public int getMaSP() {
                return maSP32;
        }

        public void setMaSP(int maSP32) {
                this.maSP32 = maSP32;
        }

        public String getTenSP() {
                return tenSP32;
        }

        public void setTenSP(String tenSP32) {
                this.tenSP32 = tenSP32;
        }

        public int getMaLoai() {
                return maLoai32;
        }

        public void setMaLoai(int maLoai32) {
                this.maLoai32 = maLoai32;
        }

        public int getSoLuong() {
                return soLuong32;
        }

        public void setSoLuong(int soLuong32) {
                this.soLuong32 = soLuong32;
        }

        public String getDonViTinh() {
                return donViTinh32;
        }

        public void setDonViTinh(String donViTinh32) {
                this.donViTinh32 = donViTinh32;
        }

        public String getHinhAnh() {
                return hinhAnh32;
        }

        public void setHinhAnh(String hinhAnh32) {
                this.hinhAnh32 = hinhAnh32;
        }

        public int getDonGia() {
                return donGia32;
        }

        public void setDonGia(int donGia32) {
                this.donGia32 = donGia32;
        }
}
