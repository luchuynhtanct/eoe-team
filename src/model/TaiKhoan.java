package model;

public class TaiKhoan {

        private int maNhanVien50;
        private String tenDangNhap50;
        private String matKhau50;
        private String quyen50;

        public TaiKhoan() {
        }

        public TaiKhoan(int maNhanVien50, String tenDangNhap50, String matKhau50, String quyen50) {
                this.maNhanVien50 = maNhanVien50;
                this.tenDangNhap50 = tenDangNhap50;
                this.matKhau50 = matKhau50;
                this.quyen50 = quyen50;
        }

        public int getMaNhanVien() {
                return maNhanVien50;
        }

        public void setMaNhanVien(int maNhanVien50) {
                this.maNhanVien50 = maNhanVien50;
        }

        public String getTenDangNhap() {
                return tenDangNhap50;
        }

        public void setTenDangNhap(String tenDangNhap50) {
                this.tenDangNhap50 = tenDangNhap50;
        }

        public String getMatKhau() {
                return matKhau50;
        }

        public void setMatKhau(String matKhau50) {
                this.matKhau50 = matKhau50;
        }

        public String getQuyen() {
                return quyen50;
        }

        public void setQuyen(String quyen50) {
                this.quyen50 = quyen50;
        }
}
