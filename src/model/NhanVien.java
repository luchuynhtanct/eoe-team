package model;

public class NhanVien {

        private int maNV30;
        private String ho30;
        private String ten30;
        private String gioiTinh30;
        private String chucVu30;

        public NhanVien() {
        }

        public NhanVien(int maNV30, String ho30, String ten30, String gioiTinh30, String chucVu30) {
                this.maNV30 = maNV30;
                this.ho30 = ho30;
                this.ten30 = ten30;
                this.gioiTinh30 = gioiTinh30;
                this.chucVu30 = chucVu30;
        }

        public int getMaNV() {
                return maNV30;
        }

        public void setMaNV(int maNV30) {
                this.maNV30 = maNV30;
        }

        public String getHo() {
                return ho30;
        }

        public void setHo(String ho30) {
                this.ho30 = ho30;
        }

        public String getTen() {
                return ten30;
        }

        public void setTen(String ten30) {
                this.ten30 = ten30;
        }

        public String getGioiTinh() {
                return gioiTinh30;
        }

        public void setGioiTinh(String gioiTinh30) {
                this.gioiTinh30 = gioiTinh30;
        }

        public String getChucVu() {
                return chucVu30;
        }

        public void setChucVu(String chucVu30) {
                this.chucVu30 = chucVu30;
        }

}
