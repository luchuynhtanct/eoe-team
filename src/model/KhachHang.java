package model;

public class KhachHang {

        private int maKH62;
        private String ho62;
        private String ten62;
        private String gioiTinh62;
        private int tongChiTieu62;

        public KhachHang() {
        }

        public KhachHang(int maKH62, String ho62, String ten62, String gioiTinh62, int tongChiTieu62) {
                this.maKH62 = maKH62;
                this.ho62 = ho62;
                this.ten62 = ten62;
                this.gioiTinh62 = gioiTinh62;
                this.tongChiTieu62 = tongChiTieu62;
        }

        public int getMaKH() {
                return maKH62;
        }

        public void setMaKH(int maKH62) {
                this.maKH62 = maKH62;
        }

        public String getHo() {
                return ho62;
        }

        public void setHo(String ho62) {
                this.ho62 = ho62;
        }

        public String getTen() {
                return ten62;
        }

        public void setTen(String ten62) {
                this.ten62 = ten62;
        }

        public String getGioiTinh() {
                return gioiTinh62;
        }

        public void setGioiTinh(String gioiTinh62) {
                this.gioiTinh62 = gioiTinh62;
        }

        public int getTongChiTieu() {
                return tongChiTieu62;
        }

        public void setTongChiTieu(int tongChiTieu62) {
                this.tongChiTieu62 = tongChiTieu62;
        }

}
