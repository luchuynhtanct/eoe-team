package model;

public class PhanQuyen {

        private String quyen50;
        private int qlSanPham50;
        private int qlNhanVien50;
        private int qlKhachHang50;
        private int thongKe50;

        public PhanQuyen() {
        }

        public PhanQuyen(String quyen50, int qlSanPham50, int qlNhanVien50, int qlKhachHang50, int thongKe50) {
                this.quyen50 = quyen50;
                this.qlSanPham50 = qlSanPham50;
                this.qlNhanVien50 = qlNhanVien50;
                this.qlKhachHang50 = qlKhachHang50;
                this.thongKe50 = thongKe50;
        }

        public String getQuyen() {
                return quyen50;
        }

        public void setQuyen(String quyen50) {
                this.quyen50 = quyen50;
        }

        public int getQlSanPham() {
                return qlSanPham50;
        }

        public void setQlSanPham(int qlSanPham50) {
                this.qlSanPham50 = qlSanPham50;
        }

        public int getQlNhanVien() {
                return qlNhanVien50;
        }

        public void setQlNhanVien(int qlNhanVien50) {
                this.qlNhanVien50 = qlNhanVien50;
        }

        public int getQlKhachHang() {
                return qlKhachHang50;
        }

        public void setQlKhachHang(int qlKhachHang50) {
                this.qlKhachHang50 = qlKhachHang50;
        }

        public int getThongKe() {
                return thongKe50;
        }

        public void setThongKe(int thongKe50) {
                this.thongKe50 = thongKe50;
        }

}
