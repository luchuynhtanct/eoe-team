package bus;

import custom.MyDialog;
import dao.HoaDonDAO;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import model.HoaDon;

public class HoaDonBUS {

    private ArrayList<HoaDon> listHoaDon36;
    private HoaDonDAO hoaDonDAO36 = new HoaDonDAO();

    public ArrayList<HoaDon> getListHoaDon() {
        listHoaDon36 = hoaDonDAO36.getListHoaDon();
        return listHoaDon36;
    }

    public void luuHoaDon(int maKH36, String nhanVien36, int tongTien36, String ghiChu36) {
        HoaDon hd36 = new HoaDon();
        String[] arrNV36 = nhanVien36.split(" - ");
        int maNV36 = Integer.parseInt(arrNV36[0]);
        hd36.setMaNV(maNV36);
        hd36.setMaKH(maKH36);
        hd36.setGhiChu(ghiChu36);
        hd36.setTongTien(tongTien36);

        hoaDonDAO36.addHoaDon(hd36);
    }

    public int getMaHoaDonMoiNhat() {
        return hoaDonDAO36.getMaHoaDonMoiNhat();
    }

    public HoaDon getHoaDon(String maHD36) {
        int ma = Integer.parseInt(maHD36);
        for (HoaDon hd36 : listHoaDon36) {
            if (hd36.getMaHD() == ma) {
                return hd36;
            }
        }
        return null;
    }

    public ArrayList<HoaDon> getListHoaDonTheoGia(String min, String max) {
        try {
            int minPrice36 = Integer.parseInt(min);
            int maxPrice36 = Integer.parseInt(max);
            ArrayList<HoaDon> dshd36 = new ArrayList<>();
            for (HoaDon hd36 : listHoaDon36) {
                if (hd36.getTongTien() > minPrice36 && hd36.getTongTien() < maxPrice36) {
                    dshd36.add(hd36);
                }
            }
            return dshd36;
        } catch (Exception e) {
            new MyDialog("Hãy nhập khoảng giá hợp lệ", MyDialog.ERROR_DIALOG);
        }
        return null;
    }

    public ArrayList<HoaDon> getListHoaDonTheoNgay(String min, String max) {
        try {
            SimpleDateFormat sdf36 = new SimpleDateFormat("dd/MM/yyyy");
            Date minDate36 = sdf36.parse(min);
            Date maxDate36 = sdf36.parse(max);

            java.sql.Date dateMin36 = new java.sql.Date(minDate36.getTime());
            java.sql.Date dateMax36 = new java.sql.Date(maxDate36.getTime());

            ArrayList<HoaDon> dshd36 = hoaDonDAO36.getListHoaDon(dateMin36, dateMax36);
            return dshd36;
        } catch (Exception e) {
            new MyDialog("Hãy nhập khoảng ngày hợp lệ!", MyDialog.ERROR_DIALOG);
        }
        return null;
    }
}
