package bus;

import custom.MyDialog;
import dao.NhanVienDAO;
import java.util.ArrayList;
import model.NhanVien;

public class NhanVienBUS {

        private NhanVienDAO nv50DAO30 = new NhanVienDAO();
        private ArrayList<NhanVien> listNhanVien30 = null;

        public NhanVienBUS() {
                docDanhSach();
        }

        public void docDanhSach() {
                this.listNhanVien30 = nv50DAO30.getDanhSachNhanVien();
        }

        public ArrayList<NhanVien> getDanhSachNhanVien() {
                if (this.listNhanVien30 == null) {
                        docDanhSach();
                }
                return this.listNhanVien30;
        }

        public boolean themNhanVien(String ho50, String ten50, String gioiTinh50, String chucVu50) {
                ho50 = ho50.trim();
                ten50 = ten50.trim();
                chucVu50 = chucVu50.trim();
                if (ten50.equals("")) {
                        new MyDialog("Tên không được để trống!", MyDialog.ERROR_DIALOG);
                        return false;
                }
                if (chucVu50.equals("")) {
                        new MyDialog("Chức vụ không được để trống!", MyDialog.ERROR_DIALOG);
                        return false;
                }
                NhanVien nv50 = new NhanVien();
                nv50.setHo(ho50);
                nv50.setTen(ten50);
                nv50.setGioiTinh(gioiTinh50);
                nv50.setChucVu(chucVu50);
                boolean flag50 = nv50DAO30.themNhanVien(nv50);
                if (!flag50) {
                        new MyDialog("Thêm thất bại!", MyDialog.ERROR_DIALOG);
                } else {
                        new MyDialog("Thêm thành công!", MyDialog.SUCCESS_DIALOG);
                }
                return flag50;
        }

        public boolean updateNhanVien(String ma, String ho50, String ten50, String gioiTinh50, String chucVu50) {
                int maNV = Integer.parseInt(ma);
                ho50 = ho50.trim();
                ten50 = ten50.trim();
                chucVu50 = chucVu50.trim();
                if (ten50.equals("")) {
                        new MyDialog("Tên không được để trống!", MyDialog.ERROR_DIALOG);
                        return false;
                }
                if (chucVu50.equals("")) {
                        new MyDialog("Chức vụ không được để trống!", MyDialog.ERROR_DIALOG);
                        return false;
                }
                NhanVien nv50 = new NhanVien();
                nv50.setMaNV(maNV);
                nv50.setHo(ho50);
                nv50.setTen(ten50);
                nv50.setGioiTinh(gioiTinh50);
                nv50.setChucVu(chucVu50);
                boolean flag50 = nv50DAO30.updateNhanVien(nv50);
                if (!flag50) {
                        new MyDialog("Cập nhập thất bại!", MyDialog.ERROR_DIALOG);
                } else {
                        new MyDialog("Cập nhập thành công!", MyDialog.SUCCESS_DIALOG);
                }
                return flag50;
        }

        public ArrayList<NhanVien> timNhanVien(String tuKhoa50) {
                tuKhoa50 = tuKhoa50.toLowerCase();
                ArrayList<NhanVien> dsnv50 = new ArrayList<>();
                for (NhanVien nv50 : listNhanVien30) {
                        if (nv50.getHo().toLowerCase().contains(tuKhoa50) || nv50.getTen().toLowerCase().contains(tuKhoa50)
                                || nv50.getGioiTinh().toLowerCase().contains(tuKhoa50) || nv50.getChucVu().toLowerCase().contains(tuKhoa50)) {
                                dsnv50.add(nv50);
                        }
                }
                return dsnv50;
        }

        public boolean xoaNhanVien(String ma) {
                try {
                        int maNV = Integer.parseInt(ma);
                        MyDialog dlg50 = new MyDialog("Bạn có chắc chắn muốn xoá?", MyDialog.WARNING_DIALOG);
                        boolean flag50 = false;
                        if (dlg50.getAction() == MyDialog.OK_OPTION) {
                                flag50 = nv50DAO30.deleteNhanVien(maNV);
                                if (flag50) {
                                        new MyDialog("Xoá thành công!", MyDialog.SUCCESS_DIALOG);
                                } else {
                                        new MyDialog("Xoá thất bại!", MyDialog.ERROR_DIALOG);
                                }
                        }
                        return flag50;
                } catch (Exception e) {
                        new MyDialog("Chưa chọn nhân viên!", MyDialog.ERROR_DIALOG);
                }
                return false;
        }
}
