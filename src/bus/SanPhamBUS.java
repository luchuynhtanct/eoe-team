package bus;

import custom.MyDialog;
import dao.SanPhamDAO;
import java.util.ArrayList;
import model.SanPham;

public class SanPhamBUS {

        private ArrayList<SanPham> listSanPham32 = null;
        private SanPhamDAO sp32DAO32 = new SanPhamDAO();

        public SanPhamBUS() {
                docListSanPham();
        }

        public void docListSanPham() {
                listSanPham32 = sp32DAO32.getListSanPham();
        }

        public ArrayList<SanPham> getListSanPham() {
                if (listSanPham32 == null) {
                        docListSanPham();
                }
                return listSanPham32;
        }

        public SanPham getSanPham(String ma) {
                if (!ma.trim().equals("")) {
                        try {
                                int maSP32 = Integer.parseInt(ma);
                                for (SanPham sp32 : listSanPham32) {
                                        if (sp32.getMaSP() == maSP32) {
                                                return sp32;
                                        }
                                }
                        } catch (Exception e) {
                        }
                }
                return null;
        }

        public ArrayList<SanPham> getSanPhamTheoTen(String ten32) {
                ArrayList<SanPham> dssp32 = new ArrayList<>();
                for (SanPham sp32 : listSanPham32) {
                        String ten32SP = sp32.getTenSP().toLowerCase();
                        if (ten32SP.toLowerCase().contains(ten32.toLowerCase())) {
                                dssp32.add(sp32);
                        }
                }
                return dssp32;
        }

        public ArrayList<SanPham> getSanPhamTheoLoai(String ma) {
                if (!ma.trim().equals("")) {
                        ArrayList<SanPham> dssp32 = new ArrayList<>();
                        try {
                                int maLoai32 = Integer.parseInt(ma);
                                for (SanPham sp32 : listSanPham32) {
                                        if (sp32.getMaLoai() == maLoai32) {
                                                dssp32.add(sp32);
                                        }
                                }
                                return dssp32;
                        } catch (Exception e) {
                        }
                }
                return null;
        }

        public String getAnh(String ma) {
                int maSP32 = Integer.parseInt(ma);
                return sp32DAO32.getAnh(maSP32);
        }

        public void capNhatSoLuongSP(int ma, int soLuong32Mat32) {
                sp32DAO32.capNhatSoLuongSP(ma, soLuong32Mat32);
        }

        public boolean themSanPham(String ten32,
                String loai32,
                String soLuong32,
                String donViTinh32,
                String anh32,
                String donGia32) {

                if (ten32.trim().equals("")) {
                        new MyDialog("Tên SP không được để rỗng!", MyDialog.ERROR_DIALOG);
                        return false;
                }

                if (donViTinh32.trim().equals("")) {
                        new MyDialog("Vui lòng điền Đơn vị tính!", MyDialog.ERROR_DIALOG);
                        return false;
                }

                try {
                        String[] loaiTmp32 = loai32.split(" - ");
                        int maLoai32 = Integer.parseInt(loaiTmp32[0]);
                        soLuong32 = soLuong32.replace(".", "");
                        int soLuongSP32 = Integer.parseInt(soLuong32);
                        donGia32 = donGia32.replace(".", "");
                        int donGia32SP = Integer.parseInt(donGia32);
                        if (maLoai32 == 0) {
                                new MyDialog("Vui lòng chọn Loại sản phẩm!", MyDialog.ERROR_DIALOG);
                                return false;
                        }
                        SanPham sp32 = new SanPham();
                        sp32.setTenSP(ten32);
                        sp32.setMaLoai(maLoai32);
                        sp32.setSoLuong(soLuongSP32);
                        sp32.setDonViTinh(donViTinh32);
                        sp32.setHinhAnh(anh32);
                        sp32.setDonGia(donGia32SP);

                        if (sp32DAO32.themSanPham(sp32)) {
                                new MyDialog("Thêm thành công!", MyDialog.SUCCESS_DIALOG);
                                return true;
                        } else {
                                new MyDialog("Thêm thất bại!", MyDialog.ERROR_DIALOG);
                                return false;
                        }
                } catch (Exception e) {
                        new MyDialog("Nhập số hợp lệ cho Đơn giá và Số lượng!", MyDialog.ERROR_DIALOG);
                        e.printStackTrace();
                }
                return false;
        }

        public boolean xoaSanPham(String ma) {
                if (ma.trim().equals("")) {
                        new MyDialog("Chưa chọn sản phẩm để xoá!", MyDialog.ERROR_DIALOG);
                        return false;
                }

                int maSP32 = Integer.parseInt(ma);
                if (sp32DAO32.xoaSanPham(maSP32)) {
                        new MyDialog("Xoá thành công!", MyDialog.SUCCESS_DIALOG);
                        return true;
                }

                new MyDialog("Xoá thất bại!", MyDialog.ERROR_DIALOG);
                return false;
        }

        public boolean suaSanPham(String ma,
                String ten32,
                String loai32,
                String soLuong32,
                String donViTinh32,
                String anh32,
                String donGia32) {

                try {
                        if (ma.trim().equals("")) {
                                new MyDialog("Chưa chọn sản phẩm để sửa!", MyDialog.ERROR_DIALOG);
                                return false;
                        }
                        donGia32 = donGia32.replace(",", "");
                        int maSP32 = Integer.parseInt(ma);
                        String[] loaiTmp32 = loai32.split(" - ");
                        int maLoai32 = Integer.parseInt(loaiTmp32[0]);
                        int soLuongSP32 = Integer.parseInt(soLuong32);
                        int donGia32SP = Integer.parseInt(donGia32);

                        if (maLoai32 == 0) {
                                new MyDialog("Vui lòng chọn Loại sản phẩm!", MyDialog.ERROR_DIALOG);
                                return false;
                        }

                        if (ten32.trim().equals("")) {
                                new MyDialog("Tên SP không được để rỗng!", MyDialog.ERROR_DIALOG);
                                return false;
                        }

                        if (donViTinh32.trim().equals("")) {
                                new MyDialog("Vui lòng điền Đơn vị tính!", MyDialog.ERROR_DIALOG);
                                return false;
                        }

                        SanPham sp32 = new SanPham();
                        sp32.setMaSP(maSP32);
                        sp32.setTenSP(ten32);
                        sp32.setMaLoai(maLoai32);
                        sp32.setSoLuong(soLuongSP32);
                        sp32.setDonViTinh(donViTinh32);
                        sp32.setHinhAnh(anh32);
                        sp32.setDonGia(donGia32SP);

                        if (sp32DAO32.suaSanPham(sp32)) {
                                new MyDialog("Sửa thành công!", MyDialog.SUCCESS_DIALOG);
                                return true;
                        } else {
                                new MyDialog("Sửa thất bại!", MyDialog.ERROR_DIALOG);
                                return false;
                        }
                } catch (Exception e) {
                        new MyDialog("Nhập số hợp lệ cho Đơn giá và Số lượng!", MyDialog.ERROR_DIALOG);
                }
                return false;
        }

        public String getTenSP(int maSP32) {
                for (SanPham sp32 : listSanPham32) {
                        if (sp32.getMaSP() == maSP32) {
                                return sp32.getTenSP();
                        }
                }
                return "";
        }
}
