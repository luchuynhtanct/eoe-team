package bus;

import custom.MyDialog;
import dao.DangNhapDAO;
import java.io.*;
import model.TaiKhoan;

public class DangNhapBUS {

        private final static int EMPTY_ERROR = 1;
        private final static int WRONG_ERROR = 2;
        public static TaiKhoan taiKhoanLogin62 = null;

        public TaiKhoan getTaiKhoanDangNhap(String user62, String password62, boolean selected62) {
                if (kiemTraDangNhap(user62, password62) == EMPTY_ERROR) {
                        new MyDialog("Không được để trống thông tin!", MyDialog.ERROR_DIALOG);
                        return null;
                }
                TaiKhoan tk62 = new TaiKhoan();
                tk62.setTenDangNhap(user62);
                tk62.setMatKhau(password62);

                DangNhapDAO dangNhapDAO62 = new DangNhapDAO();
                TaiKhoan account62 = dangNhapDAO62.dangNhap(tk62);
                taiKhoanLogin62 = account62;

                if (account62 == null) {
                        new MyDialog("Sai thông tin đăng nhập hoặc tài khoản đã bị khoá!", MyDialog.ERROR_DIALOG);
                } else {
                        PhanQuyenBUS phanQuyenBUS62 = new PhanQuyenBUS();
                        phanQuyenBUS62.kiemTraQuyen(account62.getQuyen());
                        xuLyGhiNhoDangNhap(user62, password62, selected62);
                        new MyDialog("Đăng nhập thành công!", MyDialog.SUCCESS_DIALOG);
//            new MyDialog("Vì tình hình dịch Covid phức tạp, cửa hàng chỉ thực hiện bán mang về!", MyDialog.INFO_DIALOG);
                }
                return account62;
        }

        public String getTaiKhoanGhiNho() {
                try {
                        FileInputStream fis62 = new FileInputStream("src/remember.dat");
                        BufferedReader br62 = new BufferedReader(new InputStreamReader(fis62));
                        String line62 = br62.readLine();
                        br62.close();
                        return line62;
                } catch (Exception e) {
                }
                return "";
        }

        private void xuLyGhiNhoDangNhap(String user62, String password62, boolean selected62) {
                try {
                        if (!selected62) {
                                user62 = "";
                                password62 = "";
                        }
                        FileWriter fw = new FileWriter("src/remember.dat");
                        fw.write(user62 + " | " + password62);
                        fw.close();
                } catch (Exception e) {
                }
        }

        private int kiemTraDangNhap(String user62, String password62) {
                user62 = user62.replaceAll("\\s+", "");
                password62 = password62.replaceAll("\\s+", "");
                int result62 = 0;

                TaiKhoan tk62 = new TaiKhoan();
                tk62.setTenDangNhap(user62);
                tk62.setMatKhau(password62);

                DangNhapDAO dangNhapDAO62 = new DangNhapDAO();
                TaiKhoan account62 = dangNhapDAO62.dangNhap(tk62);

                if (user62.length() <= 0 || password62.length() <= 0) {
                        result62 = EMPTY_ERROR;
                } else if (account62 == null) {
                        result62 = WRONG_ERROR;
                }
                return result62;
        }

}
