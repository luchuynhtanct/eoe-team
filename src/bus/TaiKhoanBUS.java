package bus;

import custom.MyDialog;
import dao.TaiKhoanDAO;

public class TaiKhoanBUS {

        private TaiKhoanDAO taiKhoanDAO50 = new TaiKhoanDAO();

        public String getTenDangNhapTheoMa(String ma) {
                int maNV50 = Integer.parseInt(ma);
                return taiKhoanDAO50.getTenDangNhapTheoMa(maNV50);
        }

        public String getQuyenTheoMa(String ma) {
                int maNV50 = Integer.parseInt(ma);
                return taiKhoanDAO50.getQuyenTheoMa(maNV50);
        }

        public void datLaiMatKhau(String ma, String tenDangNhap50) {
                int maNV50 = Integer.parseInt(ma);
                boolean flag50 = taiKhoanDAO50.datLaiMatKhau(maNV50, tenDangNhap50);
                if (flag50) {
                        new MyDialog("Đặt lại thành công! Mật khẩu mới là: " + tenDangNhap50, MyDialog.SUCCESS_DIALOG);
                } else {
                        new MyDialog("Đặt lại thất bại!", MyDialog.ERROR_DIALOG);
                }
        }

        public void datLaiQuyen(String ma, String quyen50) {
                int maNV50 = Integer.parseInt(ma);
                boolean flag50 = taiKhoanDAO50.datLaiQuyen(maNV50, quyen50);
                if (flag50) {
                        new MyDialog("Đặt lại thành công!", MyDialog.SUCCESS_DIALOG);
                } else {
                        new MyDialog("Đặt lại thất bại!", MyDialog.ERROR_DIALOG);
                }
        }

        public boolean kiemTraTrungTenDangNhap(String tenDangNhap50) {
                return taiKhoanDAO50.kiemTraTrungTenDangNhap(tenDangNhap50);
        }

        public boolean themTaiKhoan(String ma, String tenDangNhap50, String quyen50) {
                int maNV50 = Integer.parseInt(ma);
                if (tenDangNhap50.trim().equals("")) {
                        new MyDialog("Không được để trống Tên đăng nhập!", MyDialog.ERROR_DIALOG);
                        return false;
                }
                if (kiemTraTrungTenDangNhap(tenDangNhap50)) {
                        MyDialog dlg50 = new MyDialog("Tên đăng nhập bị trùng! Có thể tài khoản bị khoá, thực hiện mở khoá?", MyDialog.WARNING_DIALOG);
                        if (dlg50.getAction() == MyDialog.OK_OPTION) {
                                moKhoaTaiKhoan(ma);
                                return true;
                        }
                        return false;
                }
                boolean flag50 = taiKhoanDAO50.themTaiKhoan(maNV50, tenDangNhap50, quyen50);
                if (flag50) {
                        new MyDialog("Cấp tài khoản thành công! Mật khẩu là " + tenDangNhap50, MyDialog.SUCCESS_DIALOG);
                } else {
                        new MyDialog("Cấp tài khoản thất bại! Tài khoản đã tồn tại", MyDialog.ERROR_DIALOG);
                }
                return flag50;
        }

        public void khoaTaiKhoan(String ma) {
                int maNV50 = Integer.parseInt(ma);
                boolean flag50 = taiKhoanDAO50.khoaTaiKhoan(maNV50);
                if (flag50) {
                        new MyDialog("Khoá tài khoản thành công!", MyDialog.SUCCESS_DIALOG);
                } else {
                        new MyDialog("Khoá tài khoản thất bại!", MyDialog.ERROR_DIALOG);
                }
        }

        public void moKhoaTaiKhoan(String ma) {
                int maNV50 = Integer.parseInt(ma);
                boolean flag50 = taiKhoanDAO50.moKhoaTaiKhoan(maNV50);
                if (flag50) {
                        new MyDialog("Mở khoá tài khoản thành công!", MyDialog.SUCCESS_DIALOG);
                } else {
                        new MyDialog("Mở khoá tài khoản thất bại!", MyDialog.ERROR_DIALOG);
                }
        }

        public boolean doiMatKhau(String matKhauCu50, String matKhauMoi50, String nhapLaiMatKhau50) {
                if (!matKhauMoi50.equals(nhapLaiMatKhau50)) {
                        new MyDialog("Mật khẩu mới không khớp!", MyDialog.ERROR_DIALOG);
                        return false;
                }
                boolean flag50 = taiKhoanDAO50.doiMatKhau(matKhauCu50, matKhauMoi50);
                if (flag50) {
                        new MyDialog("Đổi thành công!", MyDialog.SUCCESS_DIALOG);
                } else {
                        new MyDialog("Mật khẩu cũ nhập sai!", MyDialog.ERROR_DIALOG);
                }
                return flag50;
        }

        public int getTrangThai(String maNV50) {
                int ma = Integer.parseInt(maNV50);
                return taiKhoanDAO50.getTrangThai(ma);
        }

}
