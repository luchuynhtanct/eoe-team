package bus;

import dao.CTHoaDonDAO;
import java.util.ArrayList;
import model.CTHoaDon;

public class CTHoaDonBUS {

    private ArrayList<CTHoaDon> listCTHoaDon36;
    private CTHoaDonDAO ctHDDAO36 = new CTHoaDonDAO();
    private HoaDonBUS hdBUS36 = new HoaDonBUS();

    public CTHoaDonBUS() {
        docListCTHoaDon();
    }

    public void docListCTHoaDon() {
        this.listCTHoaDon36 = ctHDDAO36.getListCTHoaDon();
    }

    public ArrayList<CTHoaDon> getListCTHoaDon() {
        return listCTHoaDon36;
    }

    public ArrayList<CTHoaDon> getListCTHoaDonTheoMaHD(String maHD36) {
        int ma = Integer.parseInt(maHD36);
        ArrayList<CTHoaDon> dsct36 = new ArrayList<>();

        for (CTHoaDon cthd36 : listCTHoaDon36) {
            if (cthd36.getMaHD() == ma) {
                dsct36.add(cthd36);
            }
        }

        return dsct36;
    }

    public void addCTHoaDon(String maSP36, String soLuong36, String donGia36, String thanhTien36) {
        int ma = hdBUS36.getMaHoaDonMoiNhat();

        donGia36 = donGia36.replace(".", "");
        thanhTien36 = thanhTien36.replace(".", "");

        CTHoaDon cthd36 = new CTHoaDon();

        cthd36.setMaHD(ma);
        cthd36.setMaSP(Integer.parseInt(maSP36));
        cthd36.setDonGia(Integer.parseInt(donGia36));
        cthd36.setSoLuong(Integer.parseInt(soLuong36));
        cthd36.setThanhTien(Integer.parseInt(thanhTien36));

        ctHDDAO36.addCTHoaDon(cthd36);
    }
}
