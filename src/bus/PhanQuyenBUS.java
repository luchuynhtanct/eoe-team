package bus;

import custom.MyDialog;
import dao.PhanQuyenDAO;
import java.util.ArrayList;
import model.PhanQuyen;

public class PhanQuyenBUS {

        public static PhanQuyen quyen50TK50 = null;
        private PhanQuyenDAO phanQuyen50DAO50 = new PhanQuyenDAO();
        private ArrayList<PhanQuyen> listPhanQuyen50 = null;

        public void docDanhSachQuyen() {
                this.listPhanQuyen50 = phanQuyen50DAO50.getListQuyen();
        }

        public void kiemTraQuyen(String quyen50) {
                quyen50TK50 = phanQuyen50DAO50.getQuyen(quyen50);
        }

        public ArrayList<PhanQuyen> getListQuyen() {
                if (listPhanQuyen50 == null) {
                        docDanhSachQuyen();
                }
                return this.listPhanQuyen50;
        }

        public boolean suaQuyen(String tenQuyen50, int sanPham50, int nhanVien50, int khachHang50, int thongKe50) {
                PhanQuyen phanQuyen50 = new PhanQuyen(tenQuyen50, sanPham50, nhanVien50, khachHang50, thongKe50);
                boolean flag50 = phanQuyen50DAO50.suaQuyen(phanQuyen50);
                if (flag50) {
                        new MyDialog("Sửa thành công!", MyDialog.SUCCESS_DIALOG);
                } else {
                        new MyDialog("Sửa thất bại!", MyDialog.ERROR_DIALOG);
                }
                return flag50;
        }

        public boolean themQuyen(String tenQuyen50) {
                if (tenQuyen50 == null || tenQuyen50.trim().equals("")) {
                        return false;
                }

                if (kiemTonTaiTraQuyen(tenQuyen50)) {
                        new MyDialog("Thêm thất bại! Quyền đã tồn tại", MyDialog.ERROR_DIALOG);
                        return false;
                }

                PhanQuyen phanQuyen50 = new PhanQuyen(tenQuyen50, 0, 0, 0, 0);
                boolean flag50 = phanQuyen50DAO50.themQuyen(phanQuyen50);
                if (flag50) {
                        new MyDialog("Thêm thành công! Hãy hiệu chỉnh quyền", MyDialog.SUCCESS_DIALOG);
                } else {
                        new MyDialog("Thêm thất bại! Quyền đã tồn tại", MyDialog.ERROR_DIALOG);
                }
                return flag50;
        }

        private boolean kiemTonTaiTraQuyen(String tenQuyen50) {
                docDanhSachQuyen();
                for (PhanQuyen q : listPhanQuyen50) {
                        if (q.getQuyen().equalsIgnoreCase(tenQuyen50)) {
                                return true;
                        }
                }
                return false;
        }

        public boolean xoaQuyen(String tenQuyen50) {
                boolean flag50 = phanQuyen50DAO50.xoaQuyen(tenQuyen50);
                if (flag50) {
                        new MyDialog("Xoá thành công!", MyDialog.SUCCESS_DIALOG);
                } else {
                        new MyDialog("Xoá thất bại!", MyDialog.ERROR_DIALOG);
                }
                return flag50;
        }
}
