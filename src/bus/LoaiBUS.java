package bus;

import custom.MyDialog;
import dao.LoaiDAO;
import java.util.ArrayList;
import model.LoaiSP;

public class LoaiBUS {

        private LoaiDAO loai32DAO32 = new LoaiDAO();
        private ArrayList<LoaiSP> listLoai32 = null;

        public LoaiBUS() {
                docDanhSachLoai();
        }

        public void docDanhSachLoai() {
                this.listLoai32 = loai32DAO32.getDanhSachLoai();
        }

        public ArrayList<LoaiSP> getDanhSachLoai() {
                if (listLoai32 == null) {
                        docDanhSachLoai();
                }
                return this.listLoai32;
        }

        public String getTenLoai(int ma) {
                for (LoaiSP loai32 : listLoai32) {
                        if (loai32.getMaLoai() == ma) {
                                return loai32.getMaLoai() + " - " + loai32.getTenLoai();
                        }
                }
                return "";
        }

        public boolean themLoai(int maLoai32, String tenLoai32) {
                if (tenLoai32.trim().equals("")) {
                        new MyDialog("Không được để trống tên loại!", MyDialog.ERROR_DIALOG);
                        return false;
                }
                maLoai32 += 1;
                LoaiSP loai32 = new LoaiSP(maLoai32, tenLoai32);
                if (loai32DAO32.themLoai(loai32)) {
                        new MyDialog("Thêm thành công!", MyDialog.SUCCESS_DIALOG);
                        return true;
                } else {
                        new MyDialog("Thêm thất bại!", MyDialog.ERROR_DIALOG);
                        return false;
                }
        }

        public boolean xoaLoai(String ma) {
                if (ma.trim().equals("")) {
                        new MyDialog("Chưa chọn loại để xoá!", MyDialog.SUCCESS_DIALOG);
                        return false;
                }
                int maLoai32 = Integer.parseInt(ma);
                if (loai32DAO32.xoaLoai(maLoai32)) {
                        new MyDialog("Xoá thành công!", MyDialog.SUCCESS_DIALOG);
                        return true;
                } else {
                        new MyDialog("Xoá thất bại! Loại có sản phẩm con", MyDialog.ERROR_DIALOG);
                        return false;
                }
        }

        public boolean suaLoai(String ma, String ten32) {
                if (ten32.trim().equals("")) {
                        new MyDialog("Không được để trống tên loại!", MyDialog.ERROR_DIALOG);
                        return false;
                }
                int maLoai32 = Integer.parseInt(ma);
                if (loai32DAO32.suaLoai(maLoai32, ten32)) {
                        new MyDialog("Sửa thành công!", MyDialog.SUCCESS_DIALOG);
                        return true;
                } else {
                        new MyDialog("Sửa thất bại!", MyDialog.ERROR_DIALOG);
                        return false;
                }
        }

}
