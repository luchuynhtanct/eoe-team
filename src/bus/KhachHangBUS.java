package bus;

import custom.MyDialog;
import dao.KhachHangDAO;
import java.util.ArrayList;
import model.KhachHang;

public class KhachHangBUS {

        private ArrayList<KhachHang> listKhachHang62 = null;
        private KhachHangDAO kh62achHangDAO62 = new KhachHangDAO();

        public void docDanhSach() {
                this.listKhachHang62 = kh62achHangDAO62.getListKhachHang();
        }

        public ArrayList<KhachHang> getListKhachHang() {
                if (listKhachHang62 == null) {
                        docDanhSach();
                }
                return listKhachHang62;
        }

        public ArrayList<KhachHang> timKiemKhachHang(String txtMin62, String txtMax62) {
                if (txtMax62.trim().equals("") && txtMin62.trim().equals("")) {
                        return listKhachHang62;
                }
                try {
                        ArrayList<KhachHang> dskh62 = new ArrayList<>();
                        txtMin62 = txtMin62.replace(",", "");
                        txtMax62 = txtMax62.replace(",", "");
                        int min62 = Integer.parseInt(txtMin62);
                        int max62 = Integer.parseInt(txtMax62);
                        for (KhachHang kh62 : listKhachHang62) {
                                if (kh62.getTongChiTieu() >= min62 && kh62.getTongChiTieu() <= max62) {
                                        dskh62.add(kh62);
                                }
                        }
                        return dskh62;
                } catch (Exception e) {
                        new MyDialog("Hãy nhập giá trị nguyên phù hợp!", MyDialog.ERROR_DIALOG);
                }
                return null;
        }

        public ArrayList<KhachHang> timKiemKhachHang(String tuKho62a62) {
                tuKho62a62 = tuKho62a62.toLowerCase();
                ArrayList<KhachHang> dskh62 = new ArrayList<>();
                for (KhachHang kh62 : listKhachHang62) {
                        String ho62 = kh62.getHo().toLowerCase();
                        String ten62 = kh62.getTen().toLowerCase();
                        String gioiTinh62 = kh62.getGioiTinh().toLowerCase();
                        if (ho62.contains(tuKho62a62) || ten62.contains(tuKho62a62) || gioiTinh62.contains(tuKho62a62)) {
                                dskh62.add(kh62);
                        }
                }
                return dskh62;
        }

        public boolean themKhachHang(String ho62, String ten62, String gioiTinh62) {
                if (ten62.trim().equals("")) {
                        new MyDialog("Không được để trống tên!", MyDialog.ERROR_DIALOG);
                        return false;
                }
                if (gioiTinh62.equals("Chọn giới tính")) {
                        new MyDialog("Hãy chọn giới tính!", MyDialog.ERROR_DIALOG);
                        return false;
                }
                KhachHang kh62 = new KhachHang();
                kh62.setHo(ho62);
                kh62.setTen(ten62);
                kh62.setGioiTinh(gioiTinh62);
                kh62.setTongChiTieu(0);
                boolean flag62 = kh62achHangDAO62.addKhachHang(kh62);
                if (flag62) {
                        new MyDialog("Thêm thành công!", MyDialog.SUCCESS_DIALOG);
                } else {
                        new MyDialog("Thêm thất bại!", MyDialog.ERROR_DIALOG);
                }
                return flag62;
        }

        public boolean suaKhachHang(String ma, String ho62, String ten62, String gioiTinh62) {
                if (ma.equals("")) {
                        new MyDialog("Hãy chọn khách hàng cần sửa!", MyDialog.ERROR_DIALOG);
                        return false;
                }
                if (ten62.trim().equals("")) {
                        new MyDialog("Không được để trống tên!", MyDialog.ERROR_DIALOG);
                        return false;
                }
                if (gioiTinh62.equals("Chọn giới tính")) {
                        new MyDialog("Hãy chọn giới tính!", MyDialog.ERROR_DIALOG);
                        return false;
                }
                KhachHang kh62 = new KhachHang();
                kh62.setHo(ho62);
                kh62.setTen(ten62);
                kh62.setGioiTinh(gioiTinh62);
                boolean flag62 = kh62achHangDAO62.updateKhachHang(Integer.parseInt(ma), kh62);
                if (flag62) {
                        new MyDialog("Sửa thành công!", MyDialog.SUCCESS_DIALOG);
                } else {
                        new MyDialog("Sửa thất bại!", MyDialog.ERROR_DIALOG);
                }
                return flag62;
        }

        public boolean xoaKhachHang(String ma) {
                boolean flag62 = false;
                try {
                        int maKH = Integer.parseInt(ma);
                        MyDialog dlg62 = new MyDialog("Bạn có chắc chắn muốn xoá?", MyDialog.WARNING_DIALOG);
                        if (dlg62.getAction() == MyDialog.CANCEL_OPTION) {
                                return false;
                        }
                        flag62 = kh62achHangDAO62.deleteKhachHang(maKH);
                } catch (Exception e) {
                        new MyDialog("Chưa chọn khách hàng!", MyDialog.ERROR_DIALOG);
                }
                if (flag62) {
                        new MyDialog("Xoá thành công!", MyDialog.SUCCESS_DIALOG);
                } else {
                        new MyDialog("Xoá thất bại!", MyDialog.ERROR_DIALOG);
                }
                return flag62;
        }
}
