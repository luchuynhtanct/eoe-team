package bus;

import dao.ThongKeDAO;
import model.ThongKe;

public class ThongKeBUS {

        public ThongKeDAO thongKeDAO62 = new ThongKeDAO();

        public ThongKe thongKe(int nam62) {
                return thongKeDAO62.getThongKe(nam62);
        }

        public double getDoanhThuThang(int thang, int nam) {
                return thongKeDAO62.getDoanhThuThang(thang, nam);
        }
}
