package main;

import view.DangNhapJFrame;

public class Main {

        public static void main(String[] args) {
                DangNhapJFrame login62 = new DangNhapJFrame();
                login62.showWindow();
        }

        public static void changLNF(String nameLNF62) {
                try {
                        for (javax.swing.UIManager.LookAndFeelInfo info62 : javax.swing.UIManager.getInstalledLookAndFeels()) {
                                if (nameLNF62.equals(info62.getName())) {
                                        javax.swing.UIManager.setLookAndFeel(info62.getClassName());
                                        break;
                                }
                        }
                } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
                }
        }
}
