package view.JThongKe;

import bean.DanhMucBean;
import controller.ChuyenManHinhController;
import java.util.ArrayList;
import java.util.List;

public class ThongKeMenuJpanel extends javax.swing.JPanel {

        public ThongKeMenuJpanel() {
                initComponents();

                ChuyenManHinhController controller62 = new ChuyenManHinhController(jPanelView50);
                controller62.setViewMenuChart(jPanelBack30, jLabelBack30);

                List<DanhMucBean> listItem62 = new ArrayList<>();
                listItem62.add(new DanhMucBean("ViewChart", jPanelChart50, jLabelViewChart50));
                listItem62.add(new DanhMucBean("ThongKe", jPanelBack30, jLabelBack30));

                controller62.setEvent(listItem62);
        }

        @SuppressWarnings("unchecked")
        // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
        private void initComponents() {

                jPanelRoot62 = new javax.swing.JPanel();
                jPanelMenu62 = new javax.swing.JPanel();
                jPanelBack30 = new javax.swing.JPanel();
                jLabelBack30 = new javax.swing.JLabel();
                jPanelChart50 = new javax.swing.JPanel();
                jLabelViewChart50 = new javax.swing.JLabel();
                jPanelView50 = new javax.swing.JPanel();

                setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

                jPanelRoot62.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

                jPanelMenu62.setBackground(new java.awt.Color(56, 56, 56));
                jPanelMenu62.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

                jPanelBack30.setBackground(new java.awt.Color(56, 56, 56));
                jPanelBack30.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
                jPanelBack30.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

                jLabelBack30.setBackground(new java.awt.Color(56, 56, 56));
                jLabelBack30.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ManagerUI/flat-color-icons_pie-chart.png"))); // NOI18N
                jPanelBack30.add(jLabelBack30, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 30, 30));

                jPanelMenu62.add(jPanelBack30, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 0, 30, 30));

                jPanelChart50.setBackground(new java.awt.Color(56, 56, 56));
                jPanelChart50.setForeground(new java.awt.Color(60, 63, 65));
                jPanelChart50.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
                jPanelChart50.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

                jLabelViewChart50.setBackground(new java.awt.Color(56, 56, 56));
                jLabelViewChart50.setForeground(new java.awt.Color(60, 63, 65));
                jLabelViewChart50.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ManagerUI/emojione_bar-chart.png"))); // NOI18N
                jLabelViewChart50.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
                jPanelChart50.add(jLabelViewChart50, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

                jPanelMenu62.add(jPanelChart50, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 0, 30, 30));

                jPanelRoot62.add(jPanelMenu62, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1200, 30));

                jPanelView50.setBackground(new java.awt.Color(56, 56, 56));
                jPanelView50.setMinimumSize(new java.awt.Dimension(1110, 630));
                jPanelView50.setPreferredSize(new java.awt.Dimension(1200, 655));
                jPanelView50.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
                jPanelRoot62.add(jPanelView50, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 30, 1200, 690));

                add(jPanelRoot62, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1200, 720));
        }// </editor-fold>//GEN-END:initComponents

        // Variables declaration - do not modify//GEN-BEGIN:variables
        private javax.swing.JLabel jLabelBack30;
        private javax.swing.JLabel jLabelViewChart50;
        private javax.swing.JPanel jPanelBack30;
        private javax.swing.JPanel jPanelChart50;
        private javax.swing.JPanel jPanelMenu62;
        private javax.swing.JPanel jPanelRoot62;
        private javax.swing.JPanel jPanelView50;
        // End of variables declaration//GEN-END:variables
}
