package view.JThongKe;

import bus.ThongKeBUS;
import java.awt.Color;
import java.util.Calendar;
import model.ThongKe;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;

public class BieuDoJPanel extends javax.swing.JPanel {

        ThongKeBUS thongKeBUS62 = new ThongKeBUS();
        private ChartPanel chartPanel62;

        public BieuDoJPanel() {
                initComponents();

                chartPanel62 = new ChartPanel(createChart());
                chartPanel62.setBounds(0, 0, 1200, 370);
                jPanelChart62.add(chartPanel62);
                hienThiTop();
        }

        private JFreeChart createChart() {
                JFreeChart barChart62 = ChartFactory.createBarChart(
                        "Doanh thu năm " + Calendar.getInstance().get(Calendar.YEAR),
                        "Tháng", "Doanh thu",
                        createDataset(), PlotOrientation.VERTICAL, false, false, false);
                barChart62.setBackgroundPaint(new Color(240, 240, 240));
                return barChart62;
        }

        private CategoryDataset createDataset() {
                var dataset62 = new DefaultCategoryDataset();
                for (int i = 1; i <= 12; i++) {
                        double value62 = thongKeBUS62.getDoanhThuThang(i, Calendar.getInstance().get(Calendar.YEAR));
                        dataset62.setValue(value62, "Doanh thu", i + "");
                }
                return dataset62;
        }

        private void hienThiTop() {
                ThongKe thongKe62 = thongKeBUS62.thongKe(Calendar.YEAR);
                jLabelMon1_30.setText(thongKe62.getTopSanPhamBanChay().get(0).getTenSP());
                jLabelMon2_30.setText(thongKe62.getTopSanPhamBanChay().get(1).getTenSP());
                jLabelMon3_30.setText(thongKe62.getTopSanPhamBanChay().get(2).getTenSP());
                jLabelMon4_30.setText(thongKe62.getTopSanPhamBanChay().get(3).getTenSP());
                jLabelMon5_30.setText(thongKe62.getTopSanPhamBanChay().get(4).getTenSP());
                jLabelSL1_30.setText("" + thongKe62.getTopSanPhamBanChay().get(0).getSoLuong());
                jLabelSL2_30.setText("" + thongKe62.getTopSanPhamBanChay().get(1).getSoLuong());
                jLabelSL3_30.setText("" + thongKe62.getTopSanPhamBanChay().get(2).getSoLuong());
                jLabelSL4_30.setText("" + thongKe62.getTopSanPhamBanChay().get(3).getSoLuong());
                jLabelSL5_30.setText("" + thongKe62.getTopSanPhamBanChay().get(4).getSoLuong());
        }

        @SuppressWarnings("unchecked")
        // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
        private void initComponents() {

                jPanelView30 = new javax.swing.JPanel();
                ThucjPanel54 = new javax.swing.JPanel();
                jLabelMon1_30 = new javax.swing.JLabel();
                jLabelMon2_30 = new javax.swing.JLabel();
                jLabelMon3_30 = new javax.swing.JLabel();
                jLabelMon4_30 = new javax.swing.JLabel();
                jLabelMon5_30 = new javax.swing.JLabel();
                jLabelSL1_30 = new javax.swing.JLabel();
                jLabelSL2_30 = new javax.swing.JLabel();
                jLabelSL3_30 = new javax.swing.JLabel();
                jLabelSL4_30 = new javax.swing.JLabel();
                jLabelSL5_30 = new javax.swing.JLabel();
                BangjLabel50 = new javax.swing.JLabel();
                jPanelChart62 = new javax.swing.JPanel();

                setLayout(new java.awt.BorderLayout());

                jPanelView30.setBackground(new java.awt.Color(240, 240, 240));
                jPanelView30.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
                jPanelView30.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

                ThucjPanel54.setBackground(new java.awt.Color(240, 240, 240));
                ThucjPanel54.setPreferredSize(new java.awt.Dimension(354, 139));
                ThucjPanel54.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

                jLabelMon1_30.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
                jLabelMon1_30.setForeground(new java.awt.Color(0, 0, 0));
                jLabelMon1_30.setText("00");
                jLabelMon1_30.setHorizontalTextPosition(javax.swing.SwingConstants.LEADING);
                ThucjPanel54.add(jLabelMon1_30, new org.netbeans.lib.awtextra.AbsoluteConstraints(55, 93, 405, 41));

                jLabelMon2_30.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
                jLabelMon2_30.setForeground(new java.awt.Color(0, 0, 0));
                jLabelMon2_30.setText("00");
                jLabelMon2_30.setHorizontalTextPosition(javax.swing.SwingConstants.LEADING);
                ThucjPanel54.add(jLabelMon2_30, new org.netbeans.lib.awtextra.AbsoluteConstraints(55, 134, 405, 41));

                jLabelMon3_30.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
                jLabelMon3_30.setForeground(new java.awt.Color(0, 0, 0));
                jLabelMon3_30.setText("00");
                jLabelMon3_30.setHorizontalTextPosition(javax.swing.SwingConstants.LEADING);
                ThucjPanel54.add(jLabelMon3_30, new org.netbeans.lib.awtextra.AbsoluteConstraints(55, 175, 405, 41));

                jLabelMon4_30.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
                jLabelMon4_30.setForeground(new java.awt.Color(0, 0, 0));
                jLabelMon4_30.setText("00");
                jLabelMon4_30.setHorizontalTextPosition(javax.swing.SwingConstants.LEADING);
                ThucjPanel54.add(jLabelMon4_30, new org.netbeans.lib.awtextra.AbsoluteConstraints(55, 216, 405, 41));

                jLabelMon5_30.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
                jLabelMon5_30.setForeground(new java.awt.Color(0, 0, 0));
                jLabelMon5_30.setText("00");
                jLabelMon5_30.setHorizontalTextPosition(javax.swing.SwingConstants.LEADING);
                ThucjPanel54.add(jLabelMon5_30, new org.netbeans.lib.awtextra.AbsoluteConstraints(55, 257, 405, 41));

                jLabelSL1_30.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
                jLabelSL1_30.setForeground(new java.awt.Color(0, 0, 0));
                jLabelSL1_30.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
                jLabelSL1_30.setText("00");
                jLabelSL1_30.setHorizontalTextPosition(javax.swing.SwingConstants.LEADING);
                ThucjPanel54.add(jLabelSL1_30, new org.netbeans.lib.awtextra.AbsoluteConstraints(461, 93, 105, 41));

                jLabelSL2_30.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
                jLabelSL2_30.setForeground(new java.awt.Color(0, 0, 0));
                jLabelSL2_30.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
                jLabelSL2_30.setText("00");
                jLabelSL2_30.setHorizontalTextPosition(javax.swing.SwingConstants.LEADING);
                ThucjPanel54.add(jLabelSL2_30, new org.netbeans.lib.awtextra.AbsoluteConstraints(461, 134, 105, 41));

                jLabelSL3_30.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
                jLabelSL3_30.setForeground(new java.awt.Color(0, 0, 0));
                jLabelSL3_30.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
                jLabelSL3_30.setText("00");
                jLabelSL3_30.setHorizontalTextPosition(javax.swing.SwingConstants.LEADING);
                ThucjPanel54.add(jLabelSL3_30, new org.netbeans.lib.awtextra.AbsoluteConstraints(461, 175, 105, 41));

                jLabelSL4_30.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
                jLabelSL4_30.setForeground(new java.awt.Color(0, 0, 0));
                jLabelSL4_30.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
                jLabelSL4_30.setText("00");
                jLabelSL4_30.setHorizontalTextPosition(javax.swing.SwingConstants.LEADING);
                ThucjPanel54.add(jLabelSL4_30, new org.netbeans.lib.awtextra.AbsoluteConstraints(461, 216, 105, 41));

                jLabelSL5_30.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
                jLabelSL5_30.setForeground(new java.awt.Color(0, 0, 0));
                jLabelSL5_30.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
                jLabelSL5_30.setText("00");
                jLabelSL5_30.setHorizontalTextPosition(javax.swing.SwingConstants.LEADING);
                ThucjPanel54.add(jLabelSL5_30, new org.netbeans.lib.awtextra.AbsoluteConstraints(461, 257, 105, 41));

                BangjLabel50.setBackground(new java.awt.Color(245, 246, 187));
                BangjLabel50.setFont(new java.awt.Font("Tahoma", 1, 48)); // NOI18N
                BangjLabel50.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ManagerUI/bangChiTiet.png"))); // NOI18N
                BangjLabel50.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
                BangjLabel50.setIconTextGap(-125);
                BangjLabel50.setVerticalTextPosition(javax.swing.SwingConstants.TOP);
                ThucjPanel54.add(BangjLabel50, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

                jPanelView30.add(ThucjPanel54, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 5, 567, 300));

                jPanelChart62.setBackground(new java.awt.Color(240, 240, 240));

                javax.swing.GroupLayout jPanelChart62Layout = new javax.swing.GroupLayout(jPanelChart62);
                jPanelChart62.setLayout(jPanelChart62Layout);
                jPanelChart62Layout.setHorizontalGroup(
                        jPanelChart62Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 1200, Short.MAX_VALUE)
                );
                jPanelChart62Layout.setVerticalGroup(
                        jPanelChart62Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 370, Short.MAX_VALUE)
                );

                jPanelView30.add(jPanelChart62, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 320, 1200, 370));

                add(jPanelView30, java.awt.BorderLayout.CENTER);
        }// </editor-fold>//GEN-END:initComponents

        // Variables declaration - do not modify//GEN-BEGIN:variables
        private javax.swing.JLabel BangjLabel50;
        private javax.swing.JPanel ThucjPanel54;
        private javax.swing.JLabel jLabelMon1_30;
        private javax.swing.JLabel jLabelMon2_30;
        private javax.swing.JLabel jLabelMon3_30;
        private javax.swing.JLabel jLabelMon4_30;
        private javax.swing.JLabel jLabelMon5_30;
        private javax.swing.JLabel jLabelSL1_30;
        private javax.swing.JLabel jLabelSL2_30;
        private javax.swing.JLabel jLabelSL3_30;
        private javax.swing.JLabel jLabelSL4_30;
        private javax.swing.JLabel jLabelSL5_30;
        private javax.swing.JPanel jPanelChart62;
        private javax.swing.JPanel jPanelView30;
        // End of variables declaration//GEN-END:variables
}
