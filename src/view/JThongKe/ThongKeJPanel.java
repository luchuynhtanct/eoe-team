package view.JThongKe;

import bus.ThongKeBUS;
import java.text.DecimalFormat;
import java.util.Calendar;
import model.ThongKe;

public class ThongKeJPanel extends javax.swing.JPanel {

        ThongKeBUS thongKeBUS62 = new ThongKeBUS();

        public ThongKeJPanel() {
                initComponents();

                loadCmbNam();
                hienThiThongKe();
        }

        @SuppressWarnings("unchecked")
        // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
        private void initComponents() {

                jScrollPane1 = new javax.swing.JScrollPane();
                jTable1 = new javax.swing.JTable();
                jPanelView50 = new javax.swing.JPanel();
                ThongKejLabel50 = new javax.swing.JLabel();
                ThucjPanel50 = new javax.swing.JPanel();
                jLabelMon50 = new javax.swing.JLabel();
                MonanjLabel50 = new javax.swing.JLabel();
                KhachjPanel50 = new javax.swing.JPanel();
                jLabelKhach50 = new javax.swing.JLabel();
                KhachjLabel50 = new javax.swing.JLabel();
                NhanVienjPanel50 = new javax.swing.JPanel();
                jLabelNV50 = new javax.swing.JLabel();
                NVjLabel50 = new javax.swing.JLabel();
                TongjPanel50 = new javax.swing.JPanel();
                jLabelTong50 = new javax.swing.JLabel();
                TongjLabel50 = new javax.swing.JLabel();
                yearjComboBox50 = new javax.swing.JComboBox<>();
                BangjPanel50 = new javax.swing.JPanel();
                jLabelQuy1_50 = new javax.swing.JLabel();
                jLabelQuy2_50 = new javax.swing.JLabel();
                jLabelQuy3_50 = new javax.swing.JLabel();
                jLabelQuy4_50 = new javax.swing.JLabel();
                jLabelTongCong_50 = new javax.swing.JLabel();
                BangjLabel50 = new javax.swing.JLabel();

                jTable1.setModel(new javax.swing.table.DefaultTableModel(
                        new Object [][] {
                                {null, null, null, null},
                                {null, null, null, null},
                                {null, null, null, null},
                                {null, null, null, null}
                        },
                        new String [] {
                                "Title 1", "Title 2", "Title 3", "Title 4"
                        }
                ));
                jScrollPane1.setViewportView(jTable1);

                setMinimumSize(new java.awt.Dimension(1110, 630));
                setPreferredSize(new java.awt.Dimension(1200, 690));

                jPanelView50.setBackground(new java.awt.Color(56, 56, 56));
                jPanelView50.setMinimumSize(new java.awt.Dimension(1110, 630));
                jPanelView50.setPreferredSize(new java.awt.Dimension(1200, 655));
                jPanelView50.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

                ThongKejLabel50.setFont(new java.awt.Font("Tahoma", 1, 28)); // NOI18N
                ThongKejLabel50.setForeground(new java.awt.Color(255, 153, 0));
                ThongKejLabel50.setText("THỐNG KÊ TỔNG QUÁT");
                jPanelView50.add(ThongKejLabel50, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 20, -1, -1));

                ThucjPanel50.setBackground(new java.awt.Color(56, 56, 56));
                ThucjPanel50.setPreferredSize(new java.awt.Dimension(354, 139));
                ThucjPanel50.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

                jLabelMon50.setFont(new java.awt.Font("Tahoma", 1, 42)); // NOI18N
                jLabelMon50.setForeground(new java.awt.Color(255, 255, 255));
                jLabelMon50.setText("00");
                ThucjPanel50.add(jLabelMon50, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 20, -1, -1));

                MonanjLabel50.setBackground(new java.awt.Color(56, 56, 56));
                MonanjLabel50.setFont(new java.awt.Font("Tahoma", 1, 48)); // NOI18N
                MonanjLabel50.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ManagerUI/thongKeMon.png"))); // NOI18N
                MonanjLabel50.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
                MonanjLabel50.setIconTextGap(-125);
                MonanjLabel50.setVerticalTextPosition(javax.swing.SwingConstants.TOP);
                ThucjPanel50.add(MonanjLabel50, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

                jPanelView50.add(ThucjPanel50, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 80, 275, 150));

                KhachjPanel50.setBackground(new java.awt.Color(56, 56, 56));
                KhachjPanel50.setPreferredSize(new java.awt.Dimension(354, 139));
                KhachjPanel50.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

                jLabelKhach50.setFont(new java.awt.Font("Tahoma", 1, 42)); // NOI18N
                jLabelKhach50.setForeground(new java.awt.Color(255, 255, 255));
                jLabelKhach50.setText("00");
                KhachjPanel50.add(jLabelKhach50, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 20, -1, -1));

                KhachjLabel50.setBackground(new java.awt.Color(56, 56, 56));
                KhachjLabel50.setFont(new java.awt.Font("Tahoma", 1, 48)); // NOI18N
                KhachjLabel50.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ManagerUI/thongKeKhachHang.png"))); // NOI18N
                KhachjLabel50.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
                KhachjLabel50.setIconTextGap(-125);
                KhachjLabel50.setVerticalTextPosition(javax.swing.SwingConstants.TOP);
                KhachjPanel50.add(KhachjLabel50, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

                jPanelView50.add(KhachjPanel50, new org.netbeans.lib.awtextra.AbsoluteConstraints(650, 80, 275, 150));

                NhanVienjPanel50.setBackground(new java.awt.Color(56, 56, 56));
                NhanVienjPanel50.setPreferredSize(new java.awt.Dimension(354, 139));
                NhanVienjPanel50.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

                jLabelNV50.setFont(new java.awt.Font("Tahoma", 1, 42)); // NOI18N
                jLabelNV50.setForeground(new java.awt.Color(255, 255, 255));
                jLabelNV50.setText("00");
                NhanVienjPanel50.add(jLabelNV50, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 20, -1, -1));

                NVjLabel50.setBackground(new java.awt.Color(56, 56, 56));
                NVjLabel50.setFont(new java.awt.Font("Tahoma", 1, 48)); // NOI18N
                NVjLabel50.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ManagerUI/thongKeNhanVien.png"))); // NOI18N
                NVjLabel50.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
                NVjLabel50.setIconTextGap(-125);
                NVjLabel50.setVerticalTextPosition(javax.swing.SwingConstants.TOP);
                NhanVienjPanel50.add(NVjLabel50, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

                jPanelView50.add(NhanVienjPanel50, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 290, 275, 150));

                TongjPanel50.setBackground(new java.awt.Color(56, 56, 56));
                TongjPanel50.setPreferredSize(new java.awt.Dimension(354, 139));
                TongjPanel50.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

                jLabelTong50.setFont(new java.awt.Font("Tahoma", 1, 42)); // NOI18N
                jLabelTong50.setForeground(new java.awt.Color(255, 255, 255));
                jLabelTong50.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
                jLabelTong50.setText("00");
                jLabelTong50.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
                TongjPanel50.add(jLabelTong50, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 20, 275, -1));

                TongjLabel50.setBackground(new java.awt.Color(56, 56, 56));
                TongjLabel50.setFont(new java.awt.Font("Tahoma", 1, 48)); // NOI18N
                TongjLabel50.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ManagerUI/thongKeDoanhThu.png"))); // NOI18N
                TongjLabel50.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
                TongjLabel50.setIconTextGap(-125);
                TongjLabel50.setVerticalTextPosition(javax.swing.SwingConstants.TOP);
                TongjPanel50.add(TongjLabel50, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

                jPanelView50.add(TongjPanel50, new org.netbeans.lib.awtextra.AbsoluteConstraints(650, 290, 275, 150));

                yearjComboBox50.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
                yearjComboBox50.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                yearjComboBox50ActionPerformed(evt);
                        }
                });
                jPanelView50.add(yearjComboBox50, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 460, 110, 30));

                BangjPanel50.setBackground(new java.awt.Color(56, 56, 56));
                BangjPanel50.setPreferredSize(new java.awt.Dimension(354, 139));
                BangjPanel50.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

                jLabelQuy1_50.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
                jLabelQuy1_50.setForeground(new java.awt.Color(255, 255, 255));
                jLabelQuy1_50.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
                jLabelQuy1_50.setText("00");
                BangjPanel50.add(jLabelQuy1_50, new org.netbeans.lib.awtextra.AbsoluteConstraints(137, 50, 133, 51));

                jLabelQuy2_50.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
                jLabelQuy2_50.setForeground(new java.awt.Color(255, 255, 255));
                jLabelQuy2_50.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
                jLabelQuy2_50.setText("00");
                BangjPanel50.add(jLabelQuy2_50, new org.netbeans.lib.awtextra.AbsoluteConstraints(272, 50, 133, 51));

                jLabelQuy3_50.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
                jLabelQuy3_50.setForeground(new java.awt.Color(255, 255, 255));
                jLabelQuy3_50.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
                jLabelQuy3_50.setText("00");
                BangjPanel50.add(jLabelQuy3_50, new org.netbeans.lib.awtextra.AbsoluteConstraints(407, 50, 133, 51));

                jLabelQuy4_50.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
                jLabelQuy4_50.setForeground(new java.awt.Color(255, 255, 255));
                jLabelQuy4_50.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
                jLabelQuy4_50.setText("00");
                BangjPanel50.add(jLabelQuy4_50, new org.netbeans.lib.awtextra.AbsoluteConstraints(542, 50, 133, 51));

                jLabelTongCong_50.setFont(new java.awt.Font("Tahoma", 1, 26)); // NOI18N
                jLabelTongCong_50.setForeground(new java.awt.Color(255, 255, 255));
                jLabelTongCong_50.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
                jLabelTongCong_50.setText("00");
                BangjPanel50.add(jLabelTongCong_50, new org.netbeans.lib.awtextra.AbsoluteConstraints(137, 101, 537, 51));

                BangjLabel50.setBackground(new java.awt.Color(56, 56, 56));
                BangjLabel50.setFont(new java.awt.Font("Tahoma", 1, 48)); // NOI18N
                BangjLabel50.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ManagerUI/bangThongKe.png"))); // NOI18N
                BangjLabel50.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
                BangjLabel50.setIconTextGap(-125);
                BangjLabel50.setVerticalTextPosition(javax.swing.SwingConstants.TOP);
                BangjPanel50.add(BangjLabel50, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

                jPanelView50.add(BangjPanel50, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 510, 675, 153));

                javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
                this.setLayout(layout);
                layout.setHorizontalGroup(
                        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addComponent(jPanelView50, javax.swing.GroupLayout.PREFERRED_SIZE, 1200, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                );
                layout.setVerticalGroup(
                        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addComponent(jPanelView50, javax.swing.GroupLayout.PREFERRED_SIZE, 690, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                );
        }// </editor-fold>//GEN-END:initComponents

        private void yearjComboBox50ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_yearjComboBox50ActionPerformed
                // TODO add your handling code here:
                hienThiThongKe();
        }//GEN-LAST:event_yearjComboBox50ActionPerformed

        private DecimalFormat dcf = new DecimalFormat("###,###");

        private void hienThiThongKe() {
                ThongKe thongKe62 = thongKeBUS62.thongKe(Integer.parseInt(yearjComboBox50.getSelectedItem() + ""));
                jLabelMon50.setText(dcf.format(thongKe62.getSoLuongSP()));
                jLabelKhach50.setText(dcf.format(thongKe62.getSoLuongKH()));
                jLabelNV50.setText(dcf.format(thongKe62.getSoLuongNV()));
                jLabelTong50.setText(dcf.format(thongKe62.getTongDoanhThu()));
                jLabelQuy1_50.setText(dcf.format(thongKe62.getTongThuQuy(1)));
                jLabelQuy2_50.setText(dcf.format(thongKe62.getTongThuQuy(2)));
                jLabelQuy3_50.setText(dcf.format(thongKe62.getTongThuQuy(3)));
                jLabelQuy4_50.setText(dcf.format(thongKe62.getTongThuQuy(4)));
                jLabelTongCong_50.setText(dcf.format(thongKe62.getTongDoanhThu()));
        }

        private void loadCmbNam() {
                int year = Calendar.getInstance().get(Calendar.YEAR);
                for (int i = year; i >= year - 10; i--) {
                        yearjComboBox50.addItem(i);
                }
        }

        public int NamCmb() {
                return (int) yearjComboBox50.getSelectedItem();
        }

        // Variables declaration - do not modify//GEN-BEGIN:variables
        private javax.swing.JLabel BangjLabel50;
        private javax.swing.JPanel BangjPanel50;
        private javax.swing.JLabel KhachjLabel50;
        private javax.swing.JPanel KhachjPanel50;
        private javax.swing.JLabel MonanjLabel50;
        private javax.swing.JLabel NVjLabel50;
        private javax.swing.JPanel NhanVienjPanel50;
        private javax.swing.JLabel ThongKejLabel50;
        private javax.swing.JPanel ThucjPanel50;
        private javax.swing.JLabel TongjLabel50;
        private javax.swing.JPanel TongjPanel50;
        private javax.swing.JLabel jLabelKhach50;
        private javax.swing.JLabel jLabelMon50;
        private javax.swing.JLabel jLabelNV50;
        private javax.swing.JLabel jLabelQuy1_50;
        private javax.swing.JLabel jLabelQuy2_50;
        private javax.swing.JLabel jLabelQuy3_50;
        private javax.swing.JLabel jLabelQuy4_50;
        private javax.swing.JLabel jLabelTong50;
        private javax.swing.JLabel jLabelTongCong_50;
        private javax.swing.JPanel jPanelView50;
        private javax.swing.JScrollPane jScrollPane1;
        private javax.swing.JTable jTable1;
        private javax.swing.JComboBox<Integer> yearjComboBox50;
        // End of variables declaration//GEN-END:variables
}
