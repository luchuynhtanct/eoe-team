package view.BanHang;

import bus.KhachHangBUS;
import custom.MyTable;
import java.util.ArrayList;
import java.util.Vector;
import javax.swing.JLabel;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableRowSorter;
import model.KhachHang;

public class TimKhachJDialog extends javax.swing.JDialog {

        private DefaultTableModel dtmKhachHang62;
        private KhachHangBUS kh62BUS62;
        public static KhachHang kh62achHangTimDuoc62 = null;

        public TimKhachJDialog() {
                initComponents();

                this.setSize(500, 400);
                this.setModal(true);
                this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
                this.setLocationRelativeTo(null);

                DefaultTableCellRenderer centerRenderer62 = new DefaultTableCellRenderer();
                centerRenderer62.setHorizontalAlignment(JLabel.CENTER);
                kh62BUS62 = new KhachHangBUS();

                new MyTable(jTableKhachHang62);
                dtmKhachHang62 = new DefaultTableModel() {
                        @Override
                        public boolean isCellEditable(int row62, int column) {
                                return false;
                        }
                };
                jTableKhachHang62.setModel(dtmKhachHang62);

                dtmKhachHang62.addColumn("Mã KH");
                dtmKhachHang62.addColumn("Họ");
                dtmKhachHang62.addColumn("Tên");
                dtmKhachHang62.addColumn("Giới tính");
                dtmKhachHang62.addColumn("Tổng chi tiêu");

                jTableKhachHang62.getColumnModel().getColumn(0).setCellRenderer(centerRenderer62);
                jTableKhachHang62.getColumnModel().getColumn(1).setCellRenderer(centerRenderer62);
                jTableKhachHang62.getColumnModel().getColumn(2).setCellRenderer(centerRenderer62);
                jTableKhachHang62.getColumnModel().getColumn(3).setCellRenderer(centerRenderer62);
                jTableKhachHang62.getColumnModel().getColumn(4).setCellRenderer(centerRenderer62);
                TableColumnModel columnModelKhachHang62 = jTableKhachHang62.getColumnModel();
                columnModelKhachHang62.getColumn(0).setPreferredWidth(77);
                columnModelKhachHang62.getColumn(1).setPreferredWidth(150);
                columnModelKhachHang62.getColumn(2).setPreferredWidth(110);
                columnModelKhachHang62.getColumn(3).setPreferredWidth(110);
                columnModelKhachHang62.getColumn(4).setPreferredWidth(150);

                loadDataLenTable();
        }

        private void loadDataLenTable() {
                dtmKhachHang62.setRowCount(0);
                ArrayList<KhachHang> dskh6262 = kh62BUS62.getListKhachHang();
                if (dskh6262 != null) {
                        for (KhachHang kh62 : dskh6262) {
                                Vector vec62 = new Vector();
                                vec62.add(kh62.getMaKH());
                                vec62.add(kh62.getHo());
                                vec62.add(kh62.getTen());
                                vec62.add(kh62.getGioiTinh());
                                vec62.add(kh62.getTongChiTieu());
                                dtmKhachHang62.addRow(vec62);
                        }
                }
        }

        @SuppressWarnings("unchecked")
        // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
        private void initComponents() {

                jPanelView62 = new javax.swing.JPanel();
                jLabelSearch62 = new javax.swing.JLabel();
                jTextFieldTimKiem62 = new javax.swing.JTextField();
                jScrollPaneKH62 = new javax.swing.JScrollPane();
                jTableKhachHang62 = new javax.swing.JTable();
                jButtonChon62 = new javax.swing.JButton();
                jButtonThem62 = new javax.swing.JButton();

                setTitle("Chọn khách hàng");
                setResizable(false);
                setSize(new java.awt.Dimension(500, 360));
                getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

                jPanelView62.setPreferredSize(new java.awt.Dimension(500, 360));
                jPanelView62.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

                jLabelSearch62.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
                jLabelSearch62.setForeground(new java.awt.Color(102, 51, 0));
                jLabelSearch62.setText("Tìm Kiếm");
                jPanelView62.add(jLabelSearch62, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 12, -1, -1));

                jTextFieldTimKiem62.setBackground(new java.awt.Color(255, 255, 255));
                jTextFieldTimKiem62.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
                jTextFieldTimKiem62.setForeground(new java.awt.Color(0, 0, 0));
                jTextFieldTimKiem62.addKeyListener(new java.awt.event.KeyAdapter() {
                        public void keyReleased(java.awt.event.KeyEvent evt) {
                                jTextFieldTimKiem62KeyReleased(evt);
                        }
                });
                jPanelView62.add(jTextFieldTimKiem62, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 10, 160, -1));

                jTableKhachHang62.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
                jTableKhachHang62.setModel(new javax.swing.table.DefaultTableModel(
                        new Object [][] {
                                {null, null, null, null},
                                {null, null, null, null},
                                {null, null, null, null},
                                {null, null, null, null}
                        },
                        new String [] {
                                "Title 1", "Title 2", "Title 3", "Title 4"
                        }
                ));
                jTableKhachHang62.setGridColor(new java.awt.Color(128, 128, 128));
                jTableKhachHang62.setRowHeight(25);
                jTableKhachHang62.setSelectionBackground(new java.awt.Color(50, 154, 114));
                jTableKhachHang62.setSelectionForeground(new java.awt.Color(255, 255, 255));
                jTableKhachHang62.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
                jTableKhachHang62.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
                jTableKhachHang62.setShowHorizontalLines(true);
                jScrollPaneKH62.setViewportView(jTableKhachHang62);

                jPanelView62.add(jScrollPaneKH62, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 40, 500, 280));

                jButtonChon62.setBackground(new java.awt.Color(255, 153, 0));
                jButtonChon62.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
                jButtonChon62.setForeground(new java.awt.Color(255, 255, 255));
                jButtonChon62.setText("OK");
                jButtonChon62.setBorder(null);
                jButtonChon62.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
                jButtonChon62.setFocusPainted(false);
                jButtonChon62.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                jButtonChon62ActionPerformed(evt);
                        }
                });
                jPanelView62.add(jButtonChon62, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 325, 100, 30));

                jButtonThem62.setBackground(new java.awt.Color(255, 153, 0));
                jButtonThem62.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
                jButtonThem62.setForeground(new java.awt.Color(255, 255, 255));
                jButtonThem62.setText("+ Thêm Khách");
                jButtonThem62.setBorder(null);
                jButtonThem62.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
                jButtonThem62.setFocusPainted(false);
                jButtonThem62.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                jButtonThem62ActionPerformed(evt);
                        }
                });
                jPanelView62.add(jButtonThem62, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 5, 100, 30));

                getContentPane().add(jPanelView62, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 500, 360));

                pack();
                setLocationRelativeTo(null);
        }// </editor-fold>//GEN-END:initComponents

        private void jButtonThem62ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonThem62ActionPerformed
                // TODO add your handling code here:
                ThemKhachJDialog dlg62 = new ThemKhachJDialog();
                dlg62.setVisible(true);
                if (dlg62.checkThemKhach62) {
                        kh62BUS62.docDanhSach();
                        loadDataLenTable();
                }
        }//GEN-LAST:event_jButtonThem62ActionPerformed

        private void jButtonChon62ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonChon62ActionPerformed
                // TODO add your handling code here:
                int row62 = jTableKhachHang62.getSelectedRow();
                if (row62 > -1) {
                        int ma62 = Integer.parseInt(jTableKhachHang62.getValueAt(row62, 0) + "");
                        String ho62 = jTableKhachHang62.getValueAt(row62, 1) + "";
                        String ten62 = jTableKhachHang62.getValueAt(row62, 2) + "";
                        String gioiTinh62 = jTableKhachHang62.getValueAt(row62, 3) + "";
                        int tongChiTieu62 = Integer.parseInt(jTableKhachHang62.getValueAt(row62, 4) + "");

                        kh62achHangTimDuoc62 = new KhachHang(ma62, ho62, ten62, gioiTinh62, tongChiTieu62);
                }
                this.dispose();
        }//GEN-LAST:event_jButtonChon62ActionPerformed

        private void jTextFieldTimKiem62KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldTimKiem62KeyReleased
                // TODO add your handling code here:
                searchTable(jTextFieldTimKiem62.getText());
        }//GEN-LAST:event_jTextFieldTimKiem62KeyReleased

        public void searchTable(String value) {
                TableRowSorter<DefaultTableModel> trs62 = new TableRowSorter<>(dtmKhachHang62);
                jTableKhachHang62.setRowSorter(trs62);
                trs62.setRowFilter(RowFilter.regexFilter(value));
        }

        // Variables declaration - do not modify//GEN-BEGIN:variables
        private javax.swing.JButton jButtonChon62;
        private javax.swing.JButton jButtonThem62;
        private javax.swing.JLabel jLabelSearch62;
        private javax.swing.JPanel jPanelView62;
        private javax.swing.JScrollPane jScrollPaneKH62;
        private javax.swing.JTable jTableKhachHang62;
        private javax.swing.JTextField jTextFieldTimKiem62;
        // End of variables declaration//GEN-END:variables
}
