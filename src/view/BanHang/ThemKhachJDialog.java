package view.BanHang;

import bus.KhachHangBUS;

public class ThemKhachJDialog extends javax.swing.JDialog {

        public ThemKhachJDialog() {
                this.setTitle("Thêm khách hàng");
                initComponents();
                cmbGioiTinh30.removeAllItems();
                cmbGioiTinh30.addItem("Chọn giới tính");
                cmbGioiTinh30.addItem("Nam");
                cmbGioiTinh30.addItem("Nữ");
                this.setModal(true);
                this.setLocationRelativeTo(null);
                this.setResizable(false);
        }

        @SuppressWarnings("unchecked")
        // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
        private void initComponents() {

                jLabelHoDem30 = new javax.swing.JLabel();
                txtHo30 = new javax.swing.JTextField();
                txtTen30 = new javax.swing.JTextField();
                jLabelTen30 = new javax.swing.JLabel();
                jLabelGT30 = new javax.swing.JLabel();
                cmbGioiTinh30 = new javax.swing.JComboBox<>();
                jPanelBT30 = new javax.swing.JPanel();
                jButtonThem30 = new javax.swing.JButton();
                jButtonThoat30 = new javax.swing.JButton();

                setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

                jLabelHoDem30.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
                jLabelHoDem30.setForeground(new java.awt.Color(102, 51, 0));
                jLabelHoDem30.setText("Họ đệm");

                txtHo30.setBackground(new java.awt.Color(255, 255, 255));
                txtHo30.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
                txtHo30.setForeground(new java.awt.Color(0, 0, 0));

                txtTen30.setBackground(new java.awt.Color(255, 255, 255));
                txtTen30.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
                txtTen30.setForeground(new java.awt.Color(0, 0, 0));

                jLabelTen30.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
                jLabelTen30.setForeground(new java.awt.Color(102, 51, 0));
                jLabelTen30.setText("Tên");

                jLabelGT30.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
                jLabelGT30.setForeground(new java.awt.Color(102, 51, 0));
                jLabelGT30.setText("Giới tính");

                cmbGioiTinh30.setBackground(new java.awt.Color(255, 255, 255));
                cmbGioiTinh30.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
                cmbGioiTinh30.setForeground(new java.awt.Color(0, 0, 0));
                cmbGioiTinh30.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

                jButtonThem30.setBackground(new java.awt.Color(255, 153, 0));
                jButtonThem30.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
                jButtonThem30.setForeground(new java.awt.Color(255, 255, 255));
                jButtonThem30.setText("Thêm");
                jButtonThem30.setBorder(null);
                jButtonThem30.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
                jButtonThem30.setFocusPainted(false);
                jButtonThem30.setPreferredSize(new java.awt.Dimension(100, 30));
                jButtonThem30.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                jButtonThem30ActionPerformed(evt);
                        }
                });
                jPanelBT30.add(jButtonThem30);

                jButtonThoat30.setBackground(new java.awt.Color(255, 153, 0));
                jButtonThoat30.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
                jButtonThoat30.setForeground(new java.awt.Color(255, 255, 255));
                jButtonThoat30.setText("Thoát");
                jButtonThoat30.setBorder(null);
                jButtonThoat30.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
                jButtonThoat30.setFocusPainted(false);
                jButtonThoat30.setPreferredSize(new java.awt.Dimension(100, 30));
                jButtonThoat30.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                jButtonThoat30ActionPerformed(evt);
                        }
                });
                jPanelBT30.add(jButtonThoat30);

                javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
                getContentPane().setLayout(layout);
                layout.setHorizontalGroup(
                        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                                .addComponent(jLabelHoDem30)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 28, Short.MAX_VALUE)
                                                .addComponent(txtHo30, javax.swing.GroupLayout.PREFERRED_SIZE, 303, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(jLabelTen30)
                                                        .addComponent(jLabelGT30))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                        .addComponent(txtTen30, javax.swing.GroupLayout.DEFAULT_SIZE, 303, Short.MAX_VALUE)
                                                        .addComponent(cmbGioiTinh30, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                                .addContainerGap())
                        .addComponent(jPanelBT30, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                );
                layout.setVerticalGroup(
                        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabelHoDem30)
                                        .addComponent(txtHo30, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabelTen30)
                                        .addComponent(txtTen30, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabelGT30)
                                        .addComponent(cmbGioiTinh30, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addComponent(jPanelBT30, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                );

                pack();
        }// </editor-fold>//GEN-END:initComponents

    private void jButtonThoat30ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonThoat30ActionPerformed
            this.dispose();
    }//GEN-LAST:event_jButtonThoat30ActionPerformed

        public boolean checkThemKhach62 = false;
    private void jButtonThem30ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonThem30ActionPerformed
            checkThemKhach62 = false;
            KhachHangBUS khachHangBUS62 = new KhachHangBUS();
            boolean flag62 = khachHangBUS62.themKhachHang(txtHo30.getText(), txtTen30.getText(), cmbGioiTinh30.getSelectedItem() + "");
            checkThemKhach62 = flag62;
            if (flag62) {
                    this.dispose();
            }
    }//GEN-LAST:event_jButtonThem30ActionPerformed

        // Variables declaration - do not modify//GEN-BEGIN:variables
        private javax.swing.JComboBox<String> cmbGioiTinh30;
        private javax.swing.JButton jButtonThem30;
        private javax.swing.JButton jButtonThoat30;
        private javax.swing.JLabel jLabelGT30;
        private javax.swing.JLabel jLabelHoDem30;
        private javax.swing.JLabel jLabelTen30;
        private javax.swing.JPanel jPanelBT30;
        private javax.swing.JTextField txtHo30;
        private javax.swing.JTextField txtTen30;
        // End of variables declaration//GEN-END:variables
}
