package view.BanHang;

import bus.DangNhapBUS;
import bus.LoaiBUS;
import bus.NhanVienBUS;
import bus.SanPhamBUS;
import custom.MyDialog;
import custom.MyTable;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Vector;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.SpinnerNumberModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableRowSorter;
import javax.swing.text.NumberFormatter;
import model.LoaiSP;
import model.NhanVien;
import model.SanPham;

public class BanHangMenuJPanel extends javax.swing.JPanel {

        private DefaultTableModel dtmSanPham62, dtmGioHang62;
        private SanPhamBUS spBUS62;
        private NhanVienBUS nvBUS62;
        private LoaiBUS loai62BUS62;
        private DecimalFormat dcf62;

        public BanHangMenuJPanel() {
                initComponents();

                DefaultTableCellRenderer centerRenderer62 = new DefaultTableCellRenderer();
                centerRenderer62.setHorizontalAlignment(JLabel.CENTER);
                dcf62 = new DecimalFormat("###,###");
                loai62BUS62 = new LoaiBUS();
                spBUS62 = new SanPhamBUS();
                nvBUS62 = new NhanVienBUS();

                SpinnerNumberModel modeSpinner62 = new SpinnerNumberModel(1, 1, 100, 1);
                jSpinnerSoLuong36.setModel(modeSpinner62);
                JFormattedTextField txtSpinner62 = ((JSpinner.NumberEditor) jSpinnerSoLuong36.getEditor()).getTextField();
                ((NumberFormatter) txtSpinner62.getFormatter()).setAllowsInvalid(false);
                txtSpinner62.setEditable(false);
                txtSpinner62.setHorizontalAlignment(JTextField.LEFT);
                jTextFieldMaSP36.setEditable(false);
                jTextFieldTenSP36.setEditable(false);
                jTextFieldDonGia36.setEditable(false);

                new MyTable(jTableDSSP36);
                dtmSanPham62 = new DefaultTableModel() {
                        @Override
                        public boolean isCellEditable(int row, int column) {
                                return false;
                        }
                };
                jTableDSSP36.setModel(dtmSanPham62);

                dtmSanPham62.addColumn("Mã SP");
                dtmSanPham62.addColumn("Tên SP");
                dtmSanPham62.addColumn("Đơn giá");
                dtmSanPham62.addColumn("Còn lại");
                dtmSanPham62.addColumn("Đơn vị tính");
                dtmSanPham62.addColumn("Ảnh");

                jTableDSSP36.getColumnModel().getColumn(0).setCellRenderer(centerRenderer62);
                jTableDSSP36.getColumnModel().getColumn(2).setCellRenderer(centerRenderer62);
                jTableDSSP36.getColumnModel().getColumn(3).setCellRenderer(centerRenderer62);
                jTableDSSP36.getColumnModel().getColumn(4).setCellRenderer(centerRenderer62);
                TableColumnModel columnModelBanHang62 = jTableDSSP36.getColumnModel();
                columnModelBanHang62.getColumn(0).setPreferredWidth(77);
                columnModelBanHang62.getColumn(1).setPreferredWidth(282);
                columnModelBanHang62.getColumn(2).setPreferredWidth(82);
                columnModelBanHang62.getColumn(3).setPreferredWidth(85);
                columnModelBanHang62.getColumn(4).setPreferredWidth(138);
                columnModelBanHang62.getColumn(5).setPreferredWidth(0);

                new MyTable(jTableGioHang36);
                dtmGioHang62 = new DefaultTableModel() {
                        @Override
                        public boolean isCellEditable(int row, int column) {
                                return false;
                        }
                };
                jTableGioHang36.setModel(dtmGioHang62);

                dtmGioHang62.addColumn("Mã SP");
                dtmGioHang62.addColumn("Tên SP");
                dtmGioHang62.addColumn("Số lượng");
                dtmGioHang62.addColumn("Đơn giá");
                dtmGioHang62.addColumn("Thành tiền");

                jTableGioHang36.getColumnModel().getColumn(0).setCellRenderer(centerRenderer62);
                jTableGioHang36.getColumnModel().getColumn(2).setCellRenderer(centerRenderer62);
                jTableGioHang36.getColumnModel().getColumn(3).setCellRenderer(centerRenderer62);
                jTableGioHang36.getColumnModel().getColumn(4).setCellRenderer(centerRenderer62);
                TableColumnModel columnModelGioHang62 = jTableGioHang36.getColumnModel();
                columnModelGioHang62.getColumn(0).setPreferredWidth(81);
                columnModelGioHang62.getColumn(1).setPreferredWidth(279);
                columnModelGioHang62.getColumn(2).setPreferredWidth(111);
                columnModelGioHang62.getColumn(3).setPreferredWidth(101);
                columnModelGioHang62.getColumn(4).setPreferredWidth(100);

                loadAnh("");
                loadLoaiSP();
                loadTableDataSP();
                loadDataComboboxNhanVienBan();
        }

        private void loadTableDataSP() {
                dtmSanPham62.setRowCount(0);
                ArrayList<SanPham> dssp62 = null;

                if (jComboBoxLoaiSP36.getItemCount() > 0) {
                        String loai62 = jComboBoxLoaiSP36.getSelectedItem() + "";
                        String loai62Arr[] = loai62.split("-");
                        String loai62SP = loai62Arr[0].trim();

                        if (loai62SP.equals("0")) {
                                dssp62 = spBUS62.getListSanPham();
                        } else {
                                dssp62 = spBUS62.getSanPhamTheoLoai(loai62SP);
                        }
                } else {
                        dssp62 = spBUS62.getListSanPham();
                }
                for (SanPham sp : dssp62) {
                        Vector vec62 = new Vector();
                        vec62.add(sp.getMaSP());
                        vec62.add(sp.getTenSP());
                        vec62.add(dcf62.format(sp.getDonGia()).replace(",", "."));
                        vec62.add(dcf62.format(sp.getSoLuong()).replace(",", "."));
                        vec62.add(sp.getDonViTinh());
                        vec62.add(sp.getHinhAnh());
                        dtmSanPham62.addRow(vec62);
                }
                jTableDSSP36.setPreferredSize(new Dimension(jTableDSSP36.getWidth(), jTableDSSP36.getRowHeight() * jTableDSSP36.getRowCount()));
        }

        private void loadLoaiSP() {
                jComboBoxLoaiSP36.removeAllItems();
                jComboBoxLoaiSP36.addItem("0 - Chọn loại");
                ArrayList<LoaiSP> dsl62 = loai62BUS62.getDanhSachLoai();

                for (LoaiSP loai62 : dsl62) {
                        jComboBoxLoaiSP36.addItem(loai62.getMaLoai() + " - " + loai62.getTenLoai());
                }
        }

        @SuppressWarnings("unchecked")
        // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
        private void initComponents() {

                jPanelView62 = new javax.swing.JPanel();
                jLabelDSSP36 = new javax.swing.JLabel();
                jScrollPaneSanPham62 = new javax.swing.JScrollPane();
                jTableDSSP36 = new javax.swing.JTable();
                jLabelGioHang36 = new javax.swing.JLabel();
                jScrollPaneGioHang36 = new javax.swing.JScrollPane();
                jTableGioHang36 = new javax.swing.JTable();
                jLabelCTSP36 = new javax.swing.JLabel();
                jComboBoxLoaiSP36 = new javax.swing.JComboBox<>();
                jLabelImageSP36 = new javax.swing.JLabel();
                jButtonXoaChonGH36 = new javax.swing.JButton();
                jButtonXuatHD36 = new javax.swing.JButton();
                jPanelTTCTSP36 = new javax.swing.JPanel();
                jLabelMaSP36 = new javax.swing.JLabel();
                jLabelNV36 = new javax.swing.JLabel();
                jLabelTen36 = new javax.swing.JLabel();
                jLabelDonGia36 = new javax.swing.JLabel();
                jLabelSL36 = new javax.swing.JLabel();
                jTextFieldMaSP36 = new javax.swing.JTextField();
                jTextFieldTenSP36 = new javax.swing.JTextField();
                jTextFieldDonGia36 = new javax.swing.JTextField();
                jSpinnerSoLuong36 = new javax.swing.JSpinner();
                jComboBoxNhanVien36 = new javax.swing.JComboBox<>();
                jButtonThemGH36 = new javax.swing.JButton();
                jLabelRefreshDb62 = new javax.swing.JLabel();
                jTextFieldSearch62 = new javax.swing.JTextField();
                jLabelSearch36 = new javax.swing.JLabel();

                setPreferredSize(new java.awt.Dimension(1200, 690));

                jPanelView62.setBackground(new java.awt.Color(245, 219, 137));
                jPanelView62.setPreferredSize(new java.awt.Dimension(1200, 690));
                jPanelView62.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

                jLabelDSSP36.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
                jLabelDSSP36.setForeground(new java.awt.Color(102, 51, 0));
                jLabelDSSP36.setText("Danh sách sản phẩm");
                jPanelView62.add(jLabelDSSP36, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 10, -1, -1));

                jScrollPaneSanPham62.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
                jScrollPaneSanPham62.setPreferredSize(new java.awt.Dimension(590, 200));

                jTableDSSP36.setFont(new java.awt.Font("Tahoma", 0, 13)); // NOI18N
                jTableDSSP36.setModel(new javax.swing.table.DefaultTableModel(
                        new Object [][] {
                                {null, null, null, null},
                                {null, null, null, null},
                                {null, null, null, null},
                                {null, null, null, null}
                        },
                        new String [] {
                                "Title 1", "Title 2", "Title 3", "Title 4"
                        }
                ));
                jTableDSSP36.setGridColor(new java.awt.Color(128, 128, 128));
                jTableDSSP36.setPreferredSize(new java.awt.Dimension(590, 0));
                jTableDSSP36.setRowHeight(25);
                jTableDSSP36.setSelectionBackground(new java.awt.Color(50, 154, 114));
                jTableDSSP36.setSelectionForeground(new java.awt.Color(255, 255, 255));
                jTableDSSP36.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
                jTableDSSP36.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
                jTableDSSP36.setShowHorizontalLines(true);
                jTableDSSP36.getTableHeader().setReorderingAllowed(false);
                jTableDSSP36.addMouseListener(new java.awt.event.MouseAdapter() {
                        public void mouseClicked(java.awt.event.MouseEvent evt) {
                                jTableDSSP36MouseClicked(evt);
                        }
                });
                jScrollPaneSanPham62.setViewportView(jTableDSSP36);

                jPanelView62.add(jScrollPaneSanPham62, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 65, 590, 200));

                jLabelGioHang36.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
                jLabelGioHang36.setForeground(new java.awt.Color(102, 51, 0));
                jLabelGioHang36.setText("Giỏ hàng");
                jPanelView62.add(jLabelGioHang36, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 285, -1, -1));

                jScrollPaneGioHang36.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
                jScrollPaneGioHang36.setPreferredSize(new java.awt.Dimension(590, 230));

                jTableGioHang36.setFont(new java.awt.Font("Tahoma", 0, 13)); // NOI18N
                jTableGioHang36.setModel(new javax.swing.table.DefaultTableModel(
                        new Object [][] {
                                {null, null, null, null},
                                {null, null, null, null},
                                {null, null, null, null},
                                {null, null, null, null}
                        },
                        new String [] {
                                "Title 1", "Title 2", "Title 3", "Title 4"
                        }
                ));
                jTableGioHang36.setFocusable(false);
                jTableGioHang36.setGridColor(new java.awt.Color(128, 128, 128));
                jTableGioHang36.setPreferredSize(new java.awt.Dimension(590, 0));
                jTableGioHang36.setRowHeight(25);
                jTableGioHang36.setSelectionBackground(new java.awt.Color(50, 154, 114));
                jTableGioHang36.setSelectionForeground(new java.awt.Color(255, 255, 255));
                jTableGioHang36.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
                jTableGioHang36.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
                jTableGioHang36.setShowHorizontalLines(true);
                jTableGioHang36.getTableHeader().setReorderingAllowed(false);
                jTableGioHang36.addMouseListener(new java.awt.event.MouseAdapter() {
                        public void mouseClicked(java.awt.event.MouseEvent evt) {
                                jTableGioHang36MouseClicked(evt);
                        }
                });
                jScrollPaneGioHang36.setViewportView(jTableGioHang36);

                jPanelView62.add(jScrollPaneGioHang36, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 320, 590, 230));

                jLabelCTSP36.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
                jLabelCTSP36.setForeground(new java.awt.Color(102, 51, 0));
                jLabelCTSP36.setText("Chi tiết sản phẩm");
                jPanelView62.add(jLabelCTSP36, new org.netbeans.lib.awtextra.AbsoluteConstraints(860, 20, -1, -1));

                jComboBoxLoaiSP36.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
                jComboBoxLoaiSP36.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
                jComboBoxLoaiSP36.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                jComboBoxLoaiSP36ActionPerformed(evt);
                        }
                });
                jPanelView62.add(jComboBoxLoaiSP36, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 35, -1, -1));

                jLabelImageSP36.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(255, 153, 0), 1, true));
                jLabelImageSP36.setPreferredSize(new java.awt.Dimension(200, 200));
                jPanelView62.add(jLabelImageSP36, new org.netbeans.lib.awtextra.AbsoluteConstraints(835, 50, 200, 200));

                jButtonXoaChonGH36.setBackground(new java.awt.Color(255, 153, 0));
                jButtonXoaChonGH36.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jButtonXoaChonGH36.setForeground(new java.awt.Color(255, 255, 255));
                jButtonXoaChonGH36.setText("Xóa");
                jButtonXoaChonGH36.setBorder(null);
                jButtonXoaChonGH36.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
                jButtonXoaChonGH36.setFocusPainted(false);
                jButtonXoaChonGH36.setPreferredSize(new java.awt.Dimension(70, 30));
                jButtonXoaChonGH36.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                jButtonXoaChonGH36ActionPerformed(evt);
                        }
                });
                jPanelView62.add(jButtonXoaChonGH36, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 560, 70, 30));

                jButtonXuatHD36.setBackground(new java.awt.Color(255, 153, 0));
                jButtonXuatHD36.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jButtonXuatHD36.setForeground(new java.awt.Color(255, 255, 255));
                jButtonXuatHD36.setText("Xuất hóa đơn");
                jButtonXuatHD36.setBorder(null);
                jButtonXuatHD36.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
                jButtonXuatHD36.setFocusPainted(false);
                jButtonXuatHD36.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                jButtonXuatHD36ActionPerformed(evt);
                        }
                });
                jPanelView62.add(jButtonXuatHD36, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 610, 150, 40));

                jPanelTTCTSP36.setBackground(new java.awt.Color(245, 219, 137));

                jLabelMaSP36.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jLabelMaSP36.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
                jLabelMaSP36.setText("Mã sản phẩm");

                jLabelNV36.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jLabelNV36.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
                jLabelNV36.setText("Nhân viên");

                jLabelTen36.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jLabelTen36.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
                jLabelTen36.setText("Tên sản phẩm");

                jLabelDonGia36.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jLabelDonGia36.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
                jLabelDonGia36.setText("Đơn giá");

                jLabelSL36.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jLabelSL36.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
                jLabelSL36.setText("Số lượng");

                jTextFieldMaSP36.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

                jTextFieldTenSP36.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

                jTextFieldDonGia36.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

                jSpinnerSoLuong36.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
                jSpinnerSoLuong36.setModel(new javax.swing.SpinnerNumberModel(1, 0, null, 1));
                jSpinnerSoLuong36.setToolTipText("");

                jComboBoxNhanVien36.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
                jComboBoxNhanVien36.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
                jComboBoxNhanVien36.setEnabled(false);

                javax.swing.GroupLayout jPanelTTCTSP36Layout = new javax.swing.GroupLayout(jPanelTTCTSP36);
                jPanelTTCTSP36.setLayout(jPanelTTCTSP36Layout);
                jPanelTTCTSP36Layout.setHorizontalGroup(
                        jPanelTTCTSP36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelTTCTSP36Layout.createSequentialGroup()
                                .addGap(15, 15, 15)
                                .addGroup(jPanelTTCTSP36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jLabelTen36)
                                        .addComponent(jLabelMaSP36)
                                        .addComponent(jLabelDonGia36)
                                        .addComponent(jLabelSL36)
                                        .addComponent(jLabelNV36))
                                .addGap(18, 18, 18)
                                .addGroup(jPanelTTCTSP36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jSpinnerSoLuong36, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jComboBoxNhanVien36, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jTextFieldMaSP36, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jTextFieldTenSP36, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jTextFieldDonGia36, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(60, 60, 60))
                );
                jPanelTTCTSP36Layout.setVerticalGroup(
                        jPanelTTCTSP36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelTTCTSP36Layout.createSequentialGroup()
                                .addGap(23, 23, 23)
                                .addGroup(jPanelTTCTSP36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jTextFieldMaSP36, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabelMaSP36))
                                .addGap(20, 20, 20)
                                .addGroup(jPanelTTCTSP36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jTextFieldTenSP36, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabelTen36))
                                .addGap(18, 18, 18)
                                .addGroup(jPanelTTCTSP36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jTextFieldDonGia36, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabelDonGia36))
                                .addGap(22, 22, 22)
                                .addGroup(jPanelTTCTSP36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jSpinnerSoLuong36, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabelSL36))
                                .addGap(22, 22, 22)
                                .addGroup(jPanelTTCTSP36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jComboBoxNhanVien36, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabelNV36))
                                .addContainerGap(15, Short.MAX_VALUE))
                );

                jPanelView62.add(jPanelTTCTSP36, new org.netbeans.lib.awtextra.AbsoluteConstraints(700, 260, 410, 270));

                jButtonThemGH36.setBackground(new java.awt.Color(255, 153, 0));
                jButtonThemGH36.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jButtonThemGH36.setForeground(new java.awt.Color(255, 255, 255));
                jButtonThemGH36.setText("Thêm vào giỏ");
                jButtonThemGH36.setBorder(null);
                jButtonThemGH36.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
                jButtonThemGH36.setFocusPainted(false);
                jButtonThemGH36.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                jButtonThemGH36ActionPerformed(evt);
                        }
                });
                jPanelView62.add(jButtonThemGH36, new org.netbeans.lib.awtextra.AbsoluteConstraints(865, 540, 140, 30));

                jLabelRefreshDb62.setBackground(new java.awt.Color(245, 219, 137));
                jLabelRefreshDb62.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
                jLabelRefreshDb62.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/mdi_database-refresh.png"))); // NOI18N
                jLabelRefreshDb62.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
                jLabelRefreshDb62.addMouseListener(new java.awt.event.MouseAdapter() {
                        public void mouseClicked(java.awt.event.MouseEvent evt) {
                                jLabelRefreshDb62MouseClicked(evt);
                        }
                });
                jPanelView62.add(jLabelRefreshDb62, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 5, 30, 30));

                jTextFieldSearch62.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
                jTextFieldSearch62.setPreferredSize(new java.awt.Dimension(130, 30));
                jTextFieldSearch62.addKeyListener(new java.awt.event.KeyAdapter() {
                        public void keyReleased(java.awt.event.KeyEvent evt) {
                                jTextFieldSearch62KeyReleased(evt);
                        }
                });
                jPanelView62.add(jTextFieldSearch62, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 27, 130, 30));

                jLabelSearch36.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/bx_search-alt.png"))); // NOI18N
                jLabelSearch36.setToolTipText("");
                jPanelView62.add(jLabelSearch36, new org.netbeans.lib.awtextra.AbsoluteConstraints(455, 27, 30, 30));

                javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
                this.setLayout(layout);
                layout.setHorizontalGroup(
                        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jPanelView62, javax.swing.GroupLayout.PREFERRED_SIZE, 1200, javax.swing.GroupLayout.PREFERRED_SIZE)
                );
                layout.setVerticalGroup(
                        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jPanelView62, javax.swing.GroupLayout.PREFERRED_SIZE, 690, javax.swing.GroupLayout.PREFERRED_SIZE)
                );
        }// </editor-fold>//GEN-END:initComponents

        private void jComboBoxLoaiSP36ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxLoaiSP36ActionPerformed
                // TODO add your handling code here:
                loadTableDataSP();
        }//GEN-LAST:event_jComboBoxLoaiSP36ActionPerformed

        private void jTableDSSP36MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableDSSP36MouseClicked
                // TODO add your handling code here:
                int row62 = jTableDSSP36.getSelectedRow();
                if (row62 > -1) {
                        String ma62 = jTableDSSP36.getValueAt(row62, 0) + "";
                        String ten62 = jTableDSSP36.getValueAt(row62, 1) + "";
                        String donGia62 = jTableDSSP36.getValueAt(row62, 2) + "";
                        String anh62 = jTableDSSP36.getValueAt(row62, 5) + "";
                        int soLuong62 = Integer.parseInt(jTableDSSP36.getValueAt(row62, 3) + "");
                        if (soLuong62 < 1) {
                                MyDialog dlg62 = new MyDialog("Sản phẩm đã hết hàng", MyDialog.ERROR_DIALOG);
                                return;
                        }

                        SpinnerNumberModel modeSpinner62 = new SpinnerNumberModel(1, 1, soLuong62, 1);
                        jSpinnerSoLuong36.setModel(modeSpinner62);
                        JFormattedTextField txtSpinner62 = ((JSpinner.NumberEditor) jSpinnerSoLuong36.getEditor()).getTextField();
                        ((NumberFormatter) txtSpinner62.getFormatter()).setAllowsInvalid(false);
                        txtSpinner62.setEditable(false);
                        txtSpinner62.setHorizontalAlignment(JTextField.LEFT);

                        jTextFieldMaSP36.setText(ma62);
                        jTextFieldTenSP36.setText(ten62);
                        jTextFieldDonGia36.setText(donGia62);
                        loadAnh(anh62);
                }
        }//GEN-LAST:event_jTableDSSP36MouseClicked

        private void loadAnh(String anh) {
                jLabelImageSP36.setIcon(getAnhSP(anh));
        }

        File fileAnhSP;

        private ImageIcon getAnhSP(String src) {
                src = src.trim().equals("") ? "default.png" : src;
                //Xử lý ảnh
                BufferedImage img = null;
                File fileImg = new File("src/images/SanPham/" + src);

                if (!fileImg.exists()) {
                        src = "default.png";
                        fileImg = new File("src/images/SanPham/" + src);
                }

                try {
                        img = ImageIO.read(fileImg);
                        fileAnhSP = new File("src/images/SanPham/" + src);
                } catch (IOException e) {
                        fileAnhSP = new File("src/images/SanPham/default.png");
                }

                if (img != null) {
                        Image dimg = img.getScaledInstance(200, 200, Image.SCALE_SMOOTH);

                        return new ImageIcon(dimg);
                }
                return null;
        }

        private void jButtonThemGH36ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonThemGH36ActionPerformed
                // TODO add your handling code here:
                int row = jTableDSSP36.getSelectedRow();
                if (row < 0) {
                        return;
                }

                String ma = jTextFieldMaSP36.getText();
                String ten = jTextFieldTenSP36.getText();
                String donGia = jTextFieldDonGia36.getText();
                int soLuong = Integer.parseInt(jSpinnerSoLuong36.getValue() + "");
                int soLuongConLai = Integer.parseInt(jTableDSSP36.getValueAt(jTableDSSP36.getSelectedRow(), 3) + "");

                if (soLuong > soLuongConLai || soLuongConLai <= 0) {
                        new MyDialog("Sản phẩm đã hết hàng", MyDialog.ERROR_DIALOG);
                        return;
                }
                jTextFieldMaSP36.setText("");
                jTextFieldTenSP36.setText("");
                jTextFieldDonGia36.setText("");
                jSpinnerSoLuong36.setValue(0);

                if (ma.trim().equalsIgnoreCase("")) {
                        return;
                }
                int key = Integer.parseInt(ma);
                for (int i = 0; i < jTableGioHang36.getRowCount(); i++) {
                        int maTbl = Integer.parseInt(jTableGioHang36.getValueAt(i, 0) + "");
                        if (maTbl == key) {
                                int soLuongAdd = Integer.parseInt(jTableGioHang36.getValueAt(i, 2) + "");
                                soLuongAdd += soLuong;
                                donGia = donGia.replace(".", "");
                                int donGiaSP = Integer.parseInt(donGia);

                                jTableGioHang36.setValueAt(soLuongAdd, i, 2);

                                jTableGioHang36.setValueAt(dcf62.format(soLuongAdd * donGiaSP).replace(",", "."), i, 4);

                                // cập nhật lại số lượng trong db
                                spBUS62.capNhatSoLuongSP(key, -soLuong);
                                spBUS62.docListSanPham();
                                loadTableDataSP();
                                return;
                        }
                }

                Vector vec62 = new Vector();
                vec62.add(ma);
                vec62.add(ten);
                vec62.add(soLuong);
                vec62.add(donGia);
                donGia = donGia.replace(".", "");
                int donGiaSP = Integer.parseInt(donGia);
                vec62.add(dcf62.format(soLuong * donGiaSP).replace(",", "."));
                // cập nhật lại số lượng trong db
                spBUS62.capNhatSoLuongSP(key, -soLuong);
                spBUS62.docListSanPham();
                loadTableDataSP();
                dtmGioHang62.addRow(vec62);
                jTableGioHang36.setPreferredSize(new Dimension(jTableGioHang36.getWidth(), jTableGioHang36.getRowHeight() * jTableGioHang36.getRowCount()));
        }//GEN-LAST:event_jButtonThemGH36ActionPerformed

        private void jButtonXoaChonGH36ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonXoaChonGH36ActionPerformed
                // TODO add your handling code here:
                int row = jTableGioHang36.getSelectedRow();
                if (row > -1) {
                        int ma = Integer.parseInt(jTableGioHang36.getValueAt(row, 0) + "");
                        int soLuong = Integer.parseInt(jTableGioHang36.getValueAt(row, 2) + "");
                        spBUS62.capNhatSoLuongSP(ma, soLuong);
                        spBUS62.docListSanPham();
                        loadTableDataSP();
                        dtmGioHang62.removeRow(row);
                }
                jTableGioHang36.setPreferredSize(new Dimension(jTableGioHang36.getWidth(), jTableGioHang36.getRowHeight() * jTableGioHang36.getRowCount()));
        }//GEN-LAST:event_jButtonXoaChonGH36ActionPerformed

        private void jTableGioHang36MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableGioHang36MouseClicked
                // TODO add your handling code here:
                int row = jTableGioHang36.getSelectedRow();
                if (row < 0) {
                        return;
                }
                String ma = jTableGioHang36.getValueAt(row, 0) + "";
                loadAnh(spBUS62.getAnh(ma));
        }//GEN-LAST:event_jTableGioHang36MouseClicked

        private void jButtonXuatHD36ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonXuatHD36ActionPerformed
                // TODO add your handling code here:
                ArrayList<Vector> dsGioHang = new ArrayList<>();
                int row = jTableGioHang36.getRowCount();
                if (row == 0) {
                        return;
                }
                int tongTien = 0;
                for (int i = 0; i < row; i++) {
                        Vector vec62 = new Vector();
                        vec62.add(jTableGioHang36.getValueAt(i, 0));
                        vec62.add(jTableGioHang36.getValueAt(i, 1));
                        vec62.add(jTableGioHang36.getValueAt(i, 2));
                        vec62.add(jTableGioHang36.getValueAt(i, 3));
                        vec62.add(jTableGioHang36.getValueAt(i, 4));
                        tongTien += Integer.parseInt((jTableGioHang36.getValueAt(i, 4) + "").replace(".", ""));
                        dsGioHang.add(vec62);
                }

                ChiTietHoaDonJFrame hoaDonUI = new ChiTietHoaDonJFrame(dsGioHang, tongTien, jComboBoxNhanVien36.getSelectedItem());
                hoaDonUI.setVisible(true);
                if (hoaDonUI.checkBanHang62) {
                        dtmGioHang62.setRowCount(0);
                }
        }//GEN-LAST:event_jButtonXuatHD36ActionPerformed

        private void jLabelRefreshDb62MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabelRefreshDb62MouseClicked
                // TODO add your handling code here:
                loadAnh("");
                jTextFieldMaSP36.setText("");
                jTextFieldTenSP36.setText("");
                jTextFieldDonGia36.setText("");
                jSpinnerSoLuong36.setValue(1);
                loadLoaiSP();
                jComboBoxLoaiSP36.setSelectedIndex(0);
                loadDataComboboxNhanVienBan();
                jTextFieldSearch62.setText("");
                searchTable(jTextFieldSearch62.getText());
        }//GEN-LAST:event_jLabelRefreshDb62MouseClicked

        private void jTextFieldSearch62KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldSearch62KeyReleased
                // TODO add your handling code here:
                searchTable(jTextFieldSearch62.getText());
        }//GEN-LAST:event_jTextFieldSearch62KeyReleased

        public void searchTable(String value) {
                TableRowSorter<DefaultTableModel> trs = new TableRowSorter<>(dtmSanPham62);
                jTableDSSP36.setRowSorter(trs);
                trs.setRowFilter(RowFilter.regexFilter(value));
                jTableDSSP36.setPreferredSize(new Dimension(jTableDSSP36.getWidth(), jTableDSSP36.getRowHeight() * jTableDSSP36.getRowCount()));
        }

        private void loadDataComboboxNhanVienBan() {
                jComboBoxNhanVien36.removeAllItems();
                ArrayList<NhanVien> dsnv = nvBUS62.getDanhSachNhanVien();
                if (dsnv != null) {
                        for (NhanVien nv : dsnv) {
                                jComboBoxNhanVien36.addItem(nv.getMaNV() + " - " + nv.getHo() + " " + nv.getTen());
                        }
                }

                for (int i = 0; i < jComboBoxNhanVien36.getItemCount(); i++) {
                        String[] cmbValue = jComboBoxNhanVien36.getItemAt(i).split(" - ");
                        if (cmbValue[0].equals(DangNhapBUS.taiKhoanLogin62.getMaNhanVien() + "")) {
                                jComboBoxNhanVien36.setSelectedIndex(i);
                                break;
                        }
                }
        }

        // Variables declaration - do not modify//GEN-BEGIN:variables
        private javax.swing.JButton jButtonThemGH36;
        private javax.swing.JButton jButtonXoaChonGH36;
        private javax.swing.JButton jButtonXuatHD36;
        private javax.swing.JComboBox<String> jComboBoxLoaiSP36;
        private javax.swing.JComboBox<String> jComboBoxNhanVien36;
        private javax.swing.JLabel jLabelCTSP36;
        private javax.swing.JLabel jLabelDSSP36;
        private javax.swing.JLabel jLabelDonGia36;
        private javax.swing.JLabel jLabelGioHang36;
        private javax.swing.JLabel jLabelImageSP36;
        private javax.swing.JLabel jLabelMaSP36;
        private javax.swing.JLabel jLabelNV36;
        private javax.swing.JLabel jLabelRefreshDb62;
        private javax.swing.JLabel jLabelSL36;
        private javax.swing.JLabel jLabelSearch36;
        private javax.swing.JLabel jLabelTen36;
        private javax.swing.JPanel jPanelTTCTSP36;
        private javax.swing.JPanel jPanelView62;
        private javax.swing.JScrollPane jScrollPaneGioHang36;
        private javax.swing.JScrollPane jScrollPaneSanPham62;
        private javax.swing.JSpinner jSpinnerSoLuong36;
        private javax.swing.JTable jTableDSSP36;
        private javax.swing.JTable jTableGioHang36;
        private javax.swing.JTextField jTextFieldDonGia36;
        private javax.swing.JTextField jTextFieldMaSP36;
        private javax.swing.JTextField jTextFieldSearch62;
        private javax.swing.JTextField jTextFieldTenSP36;
        // End of variables declaration//GEN-END:variables
}
