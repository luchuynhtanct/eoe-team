package view.BanHang;

import view.BanHang.TimKhachJDialog;
import bus.CTHoaDonBUS;
import bus.HoaDonBUS;
import custom.MyDialog;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.print.PrinterException;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Vector;
import javax.swing.JDialog;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

public class ChiTietHoaDonJFrame extends JDialog {

        private HoaDonBUS hoadonBUS62 = new HoaDonBUS();
        private CTHoaDonBUS ctHoaDonBUS62 = new CTHoaDonBUS();
        private TimKhachJDialog timKhachUI62 = new TimKhachJDialog();

        public ChiTietHoaDonJFrame() {
                checkBanHang62 = false;
                initComponents();
                this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
                this.setLocationRelativeTo(null);
                this.setModal(true);
                Image icon62 = Toolkit.getDefaultToolkit().getImage("src/images/icon-app.png");
                this.setIconImage(icon62);
                customEvents();
        }

        private ArrayList<Vector> dsGioHang62;
        private int tongTien62;
        private String nhanVien62;

        public ChiTietHoaDonJFrame(ArrayList<Vector> dsGioHang62, int tongTien62, Object nhanVien62) {
                this();
                this.tongTien62 = tongTien62;
                this.dsGioHang62 = dsGioHang62;
                this.nhanVien62 = (String) nhanVien62;
                DecimalFormat dcf62 = new DecimalFormat("###,###");
                txtTongTien50.setText(dcf62.format(tongTien62).replace(",", "."));
        }

        private void customEvents() {
                txtTenKhach50.getDocument().addDocumentListener(new DocumentListener() {
                        public void changedUpdate(DocumentEvent e) {
                                checkKhachMa();
                        }

                        public void removeUpdate(DocumentEvent e) {
                                checkKhachMa();
                        }

                        public void insertUpdate(DocumentEvent e) {
                                checkKhachMa();
                        }
                });
        }

        private void checkKhachMa() {
                if (!txtTenKhach50.getText().equals("")) {
                        btnThanhToan50.setEnabled(true);
                } else {
                        btnThanhToan50.setEnabled(false);
                }
        }

        private void xuLyHienThiHoaDon() {
                txtHoaDon50.setContentType("text/html");
                DateTimeFormatter dtf62 = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
                LocalDateTime now62 = LocalDateTime.now();
                DecimalFormat dcf62 = new DecimalFormat("###,### VND");

                String hd62 = "<style> "
                        + "table {"
                        + "border: 1px solid;"
                        + "border-bottom: none"
                        + "}"
                        + "tr {"
                        + "border-bottom: 1px solid;"
                        + "}"
                        + "td {"
                        + "padding: 8px;"
                        + "} "
                        + "th {"
                        + "font-size:16pt"
                        + "}"
                        + "</style>";
                hd62 += "<h1 style='text-align:center;'>HOÁ ĐƠN THANH TOÁN</h1>";
                hd62 += "Nhân viên: " + nhanVien62 + "<br/>";
                hd62 += "Ngày lập: " + dtf62.format(now62) + "<br/>";
                hd62 += "Khách hàng: " + txtTenKhach50.getText() + "<br/>";
                hd62 += "<div style='text-align:center;'>==========================================</div><br/>";
                hd62 += "<div style='text-align:center'>";
                hd62 += "<table style='max-width:100%'>";
                hd62 += "<tr>"
                        + "<th>Mã SP</th>"
                        + "<th>Tên SP</th>"
                        + "<th>Số lượng</th>"
                        + "<th>Đơn giá</th>"
                        + "<th>Thành tiền</th>"
                        + "</tr>";
                for (Vector vec62 : dsGioHang62) {
                        hd62 += "<tr>";
                        hd62 += "<td style='text-align:center;'>" + vec62.get(0) + "</td>";
                        hd62 += "<td style='text-align:left;'>" + vec62.get(1) + "</td>";
                        hd62 += "<td style='text-align:center;'>" + vec62.get(2) + "</td>";
                        hd62 += "<td style='text-align:center;'>" + vec62.get(3) + "</td>";
                        hd62 += "<td style='text-align:center;'>" + vec62.get(4) + "</td>";
                        hd62 += "</tr>";
                }
                hd62 += "<tr>";
                hd62 += "<td style='text-align:center;'>" + "</td>";
                hd62 += "<td style='text-align:left;'>" + "</td>";
                hd62 += "<td style='text-align:center;'>" + "</td>";
                hd62 += "<td style='text-align:center;font-weight:bold'>Tổng cộng</td>";
                hd62 += "<td style='text-align:center;'>" + dcf62.format(tongTien62).replace(",", ".") + "</td>";
                hd62 += "</tr>";
                hd62 += "<tr>";
                hd62 += "<td style='text-align:center;'>" + "</td>";
                hd62 += "<td style='text-align:left;'>" + "</td>";
                hd62 += "<td style='text-align:center;'>" + "</td>";
                hd62 += "<td style='text-align:center;font-weight:bold'>Thành tiền</td>";
                hd62 += "<td style='text-align:center;'>" + dcf62.format(tongTien62).replace(",", ".") + "</td>";
                hd62 += "</tr>";
                hd62 += "</table>";
                hd62 += "</div>";
                hd62 += "<div style='text-align:center;'>==========================================</div><br/>";
                txtHoaDon50.setText(hd62);
                txtTongTien50.setText(dcf62.format(tongTien62).replace(",", "."));
        }

        @SuppressWarnings("unchecked")
        // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
        private void initComponents() {

                jPanelTT50 = new javax.swing.JPanel();
                jLabelTT50 = new javax.swing.JLabel();
                jPanelBT50 = new javax.swing.JPanel();
                btnThanhToan50 = new javax.swing.JButton();
                btnInHoaDon50 = new javax.swing.JButton();
                jScrollPaneCTHD50 = new javax.swing.JScrollPane();
                txtHoaDon50 = new javax.swing.JEditorPane();
                txtTenKhach50 = new javax.swing.JTextField();
                jLabelKH50 = new javax.swing.JLabel();
                btnTimKhach50 = new javax.swing.JButton();
                jLabelTongTien50 = new javax.swing.JLabel();
                txtTongTien50 = new javax.swing.JTextField();
                jPanel50 = new javax.swing.JPanel();

                jLabelTT50.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
                jLabelTT50.setForeground(new java.awt.Color(255, 153, 0));
                jLabelTT50.setText("Chi tiết hoá đơn");
                jPanelTT50.add(jLabelTT50);

                btnThanhToan50.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
                btnThanhToan50.setText("Thanh toán");
                btnThanhToan50.setEnabled(false);
                btnThanhToan50.setPreferredSize(new java.awt.Dimension(128, 45));
                btnThanhToan50.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                btnThanhToan50ActionPerformed(evt);
                        }
                });
                jPanelBT50.add(btnThanhToan50);

                btnInHoaDon50.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
                btnInHoaDon50.setText("In hoá đơn");
                btnInHoaDon50.setEnabled(false);
                btnInHoaDon50.setPreferredSize(new java.awt.Dimension(128, 45));
                btnInHoaDon50.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                btnInHoaDon50ActionPerformed(evt);
                        }
                });
                jPanelBT50.add(btnInHoaDon50);

                txtHoaDon50.setEditable(false);
                jScrollPaneCTHD50.setViewportView(txtHoaDon50);

                txtTenKhach50.setEditable(false);
                txtTenKhach50.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N

                jLabelKH50.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
                jLabelKH50.setText("Khách hàng");

                btnTimKhach50.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
                btnTimKhach50.setForeground(new java.awt.Color(255, 153, 0));
                btnTimKhach50.setText("+");
                btnTimKhach50.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
                btnTimKhach50.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
                btnTimKhach50.setMargin(new java.awt.Insets(0, 0, 0, 0));
                btnTimKhach50.setVerticalAlignment(javax.swing.SwingConstants.TOP);
                btnTimKhach50.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                btnTimKhach50ActionPerformed(evt);
                        }
                });

                jLabelTongTien50.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
                jLabelTongTien50.setText("Tổng tiền");

                txtTongTien50.setEditable(false);
                txtTongTien50.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N

                javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
                getContentPane().setLayout(layout);
                layout.setHorizontalGroup(
                        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jPanelTT50, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanelBT50, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel50, javax.swing.GroupLayout.DEFAULT_SIZE, 611, Short.MAX_VALUE)
                        .addGroup(layout.createSequentialGroup()
                                .addGap(31, 31, 31)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabelKH50)
                                        .addComponent(jLabelTongTien50))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(txtTongTien50, javax.swing.GroupLayout.DEFAULT_SIZE, 368, Short.MAX_VALUE)
                                        .addComponent(txtTenKhach50))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnTimKhach50, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                        .addComponent(jScrollPaneCTHD50, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                );
                layout.setVerticalGroup(
                        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addComponent(jPanelTT50, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(txtTenKhach50, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabelKH50)
                                        .addComponent(btnTimKhach50, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabelTongTien50)
                                        .addComponent(txtTongTien50, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPaneCTHD50, javax.swing.GroupLayout.PREFERRED_SIZE, 407, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jPanel50, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jPanelBT50, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap())
                );

                pack();
        }// </editor-fold>//GEN-END:initComponents

        public static boolean checkBanHang62 = false;

        private void btnThanhToan50ActionPerformed(java.awt.event.ActionEvent evt) {
                checkBanHang62 = false;
                if (txtTenKhach50.getText().equals("")) {
                        new MyDialog("Xin chọn khách hàng", MyDialog.ERROR_DIALOG);
                        return;
                }
                xuLyHienThiHoaDon();
                btnInHoaDon50.setEnabled(true);

                hoadonBUS62.luuHoaDon(TimKhachJDialog.kh62achHangTimDuoc62.getMaKH(), nhanVien62, tongTien62, "Đã thanh toán");

                for (Vector vec62 : dsGioHang62) {
                        String maSP = vec62.get(0) + "";
                        String soLuong = vec62.get(2) + "";
                        String donGia = vec62.get(3) + "";
                        String thanhTien = vec62.get(4) + "";
                        ctHoaDonBUS62.addCTHoaDon(maSP, soLuong, donGia, thanhTien);
                }
                btnThanhToan50.setEnabled(false);
                btnTimKhach50.setEnabled(false);
                checkBanHang62 = true;
        }

        private void btnInHoaDon50ActionPerformed(java.awt.event.ActionEvent evt) {
                try {
                        if (!txtHoaDon50.getText().equals("")) {
                                txtHoaDon50.print();
                                this.dispose();
                        }
                } catch (PrinterException ex) {
                }
        }

        private void btnTimKhach50ActionPerformed(java.awt.event.ActionEvent evt) {
                timKhachUI62.setVisible(true);
                if (timKhachUI62.kh62achHangTimDuoc62 != null) {
                        txtTenKhach50.setText(timKhachUI62.kh62achHangTimDuoc62.getMaKH() + " - " + timKhachUI62.kh62achHangTimDuoc62.getHo() + " " + timKhachUI62.kh62achHangTimDuoc62.getTen());
                }
        }

        // Variables declaration - do not modify//GEN-BEGIN:variables
        private javax.swing.JButton btnInHoaDon50;
        private javax.swing.JButton btnThanhToan50;
        private javax.swing.JButton btnTimKhach50;
        private javax.swing.JLabel jLabelKH50;
        private javax.swing.JLabel jLabelTT50;
        private javax.swing.JLabel jLabelTongTien50;
        private javax.swing.JPanel jPanel50;
        private javax.swing.JPanel jPanelBT50;
        private javax.swing.JPanel jPanelTT50;
        private javax.swing.JScrollPane jScrollPaneCTHD50;
        private javax.swing.JEditorPane txtHoaDon50;
        private javax.swing.JTextField txtTenKhach50;
        private javax.swing.JTextField txtTongTien50;
        // End of variables declaration//GEN-END:variables
}
