package view.KhachHang;

import bus.KhachHangBUS;
import custom.MyTable;
import java.awt.Dimension;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Vector;
import javax.swing.JLabel;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableRowSorter;
import model.KhachHang;

public class KhachHangJPanel extends javax.swing.JPanel {

        KhachHangBUS khBUS62 = new KhachHangBUS();
        DefaultTableModel dtmKhachHang62;

        public KhachHangJPanel() {
                initComponents();

                DefaultTableCellRenderer centerRenderer62 = new DefaultTableCellRenderer();
                centerRenderer62.setHorizontalAlignment(JLabel.CENTER);

                jTextFieldMa32.setEditable(false);
                jTextFieldTongCT32.setEditable(false);

                new MyTable(jTableAll32);
                dtmKhachHang62 = new DefaultTableModel() {
                        @Override
                        public boolean isCellEditable(int row, int column) {
                                return false;
                        }
                };
                jTableAll32.setModel(dtmKhachHang62);

                dtmKhachHang62.addColumn("Mã KH");
                dtmKhachHang62.addColumn("Họ đệm");
                dtmKhachHang62.addColumn("Tên");
                dtmKhachHang62.addColumn("Giới tính");
                dtmKhachHang62.addColumn("Tổng chi tiêu");

                jTableAll32.getColumnModel().getColumn(0).setCellRenderer(centerRenderer62);

                TableColumnModel columnModelSanPham62 = jTableAll32.getColumnModel();
                columnModelSanPham62.getColumn(0).setPreferredWidth(50);
                columnModelSanPham62.getColumn(1).setPreferredWidth(230);
                columnModelSanPham62.getColumn(2).setPreferredWidth(150);
                columnModelSanPham62.getColumn(3).setPreferredWidth(120);
                columnModelSanPham62.getColumn(4).setPreferredWidth(300);

                loadDataCmbGT();
                loadDataLenTableKhachHang();
        }

        @SuppressWarnings("unchecked")
        // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
        private void initComponents() {

                jPanel32 = new javax.swing.JPanel();
                jLabelQLKH32 = new javax.swing.JLabel();
                jLabelMaKH32 = new javax.swing.JLabel();
                jLabelGT32 = new javax.swing.JLabel();
                jComboBoxGT32 = new javax.swing.JComboBox<>();
                jTextFieldMa32 = new javax.swing.JTextField();
                jButtonThem32 = new javax.swing.JButton();
                jButtonLuu32 = new javax.swing.JButton();
                jButtonXoa32 = new javax.swing.JButton();
                jScrollPane32 = new javax.swing.JScrollPane();
                jTableAll32 = new javax.swing.JTable();
                jLabelRefresh32 = new javax.swing.JLabel();
                jLabelSearch36 = new javax.swing.JLabel();
                jTextFieldSearch62 = new javax.swing.JTextField();
                jLabelHoDem32 = new javax.swing.JLabel();
                jTextFieldHoDem32 = new javax.swing.JTextField();
                jLabelTen32 = new javax.swing.JLabel();
                jTextFieldTen32 = new javax.swing.JTextField();
                jLabelTongCT32 = new javax.swing.JLabel();
                jTextFieldTongCT32 = new javax.swing.JTextField();
                jLabelCTTo32 = new javax.swing.JLabel();
                jTextFieldCTTo32 = new javax.swing.JTextField();
                jLabelCTFrom32 = new javax.swing.JLabel();
                jTextFieldCTFrom32 = new javax.swing.JTextField();
                jLabelSearchPrice62 = new javax.swing.JLabel();

                setBackground(new java.awt.Color(245, 219, 137));
                setPreferredSize(new java.awt.Dimension(1200, 655));

                jPanel32.setBackground(new java.awt.Color(245, 219, 137));
                jPanel32.setPreferredSize(new java.awt.Dimension(1200, 690));
                jPanel32.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

                jLabelQLKH32.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
                jLabelQLKH32.setForeground(new java.awt.Color(102, 51, 0));
                jLabelQLKH32.setText("QUẢN LÝ KHÁCH HÀNG");
                jPanel32.add(jLabelQLKH32, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 15, -1, -1));

                jLabelMaKH32.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jLabelMaKH32.setForeground(new java.awt.Color(0, 0, 0));
                jLabelMaKH32.setText("Mã KH");
                jPanel32.add(jLabelMaKH32, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 70, 50, -1));

                jLabelGT32.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jLabelGT32.setForeground(new java.awt.Color(0, 0, 0));
                jLabelGT32.setText("Giới tính");
                jPanel32.add(jLabelGT32, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 190, -1, -1));

                jComboBoxGT32.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
                jComboBoxGT32.setForeground(new java.awt.Color(0, 0, 0));
                jComboBoxGT32.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
                jPanel32.add(jComboBoxGT32, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 180, 390, 32));

                jTextFieldMa32.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
                jTextFieldMa32.setForeground(new java.awt.Color(0, 0, 0));
                jPanel32.add(jTextFieldMa32, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 60, 390, 32));

                jButtonThem32.setBackground(new java.awt.Color(255, 153, 0));
                jButtonThem32.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jButtonThem32.setForeground(new java.awt.Color(255, 255, 255));
                jButtonThem32.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Add.png"))); // NOI18N
                jButtonThem32.setText("Thêm");
                jButtonThem32.setBorder(null);
                jButtonThem32.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
                jButtonThem32.setFocusPainted(false);
                jButtonThem32.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                jButtonThem32ActionPerformed(evt);
                        }
                });
                jPanel32.add(jButtonThem32, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 305, 90, 30));

                jButtonLuu32.setBackground(new java.awt.Color(255, 153, 0));
                jButtonLuu32.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jButtonLuu32.setForeground(new java.awt.Color(255, 255, 255));
                jButtonLuu32.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Save.png"))); // NOI18N
                jButtonLuu32.setText("Lưu");
                jButtonLuu32.setBorder(null);
                jButtonLuu32.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
                jButtonLuu32.setFocusPainted(false);
                jButtonLuu32.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                jButtonLuu32ActionPerformed(evt);
                        }
                });
                jPanel32.add(jButtonLuu32, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 305, 90, 30));

                jButtonXoa32.setBackground(new java.awt.Color(255, 153, 0));
                jButtonXoa32.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jButtonXoa32.setForeground(new java.awt.Color(255, 255, 255));
                jButtonXoa32.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Delete.png"))); // NOI18N
                jButtonXoa32.setText("Xóa");
                jButtonXoa32.setBorder(null);
                jButtonXoa32.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
                jButtonXoa32.setFocusPainted(false);
                jButtonXoa32.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                jButtonXoa32ActionPerformed(evt);
                        }
                });
                jPanel32.add(jButtonXoa32, new org.netbeans.lib.awtextra.AbsoluteConstraints(710, 305, 90, 30));

                jScrollPane32.setPreferredSize(new java.awt.Dimension(1200, 320));

                jTableAll32.setFont(new java.awt.Font("Tahoma", 0, 13)); // NOI18N
                jTableAll32.setModel(new javax.swing.table.DefaultTableModel(
                        new Object [][] {
                                {},
                                {},
                                {},
                                {}
                        },
                        new String [] {

                        }
                ));
                jTableAll32.setGridColor(new java.awt.Color(128, 128, 128));
                jTableAll32.setPreferredSize(new java.awt.Dimension(1200, 0));
                jTableAll32.setRowHeight(25);
                jTableAll32.setSelectionBackground(new java.awt.Color(50, 154, 114));
                jTableAll32.setSelectionForeground(new java.awt.Color(255, 255, 255));
                jTableAll32.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
                jTableAll32.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
                jTableAll32.setShowHorizontalLines(true);
                jTableAll32.addMouseListener(new java.awt.event.MouseAdapter() {
                        public void mouseClicked(java.awt.event.MouseEvent evt) {
                                jTableAll32MouseClicked(evt);
                        }
                });
                jScrollPane32.setViewportView(jTableAll32);

                jPanel32.add(jScrollPane32, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 346, 1200, 374));

                jLabelRefresh32.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/mdi_database-refresh.png"))); // NOI18N
                jLabelRefresh32.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
                jLabelRefresh32.addMouseListener(new java.awt.event.MouseAdapter() {
                        public void mouseClicked(java.awt.event.MouseEvent evt) {
                                jLabelRefresh32MouseClicked(evt);
                        }
                });
                jPanel32.add(jLabelRefresh32, new org.netbeans.lib.awtextra.AbsoluteConstraints(675, 10, -1, -1));

                jLabelSearch36.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/bx_search-alt.png"))); // NOI18N
                jLabelSearch36.setToolTipText("");
                jPanel32.add(jLabelSearch36, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 305, 30, 30));

                jTextFieldSearch62.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
                jTextFieldSearch62.setPreferredSize(new java.awt.Dimension(130, 30));
                jTextFieldSearch62.addKeyListener(new java.awt.event.KeyAdapter() {
                        public void keyReleased(java.awt.event.KeyEvent evt) {
                                jTextFieldSearch62KeyReleased(evt);
                        }
                });
                jPanel32.add(jTextFieldSearch62, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 305, 160, 30));

                jLabelHoDem32.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jLabelHoDem32.setForeground(new java.awt.Color(0, 0, 0));
                jLabelHoDem32.setText("Họ đệm");
                jPanel32.add(jLabelHoDem32, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 110, 70, -1));

                jTextFieldHoDem32.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
                jTextFieldHoDem32.setForeground(new java.awt.Color(0, 0, 0));
                jPanel32.add(jTextFieldHoDem32, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 100, 390, 32));

                jLabelTen32.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jLabelTen32.setForeground(new java.awt.Color(0, 0, 0));
                jLabelTen32.setText("Tên");
                jPanel32.add(jLabelTen32, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 150, 70, -1));

                jTextFieldTen32.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
                jTextFieldTen32.setForeground(new java.awt.Color(0, 0, 0));
                jPanel32.add(jTextFieldTen32, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 140, 390, 32));

                jLabelTongCT32.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jLabelTongCT32.setForeground(new java.awt.Color(0, 0, 0));
                jLabelTongCT32.setText("Tổng chi tiêu");
                jPanel32.add(jLabelTongCT32, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 230, -1, -1));

                jTextFieldTongCT32.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
                jTextFieldTongCT32.setForeground(new java.awt.Color(0, 0, 0));
                jPanel32.add(jTextFieldTongCT32, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 220, 390, 32));

                jLabelCTTo32.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jLabelCTTo32.setForeground(new java.awt.Color(0, 0, 0));
                jLabelCTTo32.setText("đến");
                jPanel32.add(jLabelCTTo32, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 270, -1, -1));

                jTextFieldCTTo32.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
                jTextFieldCTTo32.setForeground(new java.awt.Color(0, 0, 0));
                jPanel32.add(jTextFieldCTTo32, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 260, 170, 32));

                jLabelCTFrom32.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jLabelCTFrom32.setForeground(new java.awt.Color(0, 0, 0));
                jLabelCTFrom32.setText("Chi tiêu từ");
                jPanel32.add(jLabelCTFrom32, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 270, -1, -1));

                jTextFieldCTFrom32.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
                jTextFieldCTFrom32.setForeground(new java.awt.Color(0, 0, 0));
                jPanel32.add(jTextFieldCTFrom32, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 260, 170, 32));

                jLabelSearchPrice62.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/bx_search-alt.png"))); // NOI18N
                jLabelSearchPrice62.setToolTipText("");
                jLabelSearchPrice62.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
                jLabelSearchPrice62.addMouseListener(new java.awt.event.MouseAdapter() {
                        public void mouseClicked(java.awt.event.MouseEvent evt) {
                                jLabelSearchPrice62MouseClicked(evt);
                        }
                });
                jPanel32.add(jLabelSearchPrice62, new org.netbeans.lib.awtextra.AbsoluteConstraints(810, 260, -1, -1));

                javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
                this.setLayout(layout);
                layout.setHorizontalGroup(
                        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addComponent(jPanel32, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                );
                layout.setVerticalGroup(
                        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addComponent(jPanel32, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                );
        }// </editor-fold>//GEN-END:initComponents

        private void jTextFieldSearch62KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldSearch62KeyReleased
                // TODO add your handling code here:
                searchTable(jTextFieldSearch62.getText());
        }//GEN-LAST:event_jTextFieldSearch62KeyReleased

        private void jLabelRefresh32MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabelRefresh32MouseClicked
                // TODO add your handling code here:
                xuLyRefresh();
        }//GEN-LAST:event_jLabelRefresh32MouseClicked

        private void xuLyRefresh() {
                jTextFieldMa32.setText("");
                jTextFieldHoDem32.setText("");
                jTextFieldTen32.setText("");
                jTextFieldTongCT32.setText("");
                jTextFieldCTFrom32.setText("");
                jTextFieldCTTo32.setText("");
                jComboBoxGT32.setSelectedIndex(0);
                jTextFieldSearch62.setText("");
                searchTable(jTextFieldSearch62.getText());
                jComboBoxGT32.setSelectedIndex(0);
                loadDataLenTableKhachHang();
        }

        private void jTableAll32MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableAll32MouseClicked
                // TODO add your handling code here:
                xuLyClicktblKhachHang();
        }//GEN-LAST:event_jTableAll32MouseClicked

        private void jButtonXoa32ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonXoa32ActionPerformed
                // TODO add your handling code here:
                xuLyXoaKhachHang();
        }//GEN-LAST:event_jButtonXoa32ActionPerformed

        private void xuLyXoaKhachHang() {
                if (khBUS62.xoaKhachHang(jTextFieldMa32.getText())) {
                        xuLyRefresh();
                }
        }

        private void jButtonLuu32ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonLuu32ActionPerformed
                // TODO add your handling code here:
                xuLySuaKhachHang();
        }//GEN-LAST:event_jButtonLuu32ActionPerformed

        private void xuLySuaKhachHang() {
                if (khBUS62.suaKhachHang(jTextFieldMa32.getText(), jTextFieldHoDem32.getText(), jTextFieldTen32.getText(), jComboBoxGT32.getSelectedItem() + "")) {
                        xuLyRefresh();
                }
        }

        private void jButtonThem32ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonThem32ActionPerformed
                // TODO add your handling code here:
                xuLyThemKhachHang();
        }//GEN-LAST:event_jButtonThem32ActionPerformed

        private void xuLyThemKhachHang() {
                if (khBUS62.themKhachHang(jTextFieldHoDem32.getText(), jTextFieldTen32.getText(), jComboBoxGT32.getSelectedItem() + "")) {
                        xuLyRefresh();
                }
        }

        private void jLabelSearchPrice62MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabelSearchPrice62MouseClicked
                // TODO add your handling code here:
                xuLyTimKiemTheoKhoang();
        }//GEN-LAST:event_jLabelSearchPrice62MouseClicked

        private void loadDataCmbGT() {
                jComboBoxGT32.removeAllItems();
                jComboBoxGT32.addItem("Chọn giới tính");
                jComboBoxGT32.addItem("Nam");
                jComboBoxGT32.addItem("Nữ");
        }

        private void loadDataLenTableKhachHang() {
                khBUS62.docDanhSach();
                ArrayList<KhachHang> dskh62 = khBUS62.getListKhachHang();
                loadDataLenTableKhachHang(dskh62);
        }

        private void loadDataLenTableKhachHang(ArrayList<KhachHang> dskh62) {
                dtmKhachHang62.setRowCount(0);
                DecimalFormat dcf62 = new DecimalFormat("###,###");
                for (KhachHang kh62 : dskh62) {
                        Vector vec62 = new Vector();
                        vec62.add(kh62.getMaKH());
                        vec62.add(kh62.getHo());
                        vec62.add(kh62.getTen());
                        vec62.add(kh62.getGioiTinh());
                        vec62.add(dcf62.format(kh62.getTongChiTieu()).replace(",", "."));
                        dtmKhachHang62.addRow(vec62);
                }
                jTableAll32.setPreferredSize(
                        new Dimension(jTableAll32.getWidth(), jTableAll32.getRowHeight() * jTableAll32.getRowCount()));
        }

        public void searchTable(String value) {
                TableRowSorter<DefaultTableModel> trs = new TableRowSorter<>(dtmKhachHang62);
                jTableAll32.setRowSorter(trs);
                trs.setRowFilter(RowFilter.regexFilter(value));
                jTableAll32.setPreferredSize(new Dimension(jTableAll32.getWidth(), jTableAll32.getRowHeight() * jTableAll32.getRowCount()));
        }

        private void xuLyTimKiemTheoKhoang() {
                ArrayList<KhachHang> dskh62 = khBUS62.timKiemKhachHang(jTextFieldCTFrom32.getText(), jTextFieldCTTo32.getText());
                if (dskh62 == null) {
                        return;
                }
                loadDataLenTableKhachHang(dskh62);
                jTableAll32.setPreferredSize(new Dimension(jTableAll32.getWidth(), jTableAll32.getRowHeight() * jTableAll32.getRowCount()));
        }

        private void xuLyClicktblKhachHang() {
                int row62 = jTableAll32.getSelectedRow();
                if (row62 > -1) {
                        jTextFieldMa32.setText(jTableAll32.getValueAt(row62, 0) + "");
                        jTextFieldHoDem32.setText(jTableAll32.getValueAt(row62, 1) + "");
                        jTextFieldTen32.setText(jTableAll32.getValueAt(row62, 2) + "");
                        int index = jTableAll32.getValueAt(row62, 3).equals("Nam") ? 1 : 2;
                        jComboBoxGT32.setSelectedIndex(index);
                        jTextFieldTongCT32.setText((jTableAll32.getValueAt(row62, 4) + "").replace(",", "."));
                }
        }

        // Variables declaration - do not modify//GEN-BEGIN:variables
        private javax.swing.JButton jButtonLuu32;
        private javax.swing.JButton jButtonThem32;
        private javax.swing.JButton jButtonXoa32;
        private javax.swing.JComboBox<String> jComboBoxGT32;
        private javax.swing.JLabel jLabelCTFrom32;
        private javax.swing.JLabel jLabelCTTo32;
        private javax.swing.JLabel jLabelGT32;
        private javax.swing.JLabel jLabelHoDem32;
        private javax.swing.JLabel jLabelMaKH32;
        private javax.swing.JLabel jLabelQLKH32;
        private javax.swing.JLabel jLabelRefresh32;
        private javax.swing.JLabel jLabelSearch36;
        private javax.swing.JLabel jLabelSearchPrice62;
        private javax.swing.JLabel jLabelTen32;
        private javax.swing.JLabel jLabelTongCT32;
        private javax.swing.JPanel jPanel32;
        private javax.swing.JScrollPane jScrollPane32;
        private javax.swing.JTable jTableAll32;
        private javax.swing.JTextField jTextFieldCTFrom32;
        private javax.swing.JTextField jTextFieldCTTo32;
        private javax.swing.JTextField jTextFieldHoDem32;
        private javax.swing.JTextField jTextFieldMa32;
        private javax.swing.JTextField jTextFieldSearch62;
        private javax.swing.JTextField jTextFieldTen32;
        private javax.swing.JTextField jTextFieldTongCT32;
        // End of variables declaration//GEN-END:variables
}
