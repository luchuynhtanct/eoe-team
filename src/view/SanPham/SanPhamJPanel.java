package view.SanPham;

import bus.LoaiBUS;
import bus.SanPhamBUS;
import custom.MyDialog;
import custom.MyFileChooser;
import custom.MyTable;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Vector;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.RowFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableRowSorter;
import model.LoaiSP;
import model.SanPham;

public class SanPhamJPanel extends javax.swing.JPanel {

        SanPhamBUS spBUS62 = new SanPhamBUS();
        LoaiBUS loaiBUS62 = new LoaiBUS();
        DefaultTableModel dtmSanPham62;
        private DecimalFormat dcf62;

        public SanPhamJPanel() {
                initComponents();

                DefaultTableCellRenderer centerRenderer62 = new DefaultTableCellRenderer();
                centerRenderer62.setHorizontalAlignment(JLabel.CENTER);
                dcf62 = new DecimalFormat("###,###");

                jTextFieldMaSP37.setEditable(false);

                new MyTable(jTableAll32);
                dtmSanPham62 = new DefaultTableModel() {
                        @Override
                        public boolean isCellEditable(int row, int column) {
                                return false;
                        }
                };
                jTableAll32.setModel(dtmSanPham62);

                dtmSanPham62.addColumn("Mã SP");
                dtmSanPham62.addColumn("Tên SP");
                dtmSanPham62.addColumn("Loại SP");
                dtmSanPham62.addColumn("Đơn giá");
                dtmSanPham62.addColumn("Số lượng");
                dtmSanPham62.addColumn("Đơn vị tính");
                dtmSanPham62.addColumn("Ảnh");

                jTableAll32.getColumnModel().getColumn(0).setCellRenderer(centerRenderer62);
                jTableAll32.getColumnModel().getColumn(3).setCellRenderer(centerRenderer62);
                jTableAll32.getColumnModel().getColumn(4).setCellRenderer(centerRenderer62);
                jTableAll32.getColumnModel().getColumn(5).setCellRenderer(centerRenderer62);

                TableColumnModel columnModelSanPham62 = jTableAll32.getColumnModel();
                columnModelSanPham62.getColumn(0).setPreferredWidth(50);
                columnModelSanPham62.getColumn(1).setPreferredWidth(230);
                columnModelSanPham62.getColumn(2).setPreferredWidth(150);
                columnModelSanPham62.getColumn(3).setPreferredWidth(120);
                columnModelSanPham62.getColumn(4).setPreferredWidth(100);
                columnModelSanPham62.getColumn(5).setPreferredWidth(140);
                columnModelSanPham62.getColumn(6).setPreferredWidth(100);

                loadAnh("");
                loadDataCmbLoai();
                loadDataLenBangSanPham();
        }

        @SuppressWarnings("unchecked")
        // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
        private void initComponents() {

                jPanel32 = new javax.swing.JPanel();
                jLabelQLSP32 = new javax.swing.JLabel();
                jLabelMaSP32 = new javax.swing.JLabel();
                jTextFieldSL32 = new javax.swing.JTextField();
                jLabelTen32 = new javax.swing.JLabel();
                jTextFieldTen32 = new javax.swing.JTextField();
                jLabelLoai32 = new javax.swing.JLabel();
                jComboBoxLoai32 = new javax.swing.JComboBox<>();
                jLabelSL32 = new javax.swing.JLabel();
                jLabelDVT32 = new javax.swing.JLabel();
                jTextFieldDG32 = new javax.swing.JTextField();
                jLabelDG32 = new javax.swing.JLabel();
                jTextFieldMaSP37 = new javax.swing.JTextField();
                jButtonThem32 = new javax.swing.JButton();
                jButtonLuu32 = new javax.swing.JButton();
                jButtonXoa32 = new javax.swing.JButton();
                jButtonChonAnh32 = new javax.swing.JButton();
                jScrollPane32 = new javax.swing.JScrollPane();
                jTableAll32 = new javax.swing.JTable();
                jLabelRefresh32 = new javax.swing.JLabel();
                jTextFieldDVT32 = new javax.swing.JTextField();
                jLabelImageSP36 = new javax.swing.JLabel();
                jLabelSearch36 = new javax.swing.JLabel();
                jTextFieldSearch62 = new javax.swing.JTextField();
                jButtonThem62 = new javax.swing.JButton();

                setBackground(new java.awt.Color(245, 219, 137));
                setPreferredSize(new java.awt.Dimension(1200, 655));

                jPanel32.setBackground(new java.awt.Color(245, 219, 137));
                jPanel32.setPreferredSize(new java.awt.Dimension(1200, 690));
                jPanel32.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

                jLabelQLSP32.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
                jLabelQLSP32.setForeground(new java.awt.Color(102, 51, 0));
                jLabelQLSP32.setText("QUẢN LÝ SẢN PHẨM");
                jPanel32.add(jLabelQLSP32, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 20, -1, -1));

                jLabelMaSP32.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jLabelMaSP32.setForeground(new java.awt.Color(0, 0, 0));
                jLabelMaSP32.setText("Mã SP ");
                jPanel32.add(jLabelMaSP32, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 70, 50, -1));

                jTextFieldSL32.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
                jTextFieldSL32.setForeground(new java.awt.Color(0, 0, 0));
                jPanel32.add(jTextFieldSL32, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 180, 300, 32));

                jLabelTen32.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jLabelTen32.setForeground(new java.awt.Color(0, 0, 0));
                jLabelTen32.setText("Tên SP ");
                jPanel32.add(jLabelTen32, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 110, -1, -1));

                jTextFieldTen32.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
                jTextFieldTen32.setForeground(new java.awt.Color(0, 0, 0));
                jPanel32.add(jTextFieldTen32, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 100, 300, 32));

                jLabelLoai32.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jLabelLoai32.setForeground(new java.awt.Color(0, 0, 0));
                jLabelLoai32.setText("Loại");
                jPanel32.add(jLabelLoai32, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 150, -1, -1));

                jComboBoxLoai32.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
                jComboBoxLoai32.setForeground(new java.awt.Color(0, 0, 0));
                jComboBoxLoai32.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
                jPanel32.add(jComboBoxLoai32, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 140, 300, 32));

                jLabelSL32.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jLabelSL32.setForeground(new java.awt.Color(0, 0, 0));
                jLabelSL32.setText("Số Lượng");
                jPanel32.add(jLabelSL32, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 190, -1, -1));

                jLabelDVT32.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jLabelDVT32.setForeground(new java.awt.Color(0, 0, 0));
                jLabelDVT32.setText("Đơn vị tính");
                jPanel32.add(jLabelDVT32, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 270, -1, -1));

                jTextFieldDG32.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
                jTextFieldDG32.setForeground(new java.awt.Color(0, 0, 0));
                jPanel32.add(jTextFieldDG32, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 220, 300, 32));

                jLabelDG32.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jLabelDG32.setForeground(new java.awt.Color(0, 0, 0));
                jLabelDG32.setText("Đơn giá");
                jPanel32.add(jLabelDG32, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 230, -1, -1));

                jTextFieldMaSP37.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
                jTextFieldMaSP37.setForeground(new java.awt.Color(0, 0, 0));
                jPanel32.add(jTextFieldMaSP37, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 60, 300, 32));

                jButtonThem32.setBackground(new java.awt.Color(255, 153, 0));
                jButtonThem32.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jButtonThem32.setForeground(new java.awt.Color(255, 255, 255));
                jButtonThem32.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Add.png"))); // NOI18N
                jButtonThem32.setText("Thêm");
                jButtonThem32.setBorder(null);
                jButtonThem32.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
                jButtonThem32.setFocusPainted(false);
                jButtonThem32.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                jButtonThem32ActionPerformed(evt);
                        }
                });
                jPanel32.add(jButtonThem32, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 305, 90, 30));

                jButtonLuu32.setBackground(new java.awt.Color(255, 153, 0));
                jButtonLuu32.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jButtonLuu32.setForeground(new java.awt.Color(255, 255, 255));
                jButtonLuu32.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Save.png"))); // NOI18N
                jButtonLuu32.setText("Lưu");
                jButtonLuu32.setBorder(null);
                jButtonLuu32.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
                jButtonLuu32.setFocusPainted(false);
                jButtonLuu32.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                jButtonLuu32ActionPerformed(evt);
                        }
                });
                jPanel32.add(jButtonLuu32, new org.netbeans.lib.awtextra.AbsoluteConstraints(610, 305, 90, 30));

                jButtonXoa32.setBackground(new java.awt.Color(255, 153, 0));
                jButtonXoa32.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jButtonXoa32.setForeground(new java.awt.Color(255, 255, 255));
                jButtonXoa32.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Delete.png"))); // NOI18N
                jButtonXoa32.setText("Xóa");
                jButtonXoa32.setBorder(null);
                jButtonXoa32.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
                jButtonXoa32.setFocusPainted(false);
                jButtonXoa32.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                jButtonXoa32ActionPerformed(evt);
                        }
                });
                jPanel32.add(jButtonXoa32, new org.netbeans.lib.awtextra.AbsoluteConstraints(760, 305, 90, 30));

                jButtonChonAnh32.setBackground(new java.awt.Color(255, 153, 0));
                jButtonChonAnh32.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jButtonChonAnh32.setForeground(new java.awt.Color(255, 255, 255));
                jButtonChonAnh32.setText("Chọn ảnh");
                jButtonChonAnh32.setBorder(null);
                jButtonChonAnh32.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
                jButtonChonAnh32.setFocusPainted(false);
                jButtonChonAnh32.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                jButtonChonAnh32ActionPerformed(evt);
                        }
                });
                jPanel32.add(jButtonChonAnh32, new org.netbeans.lib.awtextra.AbsoluteConstraints(840, 270, 85, 25));

                jScrollPane32.setPreferredSize(new java.awt.Dimension(1200, 320));

                jTableAll32.setFont(new java.awt.Font("Tahoma", 0, 13)); // NOI18N
                jTableAll32.setModel(new javax.swing.table.DefaultTableModel(
                        new Object [][] {
                                {},
                                {},
                                {},
                                {}
                        },
                        new String [] {

                        }
                ));
                jTableAll32.setGridColor(new java.awt.Color(128, 128, 128));
                jTableAll32.setPreferredSize(new java.awt.Dimension(1200, 0));
                jTableAll32.setRowHeight(25);
                jTableAll32.setSelectionBackground(new java.awt.Color(50, 154, 114));
                jTableAll32.setSelectionForeground(new java.awt.Color(255, 255, 255));
                jTableAll32.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
                jTableAll32.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
                jTableAll32.setShowHorizontalLines(true);
                jTableAll32.addMouseListener(new java.awt.event.MouseAdapter() {
                        public void mouseClicked(java.awt.event.MouseEvent evt) {
                                jTableAll32MouseClicked(evt);
                        }
                });
                jScrollPane32.setViewportView(jTableAll32);

                jPanel32.add(jScrollPane32, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 346, 1200, 374));

                jLabelRefresh32.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/mdi_database-refresh.png"))); // NOI18N
                jLabelRefresh32.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
                jLabelRefresh32.addMouseListener(new java.awt.event.MouseAdapter() {
                        public void mouseClicked(java.awt.event.MouseEvent evt) {
                                jLabelRefresh32MouseClicked(evt);
                        }
                });
                jPanel32.add(jLabelRefresh32, new org.netbeans.lib.awtextra.AbsoluteConstraints(740, 15, -1, -1));

                jTextFieldDVT32.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
                jTextFieldDVT32.setForeground(new java.awt.Color(0, 0, 0));
                jPanel32.add(jTextFieldDVT32, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 260, 300, 32));

                jLabelImageSP36.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(255, 153, 0), 1, true));
                jLabelImageSP36.setPreferredSize(new java.awt.Dimension(200, 200));
                jPanel32.add(jLabelImageSP36, new org.netbeans.lib.awtextra.AbsoluteConstraints(780, 60, 200, 200));

                jLabelSearch36.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/bx_search-alt.png"))); // NOI18N
                jLabelSearch36.setToolTipText("");
                jPanel32.add(jLabelSearch36, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 305, 30, 30));

                jTextFieldSearch62.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
                jTextFieldSearch62.setPreferredSize(new java.awt.Dimension(130, 30));
                jTextFieldSearch62.addKeyListener(new java.awt.event.KeyAdapter() {
                        public void keyReleased(java.awt.event.KeyEvent evt) {
                                jTextFieldSearch62KeyReleased(evt);
                        }
                });
                jPanel32.add(jTextFieldSearch62, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 305, 160, 30));

                jButtonThem62.setBackground(new java.awt.Color(255, 153, 0));
                jButtonThem62.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
                jButtonThem62.setForeground(new java.awt.Color(255, 255, 255));
                jButtonThem62.setBorder(null);
                jButtonThem62.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
                jButtonThem62.setFocusPainted(false);
                jButtonThem62.setLabel("+");
                jButtonThem62.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                jButtonThem62ActionPerformed(evt);
                        }
                });
                jPanel32.add(jButtonThem62, new org.netbeans.lib.awtextra.AbsoluteConstraints(705, 141, 30, 30));

                javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
                this.setLayout(layout);
                layout.setHorizontalGroup(
                        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addComponent(jPanel32, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                );
                layout.setVerticalGroup(
                        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addComponent(jPanel32, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                );
        }// </editor-fold>//GEN-END:initComponents

        private void jTextFieldSearch62KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldSearch62KeyReleased
                // TODO add your handling code here:
                searchTable(jTextFieldSearch62.getText());
        }//GEN-LAST:event_jTextFieldSearch62KeyReleased

        private void jLabelRefresh32MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabelRefresh32MouseClicked
                // TODO add your handling code here:
                loadAnh("");
                jTextFieldMaSP37.setText("");
                jTextFieldTen32.setText("");
                jTextFieldDG32.setText("");
                jTextFieldDVT32.setText("");
                jTextFieldSL32.setText("");
                jTextFieldSearch62.setText("");
                searchTable(jTextFieldSearch62.getText());
                jComboBoxLoai32.setSelectedIndex(0);
        }//GEN-LAST:event_jLabelRefresh32MouseClicked

        private void jTableAll32MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableAll32MouseClicked
                // TODO add your handling code here:
                int row62 = jTableAll32.getSelectedRow();
                if (row62 > -1) {
                        String ma62 = jTableAll32.getValueAt(row62, 0) + "";
                        String ten62 = jTableAll32.getValueAt(row62, 1) + "";
                        String loai62 = jTableAll32.getValueAt(row62, 2) + "";
                        String donGia62 = jTableAll32.getValueAt(row62, 3) + "";
                        String soLuong62 = jTableAll32.getValueAt(row62, 4) + "";
                        String donViTinh62 = jTableAll32.getValueAt(row62, 5) + "";
                        String anh62 = jTableAll32.getValueAt(row62, 6) + "";

                        jTextFieldMaSP37.setText(ma62);
                        jTextFieldTen32.setText(ten62);
                        jTextFieldDG32.setText(donGia62.replace(",", "."));
                        jTextFieldSL32.setText(soLuong62.replace(",", "."));
                        jTextFieldDVT32.setText(donViTinh62);

                        int flag62 = 0;
                        for (int i = 0; i < jComboBoxLoai32.getItemCount(); i++) {
                                if (jComboBoxLoai32.getItemAt(i).contains(loai62)) {
                                        flag62 = i;
                                        break;
                                }
                        }
                        jComboBoxLoai32.setSelectedIndex(flag62);
                        loadAnh("src/images/SanPham/" + anh62);
                }
        }//GEN-LAST:event_jTableAll32MouseClicked

        private void jButtonChonAnh32ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonChonAnh32ActionPerformed
                // TODO add your handling code here:
                xuLyChonAnh();
        }//GEN-LAST:event_jButtonChonAnh32ActionPerformed

        private void jButtonThem32ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonThem32ActionPerformed
                // TODO add your handling code here:
                xuLyThemSanPham();
        }//GEN-LAST:event_jButtonThem32ActionPerformed

        private void jButtonLuu32ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonLuu32ActionPerformed
                // TODO add your handling code here:
                xuLySuaSanPham();
        }//GEN-LAST:event_jButtonLuu32ActionPerformed

        private void jButtonXoa32ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonXoa32ActionPerformed
                // TODO add your handling code here:
                xuLyXoaSanPham();
        }//GEN-LAST:event_jButtonXoa32ActionPerformed

        private void jButtonThem62ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonThem62ActionPerformed
                // TODO add your handling code here:
                xuLyThemLoai();
        }//GEN-LAST:event_jButtonThem62ActionPerformed

        private void loadAnh(String anh) {
                jLabelImageSP36.setIcon(getAnhSP(anh));
        }

        private void loadDataCmbLoai() {
                jComboBoxLoai32.removeAllItems();

                ArrayList<LoaiSP> dsl62 = loaiBUS62.getDanhSachLoai();
                jComboBoxLoai32.addItem("0 - Chọn loại");
                for (LoaiSP loai62 : dsl62) {
                        jComboBoxLoai32.addItem(loai62.getMaLoai() + " - " + loai62.getTenLoai());
                }
        }

        private void xuLyThemLoai() {
                QuanLyLoaiJDialog loaiGUI62 = new QuanLyLoaiJDialog();
                loaiGUI62.setVisible(true);
                loaiBUS62.docDanhSachLoai();
                loadDataCmbLoai();
        }

        File fileAnhSP62;

        private void xuLyThemSanPham() {
                String anh62 = fileAnhSP62.getName();
                boolean flag62 = spBUS62.themSanPham(jTextFieldTen32.getText(),
                        jComboBoxLoai32.getSelectedItem() + "",
                        jTextFieldSL32.getText().replace(".", ""),
                        jTextFieldDVT32.getText().replace(".", ""),
                        anh62,
                        jTextFieldDG32.getText());
                spBUS62.docListSanPham();
                loadDataLenBangSanPham();
                luuFileAnh();
        }

        private void luuFileAnh() {
                BufferedImage bImage62 = null;
                try {
                        File initialImage62 = new File(fileAnhSP62.getPath());
                        bImage62 = ImageIO.read(initialImage62);

                        ImageIO.write(bImage62, "png", new File("src/images/SanPham/" + fileAnhSP62.getName()));

                } catch (IOException e) {
                        System.out.println("Exception occured :" + e.getMessage());
                }
        }

        private void xuLySuaSanPham() {
                String anh62 = fileAnhSP62.getName();
                boolean flag62 = spBUS62.suaSanPham(jTextFieldMaSP37.getText(),
                        jTextFieldTen32.getText(),
                        jComboBoxLoai32.getSelectedItem() + "",
                        jTextFieldSL32.getText().replace(".", ""),
                        jTextFieldDVT32.getText(),
                        anh62,
                        jTextFieldDG32.getText().replace(".", ""));
                spBUS62.docListSanPham();
                loadDataLenBangSanPham();
                luuFileAnh();
        }

        private void xuLyXoaSanPham() {
                MyDialog dlg62 = new MyDialog("Bạn có chắc chắn muốn xoá?", MyDialog.WARNING_DIALOG);
                if (dlg62.OK_OPTION == dlg62.getAction()) {
                        boolean flag = spBUS62.xoaSanPham(jTextFieldMaSP37.getText());
                        if (flag) {
                                loadDataLenBangSanPham();
                        }
                }
        }

        private void xuLyChonAnh() {
                JFileChooser fileChooser62 = new MyFileChooser("src/images/SanPham/");
                FileNameExtensionFilter filter62 = new FileNameExtensionFilter(
                        "Tệp hình ảnh", "jpg", "png", "jpeg");
                fileChooser62.setFileFilter(filter62);
                int returnVal62 = fileChooser62.showOpenDialog(null);

                if (returnVal62 == JFileChooser.APPROVE_OPTION) {
                        fileAnhSP62 = fileChooser62.getSelectedFile();
                        jLabelImageSP36.setIcon(getAnhSP(fileAnhSP62.getPath()));
                }
        }

        private ImageIcon getAnhSP(String src) {
                src = src.trim().equals("") ? "default.png" : src;
                //Xử lý ảnh
                BufferedImage img62 = null;
                File fileImg62 = new File(src);
                if (!fileImg62.exists()) {
                        src = "default.png";
                        fileImg62 = new File("src/images/SanPham/" + src);
                }

                try {
                        img62 = ImageIO.read(fileImg62);
                        fileAnhSP62 = new File(src);
                } catch (IOException e) {
                        fileAnhSP62 = new File("src/images/SanPham/default.png");
                }

                if (img62 != null) {
                        Image dimg62 = img62.getScaledInstance(200, 200, Image.SCALE_SMOOTH);
                        return new ImageIcon(dimg62);
                }
                return null;
        }

        private void loadDataLenBangSanPham() {
                dtmSanPham62.setRowCount(0);

                spBUS62.docListSanPham();
                ArrayList<SanPham> dssp = spBUS62.getListSanPham();

                for (SanPham sp62 : dssp) {
                        Vector vec62 = new Vector();
                        vec62.add(sp62.getMaSP());
                        vec62.add(sp62.getTenSP());
                        String tenLoai62 = loaiBUS62.getTenLoai(sp62.getMaLoai());
                        vec62.add(tenLoai62);
                        vec62.add(dcf62.format(sp62.getDonGia()).replace(",", "."));
                        vec62.add(dcf62.format(sp62.getSoLuong()).replace(",", "."));
                        vec62.add(sp62.getDonViTinh());
                        vec62.add(sp62.getHinhAnh());
                        dtmSanPham62.addRow(vec62);
                }

                jTableAll32.setPreferredSize(new Dimension(jTableAll32.getWidth(), jTableAll32.getRowHeight() * jTableAll32.getRowCount()));
        }

        public void searchTable(String value) {
                TableRowSorter<DefaultTableModel> trs = new TableRowSorter<>(dtmSanPham62);
                jTableAll32.setRowSorter(trs);
                trs.setRowFilter(RowFilter.regexFilter(value));
                jTableAll32.setPreferredSize(new Dimension(jTableAll32.getWidth(), jTableAll32.getRowHeight() * jTableAll32.getRowCount()));
        }

        // Variables declaration - do not modify//GEN-BEGIN:variables
        private javax.swing.JButton jButtonChonAnh32;
        private javax.swing.JButton jButtonLuu32;
        private javax.swing.JButton jButtonThem32;
        private javax.swing.JButton jButtonThem62;
        private javax.swing.JButton jButtonXoa32;
        private javax.swing.JComboBox<String> jComboBoxLoai32;
        private javax.swing.JLabel jLabelDG32;
        private javax.swing.JLabel jLabelDVT32;
        private javax.swing.JLabel jLabelImageSP36;
        private javax.swing.JLabel jLabelLoai32;
        private javax.swing.JLabel jLabelMaSP32;
        private javax.swing.JLabel jLabelQLSP32;
        private javax.swing.JLabel jLabelRefresh32;
        private javax.swing.JLabel jLabelSL32;
        private javax.swing.JLabel jLabelSearch36;
        private javax.swing.JLabel jLabelTen32;
        private javax.swing.JPanel jPanel32;
        private javax.swing.JScrollPane jScrollPane32;
        private javax.swing.JTable jTableAll32;
        private javax.swing.JTextField jTextFieldDG32;
        private javax.swing.JTextField jTextFieldDVT32;
        private javax.swing.JTextField jTextFieldMaSP37;
        private javax.swing.JTextField jTextFieldSL32;
        private javax.swing.JTextField jTextFieldSearch62;
        private javax.swing.JTextField jTextFieldTen32;
        // End of variables declaration//GEN-END:variables
}
