package view.SanPham;

import bus.LoaiBUS;
import custom.MyDialog;
import custom.MyTable;
import java.util.ArrayList;
import java.util.Vector;
import javax.swing.table.DefaultTableModel;
import model.LoaiSP;

public class QuanLyLoaiJDialog extends javax.swing.JDialog {

        DefaultTableModel dtmLoai62;

        public QuanLyLoaiJDialog() {
                initComponents();
                new MyTable(tblLoai50);
                dtmLoai62 = new DefaultTableModel();
                dtmLoai62.addColumn("Mã loại");
                dtmLoai62.addColumn("Tên loại");
                tblLoai50.setModel(dtmLoai62);
                loadDataLenTblLoai();
                this.setLocationRelativeTo(null);
        }

        LoaiBUS loaiBUS62 = new LoaiBUS();

        private void loadDataLenTblLoai() {
                dtmLoai62.setRowCount(0);
                ArrayList<LoaiSP> dsl62 = loaiBUS62.getDanhSachLoai();
                if (dsl62 != null) {
                        for (LoaiSP loai62 : dsl62) {
                                Vector vec62 = new Vector();
                                vec62.add(loai62.getMaLoai());
                                vec62.add(loai62.getTenLoai());
                                dtmLoai62.addRow(vec62);
                        }
                }
        }

        @SuppressWarnings("unchecked")
        // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
        private void initComponents() {

                jPanelTT50 = new javax.swing.JPanel();
                jLabelQLLoai50 = new javax.swing.JLabel();
                pnTable50 = new javax.swing.JPanel();
                jScrollPane50 = new javax.swing.JScrollPane();
                tblLoai50 = new javax.swing.JTable();
                jPanelForm50 = new javax.swing.JPanel();
                pnMaLoai50 = new javax.swing.JPanel();
                lblMaLoai50 = new javax.swing.JLabel();
                txtMaLoai50 = new javax.swing.JTextField();
                pnTenLoai50 = new javax.swing.JPanel();
                lblTenLoai50 = new javax.swing.JLabel();
                txtTenLoai50 = new javax.swing.JTextField();
                pnButton50 = new javax.swing.JPanel();
                btnThem50 = new javax.swing.JButton();
                btnSua50 = new javax.swing.JButton();
                btnXoa50 = new javax.swing.JButton();

                setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
                setLocation(new java.awt.Point(0, 0));
                setModal(true);

                jLabelQLLoai50.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
                jLabelQLLoai50.setForeground(new java.awt.Color(255, 153, 0));
                jLabelQLLoai50.setText("QUẢN LÝ LOẠI");
                jPanelTT50.add(jLabelQLLoai50);

                tblLoai50.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
                tblLoai50.setModel(new javax.swing.table.DefaultTableModel(
                        new Object [][] {

                        },
                        new String [] {
                                "Mã loại", "Tên loại"
                        }
                ));
                tblLoai50.setGridColor(new java.awt.Color(128, 128, 128));
                tblLoai50.setRowHeight(25);
                tblLoai50.setSelectionBackground(new java.awt.Color(50, 154, 114));
                tblLoai50.setSelectionForeground(new java.awt.Color(255, 255, 255));
                tblLoai50.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
                tblLoai50.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
                tblLoai50.setShowHorizontalLines(true);
                tblLoai50.getTableHeader().setReorderingAllowed(false);
                tblLoai50.addMouseListener(new java.awt.event.MouseAdapter() {
                        public void mouseClicked(java.awt.event.MouseEvent evt) {
                                tblLoai50MouseClicked(evt);
                        }
                });
                jScrollPane50.setViewportView(tblLoai50);

                javax.swing.GroupLayout pnTable50Layout = new javax.swing.GroupLayout(pnTable50);
                pnTable50.setLayout(pnTable50Layout);
                pnTable50Layout.setHorizontalGroup(
                        pnTable50Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jScrollPane50, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                );
                pnTable50Layout.setVerticalGroup(
                        pnTable50Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(pnTable50Layout.createSequentialGroup()
                                .addComponent(jScrollPane50, javax.swing.GroupLayout.PREFERRED_SIZE, 205, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                );

                jPanelForm50.setLayout(new javax.swing.BoxLayout(jPanelForm50, javax.swing.BoxLayout.Y_AXIS));

                lblMaLoai50.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
                lblMaLoai50.setText("Mã loại");
                lblMaLoai50.setPreferredSize(new java.awt.Dimension(63, 22));
                pnMaLoai50.add(lblMaLoai50);

                txtMaLoai50.setEditable(false);
                txtMaLoai50.setColumns(15);
                txtMaLoai50.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
                pnMaLoai50.add(txtMaLoai50);

                jPanelForm50.add(pnMaLoai50);

                lblTenLoai50.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
                lblTenLoai50.setText("Tên loại");
                pnTenLoai50.add(lblTenLoai50);

                txtTenLoai50.setColumns(15);
                txtTenLoai50.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
                pnTenLoai50.add(txtTenLoai50);

                jPanelForm50.add(pnTenLoai50);

                btnThem50.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
                btnThem50.setText("Thêm");
                btnThem50.setPreferredSize(new java.awt.Dimension(80, 35));
                btnThem50.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                btnThem50ActionPerformed(evt);
                        }
                });
                pnButton50.add(btnThem50);

                btnSua50.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
                btnSua50.setText("Sửa");
                btnSua50.setPreferredSize(new java.awt.Dimension(80, 35));
                btnSua50.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                btnSua50ActionPerformed(evt);
                        }
                });
                pnButton50.add(btnSua50);

                btnXoa50.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
                btnXoa50.setText("Xoá");
                btnXoa50.setPreferredSize(new java.awt.Dimension(80, 35));
                btnXoa50.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                btnXoa50ActionPerformed(evt);
                        }
                });
                pnButton50.add(btnXoa50);

                jPanelForm50.add(pnButton50);

                javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
                getContentPane().setLayout(layout);
                layout.setHorizontalGroup(
                        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jPanelTT50, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanelForm50, javax.swing.GroupLayout.DEFAULT_SIZE, 446, Short.MAX_VALUE)
                        .addComponent(pnTable50, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                );
                layout.setVerticalGroup(
                        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addComponent(jPanelTT50, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(pnTable50, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jPanelForm50, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                );

                pack();
        }// </editor-fold>//GEN-END:initComponents

    private void tblLoai50MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblLoai50MouseClicked
            int row62 = tblLoai50.getSelectedRow();
            if (row62 > -1) {
                    String maLoai62 = tblLoai50.getValueAt(row62, 0) + "";
                    String tenLoai62 = tblLoai50.getValueAt(row62, 1) + "";
                    txtMaLoai50.setText(maLoai62);
                    txtTenLoai50.setText(tenLoai62);
            }
    }//GEN-LAST:event_tblLoai50MouseClicked

    private void btnThem50ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnThem50ActionPerformed
            // TODO add your handling code here:
            if (loaiBUS62.themLoai(dtmLoai62.getRowCount(), txtTenLoai50.getText())) {
                    loaiBUS62.docDanhSachLoai();
                    loadDataLenTblLoai();
            }
    }//GEN-LAST:event_btnThem50ActionPerformed

    private void btnXoa50ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnXoa50ActionPerformed
            MyDialog dlg62 = new MyDialog("Bạn có chắc chắn muốn xoá?", MyDialog.WARNING_DIALOG);
            if (dlg62.OK_OPTION == dlg62.getAction()) {
                    String ma62 = txtMaLoai50.getText();
                    if (loaiBUS62.xoaLoai(ma62)) {
                            loaiBUS62.docDanhSachLoai();
                            loadDataLenTblLoai();
                    }
            }
    }//GEN-LAST:event_btnXoa50ActionPerformed

    private void btnSua50ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSua50ActionPerformed
            String ma62 = txtMaLoai50.getText();
            String ten62 = txtTenLoai50.getText();
            if (loaiBUS62.suaLoai(ma62, ten62)) {
                    loaiBUS62.docDanhSachLoai();
                    loadDataLenTblLoai();
            }
    }//GEN-LAST:event_btnSua50ActionPerformed

        // Variables declaration - do not modify//GEN-BEGIN:variables
        private javax.swing.JButton btnSua50;
        private javax.swing.JButton btnThem50;
        private javax.swing.JButton btnXoa50;
        private javax.swing.JLabel jLabelQLLoai50;
        private javax.swing.JPanel jPanelForm50;
        private javax.swing.JPanel jPanelTT50;
        private javax.swing.JScrollPane jScrollPane50;
        private javax.swing.JLabel lblMaLoai50;
        private javax.swing.JLabel lblTenLoai50;
        private javax.swing.JPanel pnButton50;
        private javax.swing.JPanel pnMaLoai50;
        private javax.swing.JPanel pnTable50;
        private javax.swing.JPanel pnTenLoai50;
        private javax.swing.JTable tblLoai50;
        private javax.swing.JTextField txtMaLoai50;
        private javax.swing.JTextField txtTenLoai50;
        // End of variables declaration//GEN-END:variables
}
