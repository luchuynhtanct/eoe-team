package view.HoaDon;

import bus.CTHoaDonBUS;
import bus.HoaDonBUS;
import bus.SanPhamBUS;
import custom.MyTable;
import java.awt.Dimension;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Vector;
import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import model.CTHoaDon;
import model.HoaDon;

public class HoaDonJPanel extends javax.swing.JPanel {

        private HoaDonBUS hoaDonBUS62 = new HoaDonBUS();
        private SanPhamBUS spBUS62 = new SanPhamBUS();
        DecimalFormat dcf62 = new DecimalFormat("###,###");
        SimpleDateFormat sdf62 = new SimpleDateFormat("dd/MM/yyyy");
        DefaultTableModel dtmCTHoaDon62 = new DefaultTableModel();

        public HoaDonJPanel() {
                initComponents();

                DefaultTableCellRenderer centerRenderer62 = new DefaultTableCellRenderer();
                centerRenderer62.setHorizontalAlignment(JLabel.CENTER);

                dtmCTHoaDon62 = new DefaultTableModel() {
                        @Override
                        public boolean isCellEditable(int row62, int column) {
                                return false;
                        }
                };
                new MyTable(jTableCTHoaDon36);
                jTableCTHoaDon36.setModel(dtmCTHoaDon62);
                dtmCTHoaDon62.addColumn("Mã HD");
                dtmCTHoaDon62.addColumn("Mã SP");
                dtmCTHoaDon62.addColumn("Số lượng");
                dtmCTHoaDon62.addColumn("Đơn giá");
                dtmCTHoaDon62.addColumn("Thành tiền");

                jTableCTHoaDon36.getColumnModel().getColumn(0).setCellRenderer(centerRenderer62);
                jTableCTHoaDon36.getColumnModel().getColumn(1).setCellRenderer(centerRenderer62);
                jTableCTHoaDon36.getColumnModel().getColumn(2).setCellRenderer(centerRenderer62);
                jTableCTHoaDon36.getColumnModel().getColumn(3).setCellRenderer(centerRenderer62);
                jTableCTHoaDon36.getColumnModel().getColumn(4).setCellRenderer(centerRenderer62);
                TableColumnModel columnModelBanHang62 = jTableCTHoaDon36.getColumnModel();
                columnModelBanHang62.getColumn(0).setPreferredWidth(50);
                columnModelBanHang62.getColumn(1).setPreferredWidth(50);
                columnModelBanHang62.getColumn(2).setPreferredWidth(60);
                columnModelBanHang62.getColumn(3).setPreferredWidth(90);
                columnModelBanHang62.getColumn(4).setPreferredWidth(90);

                loadDataListHoaDon();
                loadDataTblCTHoaDon();
        }

        @SuppressWarnings("unchecked")
        // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
        private void initComponents() {

                jPanelView62 = new javax.swing.JPanel();
                jPanelCTHD36 = new javax.swing.JPanel();
                jScrollPaneCTHD36 = new javax.swing.JScrollPane();
                jTableCTHoaDon36 = new javax.swing.JTable();
                jLabelCTSP36 = new javax.swing.JLabel();
                jLabelRefreshDbCT62 = new javax.swing.JLabel();
                jPanelTTCTHDD36 = new javax.swing.JPanel();
                MaHDct36 = new javax.swing.JLabel();
                jLabelTTct36 = new javax.swing.JLabel();
                jLabelSPct36 = new javax.swing.JLabel();
                jLabelSLct36 = new javax.swing.JLabel();
                jLabelDGct36 = new javax.swing.JLabel();
                jTextFieldMaHDct36 = new javax.swing.JTextField();
                jTextFieldTenSPct36 = new javax.swing.JTextField();
                jTextFieldSoLuong36 = new javax.swing.JTextField();
                jTextFieldDonGia36 = new javax.swing.JTextField();
                jTextFieldThanhTien36 = new javax.swing.JTextField();
                jPanelTTHD36 = new javax.swing.JPanel();
                jPanelTTCTHD36 = new javax.swing.JPanel();
                jLabelMaHD36 = new javax.swing.JLabel();
                jLabelTT36 = new javax.swing.JLabel();
                jLabelMaKH36 = new javax.swing.JLabel();
                jLabelMaNV36 = new javax.swing.JLabel();
                jLabelDate36 = new javax.swing.JLabel();
                jLabelGhiChu36 = new javax.swing.JLabel();
                jTextFieldMaHD36 = new javax.swing.JTextField();
                jTextFieldMaKH36 = new javax.swing.JTextField();
                jTextFieldMaNV36 = new javax.swing.JTextField();
                jTextFieldNgayLap36 = new javax.swing.JTextField();
                jTextFieldTongTien36 = new javax.swing.JTextField();
                jTextFieldGhiChu36 = new javax.swing.JTextField();
                jLabelDSSP36 = new javax.swing.JLabel();
                jScrollPaneTTHD36 = new javax.swing.JScrollPane();
                jListHoaDon36 = new javax.swing.JList<>();
                jPanelLoc36 = new javax.swing.JPanel();
                jLabelPrFr36 = new javax.swing.JLabel();
                jTextFieldDonGiaFrom36 = new javax.swing.JTextField();
                jLabelPrTo36 = new javax.swing.JLabel();
                jTextFieldDonGiaTo36 = new javax.swing.JTextField();
                jLabelDateFr36 = new javax.swing.JLabel();
                jTextFieldNgayFrom36 = new javax.swing.JTextField();
                jLabelDateTo36 = new javax.swing.JLabel();
                jTextFieldNgayTo36 = new javax.swing.JTextField();
                jLabelSearchPrice62 = new javax.swing.JLabel();
                jLabelSearchDate62 = new javax.swing.JLabel();
                jLabelRefreshDb62 = new javax.swing.JLabel();

                setPreferredSize(new java.awt.Dimension(1200, 690));

                jPanelView62.setBackground(new java.awt.Color(238, 196, 70));
                jPanelView62.setMinimumSize(new java.awt.Dimension(1110, 630));
                jPanelView62.setPreferredSize(new java.awt.Dimension(1200, 690));
                jPanelView62.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

                jPanelCTHD36.setBackground(new java.awt.Color(245, 219, 137));
                jPanelCTHD36.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 153, 0)));
                jPanelCTHD36.setPreferredSize(new java.awt.Dimension(600, 655));
                jPanelCTHD36.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

                jScrollPaneCTHD36.setPreferredSize(new java.awt.Dimension(600, 455));

                jTableCTHoaDon36.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
                jTableCTHoaDon36.setModel(new javax.swing.table.DefaultTableModel(
                        new Object [][] {
                                {null, null, null, null},
                                {null, null, null, null},
                                {null, null, null, null},
                                {null, null, null, null}
                        },
                        new String [] {
                                "Title 1", "Title 2", "Title 3", "Title 4"
                        }
                ));
                jTableCTHoaDon36.setGridColor(new java.awt.Color(128, 128, 128));
                jTableCTHoaDon36.setPreferredSize(new java.awt.Dimension(600, 0));
                jTableCTHoaDon36.setSelectionBackground(new java.awt.Color(50, 154, 114));
                jTableCTHoaDon36.setSelectionForeground(new java.awt.Color(255, 255, 255));
                jTableCTHoaDon36.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
                jTableCTHoaDon36.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
                jTableCTHoaDon36.setShowHorizontalLines(true);
                jTableCTHoaDon36.addMouseListener(new java.awt.event.MouseAdapter() {
                        public void mouseClicked(java.awt.event.MouseEvent evt) {
                                jTableCTHoaDon36MouseClicked(evt);
                        }
                });
                jScrollPaneCTHD36.setViewportView(jTableCTHoaDon36);

                jPanelCTHD36.add(jScrollPaneCTHD36, new org.netbeans.lib.awtextra.AbsoluteConstraints(2, 240, 600, 455));

                jLabelCTSP36.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
                jLabelCTSP36.setForeground(new java.awt.Color(102, 51, 0));
                jLabelCTSP36.setText("Chi tiết hóa đơn");
                jPanelCTHD36.add(jLabelCTSP36, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 15, -1, -1));

                jLabelRefreshDbCT62.setBackground(new java.awt.Color(245, 219, 137));
                jLabelRefreshDbCT62.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
                jLabelRefreshDbCT62.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/mdi_database-refresh.png"))); // NOI18N
                jLabelRefreshDbCT62.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
                jLabelRefreshDbCT62.addMouseListener(new java.awt.event.MouseAdapter() {
                        public void mouseClicked(java.awt.event.MouseEvent evt) {
                                jLabelRefreshDbCT62MouseClicked(evt);
                        }
                });
                jPanelCTHD36.add(jLabelRefreshDbCT62, new org.netbeans.lib.awtextra.AbsoluteConstraints(385, 10, 30, 30));

                jPanelTTCTHDD36.setBackground(new java.awt.Color(245, 219, 137));

                MaHDct36.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                MaHDct36.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
                MaHDct36.setText("Mã hóa đơn");

                jLabelTTct36.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jLabelTTct36.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
                jLabelTTct36.setText("Thành tiền");

                jLabelSPct36.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jLabelSPct36.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
                jLabelSPct36.setText("Sản phẩm");

                jLabelSLct36.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jLabelSLct36.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
                jLabelSLct36.setText("Số lượng");

                jLabelDGct36.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jLabelDGct36.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
                jLabelDGct36.setText("Đơn giá");

                jTextFieldMaHDct36.setBackground(java.awt.Color.white);
                jTextFieldMaHDct36.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jTextFieldMaHDct36.setForeground(new java.awt.Color(0, 0, 0));
                jTextFieldMaHDct36.setEnabled(false);

                jTextFieldTenSPct36.setBackground(java.awt.Color.white);
                jTextFieldTenSPct36.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jTextFieldTenSPct36.setForeground(new java.awt.Color(0, 0, 0));
                jTextFieldTenSPct36.setEnabled(false);

                jTextFieldSoLuong36.setBackground(java.awt.Color.white);
                jTextFieldSoLuong36.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jTextFieldSoLuong36.setForeground(new java.awt.Color(0, 0, 0));
                jTextFieldSoLuong36.setEnabled(false);

                jTextFieldDonGia36.setBackground(java.awt.Color.white);
                jTextFieldDonGia36.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jTextFieldDonGia36.setForeground(new java.awt.Color(0, 0, 0));
                jTextFieldDonGia36.setEnabled(false);

                jTextFieldThanhTien36.setBackground(java.awt.Color.white);
                jTextFieldThanhTien36.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jTextFieldThanhTien36.setForeground(new java.awt.Color(0, 0, 0));
                jTextFieldThanhTien36.setEnabled(false);

                javax.swing.GroupLayout jPanelTTCTHDD36Layout = new javax.swing.GroupLayout(jPanelTTCTHDD36);
                jPanelTTCTHDD36.setLayout(jPanelTTCTHDD36Layout);
                jPanelTTCTHDD36Layout.setHorizontalGroup(
                        jPanelTTCTHDD36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelTTCTHDD36Layout.createSequentialGroup()
                                .addGroup(jPanelTTCTHDD36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanelTTCTHDD36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                .addComponent(jLabelDGct36)
                                                .addComponent(jLabelSLct36)
                                                .addComponent(MaHDct36)
                                                .addComponent(jLabelSPct36))
                                        .addGroup(jPanelTTCTHDD36Layout.createSequentialGroup()
                                                .addContainerGap()
                                                .addComponent(jLabelTTct36)))
                                .addGap(38, 38, 38)
                                .addGroup(jPanelTTCTHDD36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jTextFieldThanhTien36, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jTextFieldMaHDct36, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jTextFieldTenSPct36, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jTextFieldSoLuong36, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jTextFieldDonGia36, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(60, 60, 60))
                );
                jPanelTTCTHDD36Layout.setVerticalGroup(
                        jPanelTTCTHDD36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelTTCTHDD36Layout.createSequentialGroup()
                                .addGap(23, 23, 23)
                                .addGroup(jPanelTTCTHDD36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jTextFieldMaHDct36, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(MaHDct36))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanelTTCTHDD36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jTextFieldTenSPct36, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabelSPct36))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanelTTCTHDD36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jTextFieldSoLuong36, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabelSLct36))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanelTTCTHDD36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jTextFieldDonGia36, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabelDGct36))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanelTTCTHDD36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jTextFieldThanhTien36, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabelTTct36))
                                .addContainerGap(13, Short.MAX_VALUE))
                );

                jPanelCTHD36.add(jPanelTTCTHDD36, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 30, 360, 210));

                jPanelView62.add(jPanelCTHD36, new org.netbeans.lib.awtextra.AbsoluteConstraints(601, -5, 600, 695));

                jPanelTTHD36.setBackground(new java.awt.Color(245, 219, 137));
                jPanelTTHD36.setPreferredSize(new java.awt.Dimension(623, 655));
                jPanelTTHD36.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

                jPanelTTCTHD36.setBackground(new java.awt.Color(245, 219, 137));

                jLabelMaHD36.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jLabelMaHD36.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
                jLabelMaHD36.setText("Mã hóa đơn");

                jLabelTT36.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jLabelTT36.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
                jLabelTT36.setText("Tổng tiền");

                jLabelMaKH36.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jLabelMaKH36.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
                jLabelMaKH36.setText("Mã khách hàng");

                jLabelMaNV36.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jLabelMaNV36.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
                jLabelMaNV36.setText("Mã nhân viên");

                jLabelDate36.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jLabelDate36.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
                jLabelDate36.setText("Ngày");

                jLabelGhiChu36.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jLabelGhiChu36.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
                jLabelGhiChu36.setText("Ghi chú");

                jTextFieldMaHD36.setBackground(java.awt.Color.white);
                jTextFieldMaHD36.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jTextFieldMaHD36.setForeground(new java.awt.Color(0, 0, 0));
                jTextFieldMaHD36.setEnabled(false);

                jTextFieldMaKH36.setBackground(java.awt.Color.white);
                jTextFieldMaKH36.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jTextFieldMaKH36.setForeground(new java.awt.Color(0, 0, 0));
                jTextFieldMaKH36.setEnabled(false);

                jTextFieldMaNV36.setBackground(java.awt.Color.white);
                jTextFieldMaNV36.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jTextFieldMaNV36.setForeground(new java.awt.Color(0, 0, 0));
                jTextFieldMaNV36.setEnabled(false);

                jTextFieldNgayLap36.setBackground(java.awt.Color.white);
                jTextFieldNgayLap36.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jTextFieldNgayLap36.setForeground(new java.awt.Color(0, 0, 0));
                jTextFieldNgayLap36.setEnabled(false);

                jTextFieldTongTien36.setBackground(java.awt.Color.white);
                jTextFieldTongTien36.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jTextFieldTongTien36.setForeground(new java.awt.Color(0, 0, 0));
                jTextFieldTongTien36.setEnabled(false);

                jTextFieldGhiChu36.setBackground(java.awt.Color.white);
                jTextFieldGhiChu36.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jTextFieldGhiChu36.setForeground(new java.awt.Color(0, 0, 0));
                jTextFieldGhiChu36.setEnabled(false);

                javax.swing.GroupLayout jPanelTTCTHD36Layout = new javax.swing.GroupLayout(jPanelTTCTHD36);
                jPanelTTCTHD36.setLayout(jPanelTTCTHD36Layout);
                jPanelTTCTHD36Layout.setHorizontalGroup(
                        jPanelTTCTHD36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelTTCTHD36Layout.createSequentialGroup()
                                .addGroup(jPanelTTCTHD36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jLabelTT36)
                                        .addComponent(jLabelDate36)
                                        .addComponent(jLabelMaNV36)
                                        .addComponent(jLabelMaHD36)
                                        .addComponent(jLabelMaKH36)
                                        .addComponent(jLabelGhiChu36))
                                .addGap(38, 38, 38)
                                .addGroup(jPanelTTCTHD36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(jTextFieldMaKH36)
                                        .addComponent(jTextFieldMaNV36)
                                        .addComponent(jTextFieldTongTien36)
                                        .addComponent(jTextFieldNgayLap36)
                                        .addComponent(jTextFieldGhiChu36)
                                        .addComponent(jTextFieldMaHD36, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(60, 60, 60))
                );
                jPanelTTCTHD36Layout.setVerticalGroup(
                        jPanelTTCTHD36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelTTCTHD36Layout.createSequentialGroup()
                                .addGroup(jPanelTTCTHD36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanelTTCTHD36Layout.createSequentialGroup()
                                                .addGap(6, 6, 6)
                                                .addComponent(jLabelMaHD36))
                                        .addComponent(jTextFieldMaHD36, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanelTTCTHD36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jTextFieldMaKH36, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabelMaKH36))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanelTTCTHD36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jTextFieldMaNV36, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabelMaNV36))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanelTTCTHD36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jTextFieldNgayLap36, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabelDate36))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanelTTCTHD36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jTextFieldTongTien36, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabelTT36))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanelTTCTHD36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jTextFieldGhiChu36, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabelGhiChu36))
                                .addContainerGap(10, Short.MAX_VALUE))
                );

                jPanelTTHD36.add(jPanelTTCTHD36, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 50, 380, 220));

                jLabelDSSP36.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
                jLabelDSSP36.setForeground(new java.awt.Color(102, 51, 0));
                jLabelDSSP36.setText("Thông tin hóa đơn");
                jPanelTTHD36.add(jLabelDSSP36, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 10, -1, -1));

                jScrollPaneTTHD36.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
                jScrollPaneTTHD36.setPreferredSize(new java.awt.Dimension(600, 315));

                jListHoaDon36.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
                jListHoaDon36.setModel(new javax.swing.AbstractListModel<String>() {
                        String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
                        public int getSize() { return strings.length; }
                        public String getElementAt(int i) { return strings[i]; }
                });
                jListHoaDon36.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
                jListHoaDon36.setFixedCellHeight(25);
                jListHoaDon36.setPreferredSize(new java.awt.Dimension(600, 600));
                jListHoaDon36.setSelectionBackground(new java.awt.Color(50, 154, 114));
                jListHoaDon36.setSelectionForeground(new java.awt.Color(255, 255, 255));
                jListHoaDon36.setVisibleRowCount(4);
                jListHoaDon36.addMouseListener(new java.awt.event.MouseAdapter() {
                        public void mouseClicked(java.awt.event.MouseEvent evt) {
                                jListHoaDon36MouseClicked(evt);
                        }
                });
                jScrollPaneTTHD36.setViewportView(jListHoaDon36);

                jPanelTTHD36.add(jScrollPaneTTHD36, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 375, 600, 315));

                jPanelLoc36.setBackground(new java.awt.Color(245, 219, 137));

                jLabelPrFr36.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jLabelPrFr36.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
                jLabelPrFr36.setText("Giá từ:");

                jTextFieldDonGiaFrom36.setBackground(java.awt.Color.white);
                jTextFieldDonGiaFrom36.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jTextFieldDonGiaFrom36.setForeground(new java.awt.Color(0, 0, 0));

                jLabelPrTo36.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jLabelPrTo36.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
                jLabelPrTo36.setText("đến:");

                jTextFieldDonGiaTo36.setBackground(java.awt.Color.white);
                jTextFieldDonGiaTo36.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jTextFieldDonGiaTo36.setForeground(new java.awt.Color(0, 0, 0));

                jLabelDateFr36.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jLabelDateFr36.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
                jLabelDateFr36.setText("Ngày từ:");

                jTextFieldNgayFrom36.setBackground(java.awt.Color.white);
                jTextFieldNgayFrom36.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jTextFieldNgayFrom36.setForeground(new java.awt.Color(0, 0, 0));

                jLabelDateTo36.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jLabelDateTo36.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
                jLabelDateTo36.setText("đến:");

                jTextFieldNgayTo36.setBackground(java.awt.Color.white);
                jTextFieldNgayTo36.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jTextFieldNgayTo36.setForeground(new java.awt.Color(0, 0, 0));

                jLabelSearchPrice62.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/bx_search-alt.png"))); // NOI18N
                jLabelSearchPrice62.setToolTipText("");
                jLabelSearchPrice62.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
                jLabelSearchPrice62.addMouseListener(new java.awt.event.MouseAdapter() {
                        public void mouseClicked(java.awt.event.MouseEvent evt) {
                                jLabelSearchPrice62MouseClicked(evt);
                        }
                });

                jLabelSearchDate62.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/bx_search-alt.png"))); // NOI18N
                jLabelSearchDate62.setToolTipText("");
                jLabelSearchDate62.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
                jLabelSearchDate62.addMouseListener(new java.awt.event.MouseAdapter() {
                        public void mouseClicked(java.awt.event.MouseEvent evt) {
                                jLabelSearchDate62MouseClicked(evt);
                        }
                });

                javax.swing.GroupLayout jPanelLoc36Layout = new javax.swing.GroupLayout(jPanelLoc36);
                jPanelLoc36.setLayout(jPanelLoc36Layout);
                jPanelLoc36Layout.setHorizontalGroup(
                        jPanelLoc36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelLoc36Layout.createSequentialGroup()
                                .addGroup(jPanelLoc36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addGroup(jPanelLoc36Layout.createSequentialGroup()
                                                .addGap(26, 26, 26)
                                                .addComponent(jLabelPrFr36)
                                                .addGap(18, 18, 18)
                                                .addComponent(jTextFieldDonGiaFrom36, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(jPanelLoc36Layout.createSequentialGroup()
                                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(jLabelDateFr36)
                                                .addGap(18, 18, 18)
                                                .addComponent(jTextFieldNgayFrom36, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(18, 18, 18)
                                .addGroup(jPanelLoc36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanelLoc36Layout.createSequentialGroup()
                                                .addComponent(jLabelPrTo36)
                                                .addGap(18, 18, 18)
                                                .addComponent(jTextFieldDonGiaTo36, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(jLabelSearchPrice62))
                                        .addGroup(jPanelLoc36Layout.createSequentialGroup()
                                                .addComponent(jLabelDateTo36)
                                                .addGap(18, 18, 18)
                                                .addComponent(jTextFieldNgayTo36, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(jLabelSearchDate62)))
                                .addContainerGap(9, Short.MAX_VALUE))
                );
                jPanelLoc36Layout.setVerticalGroup(
                        jPanelLoc36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelLoc36Layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(jPanelLoc36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanelLoc36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                .addComponent(jTextFieldDonGiaTo36, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(jLabelPrTo36))
                                        .addGroup(jPanelLoc36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                .addComponent(jTextFieldDonGiaFrom36, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(jLabelPrFr36))
                                        .addComponent(jLabelSearchPrice62))
                                .addGap(18, 18, 18)
                                .addGroup(jPanelLoc36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanelLoc36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                .addComponent(jTextFieldNgayTo36, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(jLabelDateTo36))
                                        .addGroup(jPanelLoc36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                .addComponent(jTextFieldNgayFrom36, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(jLabelDateFr36))
                                        .addComponent(jLabelSearchDate62))
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                );

                jPanelTTHD36.add(jPanelLoc36, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 270, 550, 90));

                jLabelRefreshDb62.setBackground(new java.awt.Color(245, 219, 137));
                jLabelRefreshDb62.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
                jLabelRefreshDb62.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/mdi_database-refresh.png"))); // NOI18N
                jLabelRefreshDb62.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
                jLabelRefreshDb62.addMouseListener(new java.awt.event.MouseAdapter() {
                        public void mouseClicked(java.awt.event.MouseEvent evt) {
                                jLabelRefreshDb62MouseClicked(evt);
                        }
                });
                jPanelTTHD36.add(jLabelRefreshDb62, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 5, 30, 30));

                jPanelView62.add(jPanelTTHD36, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 600, 690));

                javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
                this.setLayout(layout);
                layout.setHorizontalGroup(
                        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 1200, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(jPanelView62, javax.swing.GroupLayout.PREFERRED_SIZE, 1200, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE)))
                );
                layout.setVerticalGroup(
                        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 690, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(jPanelView62, javax.swing.GroupLayout.PREFERRED_SIZE, 690, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE)))
                );
        }// </editor-fold>//GEN-END:initComponents

        private void jListHoaDon36MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jListHoaDon36MouseClicked
                // TODO add your handling code here:
                xuLyHienCTHoaDon();
        }//GEN-LAST:event_jListHoaDon36MouseClicked

        private void jTableCTHoaDon36MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableCTHoaDon36MouseClicked
                // TODO add your handling code here:
                xuLyClickTblCTHoaDon();
        }//GEN-LAST:event_jTableCTHoaDon36MouseClicked

        private void jLabelRefreshDb62MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabelRefreshDb62MouseClicked
                // TODO add your handling code here:
                loadDataListHoaDon();
                loadDataTblCTHoaDon();
        }//GEN-LAST:event_jLabelRefreshDb62MouseClicked

        private void jLabelRefreshDbCT62MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabelRefreshDbCT62MouseClicked
                // TODO add your handling code here:
                loadDataTblCTHoaDon();
        }//GEN-LAST:event_jLabelRefreshDbCT62MouseClicked

        private void jLabelSearchPrice62MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabelSearchPrice62MouseClicked
                // TODO add your handling code here:
                xuLyTimTheoKhoangGia();
        }//GEN-LAST:event_jLabelSearchPrice62MouseClicked

        private void jLabelSearchDate62MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabelSearchDate62MouseClicked
                // TODO add your handling code here:
                xuLyTimTheoKhoangNgay();
        }//GEN-LAST:event_jLabelSearchDate62MouseClicked

        private void xuLyTimTheoKhoangNgay() {
                ArrayList<HoaDon> listHoaDon62 = hoaDonBUS62.getListHoaDonTheoNgay(jTextFieldNgayFrom36.getText(), jTextFieldNgayTo36.getText());
                addDataListHoaDon(listHoaDon62);
        }

        private void xuLyTimTheoKhoangGia() {
                ArrayList<HoaDon> listHoaDon62 = hoaDonBUS62.getListHoaDonTheoGia(jTextFieldDonGiaFrom36.getText(), jTextFieldDonGiaTo36.getText());
                addDataListHoaDon(listHoaDon62);
        }

        private void loadDataListHoaDon() {
                ArrayList<HoaDon> dshd6262 = hoaDonBUS62.getListHoaDon();
                addDataListHoaDon(dshd6262);
        }

        private void addDataListHoaDon(ArrayList<HoaDon> dshd6262) {
                DefaultListModel<String> listModel62 = new DefaultListModel<>();
                if (dshd6262 != null) {
                        for (HoaDon hd62 : dshd6262) {
                                listModel62.addElement(hd62.getMaHD() + " | " + sdf62.format(hd62.getNgayLap()) + " | " + dcf62.format(hd62.getTongTien()).replace(",", ".") + " VND");
                        }
                        jListHoaDon36.setModel(listModel62);
                }

                try {
                        jListHoaDon36.setPreferredSize(new Dimension(jListHoaDon36.getWidth(), dshd6262.size() * jListHoaDon36.getFixedCellHeight()));
                } catch (NullPointerException e) {
                }
        }

        private void xuLyHienCTHoaDon() {
                String hoaDon62 = jListHoaDon36.getSelectedValue();
                String[] stMaHD62 = hoaDon62.split(" | ");

                HoaDon hd62 = hoaDonBUS62.getHoaDon(stMaHD62[0]);
                jTextFieldMaHD36.setText(hd62.getMaHD() + "");
                jTextFieldMaKH36.setText(hd62.getMaKH() + "");
                jTextFieldMaNV36.setText(hd62.getMaNV() + "");

                jTextFieldNgayLap36.setText(sdf62.format(hd62.getNgayLap()));
                jTextFieldTongTien36.setText(dcf62.format(hd62.getTongTien()).replace(",", "."));
                jTextFieldGhiChu36.setText(hd62.getGhiChu());

                // Gọi hiển thị data trên tblCTHoaDon
                loadDataTblCTHoaDon(stMaHD62[0]);
        }

        private CTHoaDonBUS ctHDBUS62 = new CTHoaDonBUS();

        private void loadDataTblCTHoaDon() {
                ctHDBUS62.docListCTHoaDon();
                ArrayList<CTHoaDon> listCTHoaDon62 = ctHDBUS62.getListCTHoaDon();
                addDataTableCTHoaDon(listCTHoaDon62);
        }

        private void addDataTableCTHoaDon(ArrayList<CTHoaDon> listCTHoaDon62) {
                dtmCTHoaDon62.setRowCount(0);
                for (CTHoaDon ct : listCTHoaDon62) {
                        Vector<String> vec62 = new Vector<>();
                        vec62.add(ct.getMaHD() + "");
                        vec62.add(ct.getMaSP() + "");
                        vec62.add(ct.getSoLuong() + "");
                        vec62.add(dcf62.format(ct.getDonGia()).replace(",", "."));
                        vec62.add(dcf62.format(ct.getThanhTien()).replace(",", "."));
                        dtmCTHoaDon62.addRow(vec62);
                }
                jTableCTHoaDon36.setPreferredSize(new Dimension(jTableCTHoaDon36.getWidth(), jTableCTHoaDon36.getRowHeight() * jTableCTHoaDon36.getRowCount()));
        }

        private void loadDataTblCTHoaDon(String maHD62) {
                ArrayList<CTHoaDon> listCTHoaDon62 = ctHDBUS62.getListCTHoaDonTheoMaHD(maHD62);

                addDataTableCTHoaDon(listCTHoaDon62);
        }

        private void xuLyClickTblCTHoaDon() {
                int row62 = jTableCTHoaDon36.getSelectedRow();
                if (row62 > -1) {
                        String maHD62 = jTableCTHoaDon36.getValueAt(row62, 0) + "";
                        String maSP62 = spBUS62.getSanPham("" + jTableCTHoaDon36.getValueAt(row62, 1)).getTenSP();
                        String soLuong62 = jTableCTHoaDon36.getValueAt(row62, 2) + "";
                        String donGia62 = jTableCTHoaDon36.getValueAt(row62, 3) + "";
                        String thanhTien62 = jTableCTHoaDon36.getValueAt(row62, 4) + "";

                        jTextFieldMaHDct36.setText(maHD62);
                        jTextFieldTenSPct36.setText(maSP62);
                        jTextFieldSoLuong36.setText(soLuong62);
                        jTextFieldDonGia36.setText(donGia62);
                        jTextFieldThanhTien36.setText(thanhTien62);
                }
        }

        // Variables declaration - do not modify//GEN-BEGIN:variables
        private javax.swing.JLabel MaHDct36;
        private javax.swing.JLabel jLabelCTSP36;
        private javax.swing.JLabel jLabelDGct36;
        private javax.swing.JLabel jLabelDSSP36;
        private javax.swing.JLabel jLabelDate36;
        private javax.swing.JLabel jLabelDateFr36;
        private javax.swing.JLabel jLabelDateTo36;
        private javax.swing.JLabel jLabelGhiChu36;
        private javax.swing.JLabel jLabelMaHD36;
        private javax.swing.JLabel jLabelMaKH36;
        private javax.swing.JLabel jLabelMaNV36;
        private javax.swing.JLabel jLabelPrFr36;
        private javax.swing.JLabel jLabelPrTo36;
        private javax.swing.JLabel jLabelRefreshDb62;
        private javax.swing.JLabel jLabelRefreshDbCT62;
        private javax.swing.JLabel jLabelSLct36;
        private javax.swing.JLabel jLabelSPct36;
        private javax.swing.JLabel jLabelSearchDate62;
        private javax.swing.JLabel jLabelSearchPrice62;
        private javax.swing.JLabel jLabelTT36;
        private javax.swing.JLabel jLabelTTct36;
        private javax.swing.JList<String> jListHoaDon36;
        private javax.swing.JPanel jPanelCTHD36;
        private javax.swing.JPanel jPanelLoc36;
        private javax.swing.JPanel jPanelTTCTHD36;
        private javax.swing.JPanel jPanelTTCTHDD36;
        private javax.swing.JPanel jPanelTTHD36;
        private javax.swing.JPanel jPanelView62;
        private javax.swing.JScrollPane jScrollPaneCTHD36;
        private javax.swing.JScrollPane jScrollPaneTTHD36;
        private javax.swing.JTable jTableCTHoaDon36;
        private javax.swing.JTextField jTextFieldDonGia36;
        private javax.swing.JTextField jTextFieldDonGiaFrom36;
        private javax.swing.JTextField jTextFieldDonGiaTo36;
        private javax.swing.JTextField jTextFieldGhiChu36;
        private javax.swing.JTextField jTextFieldMaHD36;
        private javax.swing.JTextField jTextFieldMaHDct36;
        private javax.swing.JTextField jTextFieldMaKH36;
        private javax.swing.JTextField jTextFieldMaNV36;
        private javax.swing.JTextField jTextFieldNgayFrom36;
        private javax.swing.JTextField jTextFieldNgayLap36;
        private javax.swing.JTextField jTextFieldNgayTo36;
        private javax.swing.JTextField jTextFieldSoLuong36;
        private javax.swing.JTextField jTextFieldTenSPct36;
        private javax.swing.JTextField jTextFieldThanhTien36;
        private javax.swing.JTextField jTextFieldTongTien36;
        // End of variables declaration//GEN-END:variables
}
