package view.NhanVien;

import bus.NhanVienBUS;
import bus.PhanQuyenBUS;
import bus.TaiKhoanBUS;
import custom.MyDialog;
import custom.MyTable;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Vector;
import javax.swing.JLabel;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableRowSorter;
import model.NhanVien;
import model.PhanQuyen;
import org.apache.commons.text.WordUtils;

public class NhanVienJPanel extends javax.swing.JPanel {

        private PhanQuyenBUS phanQuyenBUS62 = new PhanQuyenBUS();
        private NhanVienBUS nhanVienBUS62 = new NhanVienBUS();
        private TaiKhoanBUS taiKhoanBUS62 = new TaiKhoanBUS();
        DefaultTableModel dtmNhanVien62;

        public NhanVienJPanel() {
                initComponents();

                DefaultTableCellRenderer centerRenderer62 = new DefaultTableCellRenderer();
                centerRenderer62.setHorizontalAlignment(JLabel.CENTER);

                jTextFieldMa50.setEditable(false);

                new MyTable(jTableAll50);
                dtmNhanVien62 = new DefaultTableModel() {
                        @Override
                        public boolean isCellEditable(int row, int column) {
                                return false;
                        }
                };
                jTableAll50.setModel(dtmNhanVien62);

                dtmNhanVien62.addColumn("Mã NV");
                dtmNhanVien62.addColumn("Họ đệm");
                dtmNhanVien62.addColumn("Tên");
                dtmNhanVien62.addColumn("Giới tính");
                dtmNhanVien62.addColumn("Chức vụ");
                dtmNhanVien62.addColumn("Tài khoản");

                jTableAll50.getColumnModel().getColumn(0).setCellRenderer(centerRenderer62);

                TableColumnModel columnModelSanPham62 = jTableAll50.getColumnModel();
                columnModelSanPham62.getColumn(0).setPreferredWidth(50);
                columnModelSanPham62.getColumn(1).setPreferredWidth(230);
                columnModelSanPham62.getColumn(2).setPreferredWidth(150);
                columnModelSanPham62.getColumn(3).setPreferredWidth(120);
                columnModelSanPham62.getColumn(4).setPreferredWidth(150);
                columnModelSanPham62.getColumn(5).setPreferredWidth(150);

                loadDataCmbGT();
                loadDataCmbQuyen();
                loadDataLenBangNhanVien();
        }

        @SuppressWarnings("unchecked")
        // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
        private void initComponents() {

                NhanVienViewjPanel50 = new javax.swing.JPanel();
                CapjButton50 = new javax.swing.JButton();
                QuyenjButton50 = new javax.swing.JButton();
                KhoajButton50 = new javax.swing.JButton();
                jLabelQLKH32 = new javax.swing.JLabel();
                jLabelRefresh32 = new javax.swing.JLabel();
                jLabelMaNV50 = new javax.swing.JLabel();
                jTextFieldMa50 = new javax.swing.JTextField();
                jLabelHoDem50 = new javax.swing.JLabel();
                jTextFieldHoDem50 = new javax.swing.JTextField();
                jLabelGT50 = new javax.swing.JLabel();
                jComboBoxGT50 = new javax.swing.JComboBox<>();
                jLabelSearch50 = new javax.swing.JLabel();
                jTextFieldSearch62 = new javax.swing.JTextField();
                jButtonThem50 = new javax.swing.JButton();
                jButtonLuu50 = new javax.swing.JButton();
                jButtonXoa50 = new javax.swing.JButton();
                jScrollPane32 = new javax.swing.JScrollPane();
                jTableAll50 = new javax.swing.JTable();
                jLabelTen50 = new javax.swing.JLabel();
                jTextFieldTen50 = new javax.swing.JTextField();
                jLabelLoai50 = new javax.swing.JLabel();
                jComboBoxLoai50 = new javax.swing.JComboBox<>();
                jButtonThem62 = new javax.swing.JButton();

                setMinimumSize(new java.awt.Dimension(1110, 630));
                setPreferredSize(new java.awt.Dimension(1200, 720));

                NhanVienViewjPanel50.setBackground(new java.awt.Color(245, 219, 137));
                NhanVienViewjPanel50.setMinimumSize(new java.awt.Dimension(1110, 630));
                NhanVienViewjPanel50.setPreferredSize(new java.awt.Dimension(1200, 720));
                NhanVienViewjPanel50.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

                CapjButton50.setBackground(new java.awt.Color(255, 153, 0));
                CapjButton50.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                CapjButton50.setForeground(new java.awt.Color(255, 255, 255));
                CapjButton50.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/baseline_account_circle_white_24dp.png"))); // NOI18N
                CapjButton50.setText("Cấp tài khoản");
                CapjButton50.setBorder(null);
                CapjButton50.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
                CapjButton50.setFocusPainted(false);
                CapjButton50.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                CapjButton50ActionPerformed(evt);
                        }
                });
                NhanVienViewjPanel50.add(CapjButton50, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 260, 155, 35));

                QuyenjButton50.setBackground(new java.awt.Color(255, 153, 0));
                QuyenjButton50.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                QuyenjButton50.setForeground(new java.awt.Color(255, 255, 255));
                QuyenjButton50.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/baseline_admin_panel_settings_white_24dp.png"))); // NOI18N
                QuyenjButton50.setText("Mật khẩu/Quyền");
                QuyenjButton50.setBorder(null);
                QuyenjButton50.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
                QuyenjButton50.setFocusPainted(false);
                QuyenjButton50.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                QuyenjButton50ActionPerformed(evt);
                        }
                });
                NhanVienViewjPanel50.add(QuyenjButton50, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 260, 155, 35));

                KhoajButton50.setBackground(new java.awt.Color(255, 153, 0));
                KhoajButton50.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                KhoajButton50.setForeground(new java.awt.Color(255, 255, 255));
                KhoajButton50.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/baseline_lock_white_24dp.png"))); // NOI18N
                KhoajButton50.setText("Khóa tài khoản");
                KhoajButton50.setBorder(null);
                KhoajButton50.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
                KhoajButton50.setFocusPainted(false);
                KhoajButton50.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                KhoajButton50ActionPerformed(evt);
                        }
                });
                NhanVienViewjPanel50.add(KhoajButton50, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 260, 155, 35));

                jLabelQLKH32.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
                jLabelQLKH32.setForeground(new java.awt.Color(102, 51, 0));
                jLabelQLKH32.setText("QUẢN LÝ NHÂN VIÊN");
                NhanVienViewjPanel50.add(jLabelQLKH32, new org.netbeans.lib.awtextra.AbsoluteConstraints(455, 15, -1, -1));

                jLabelRefresh32.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/mdi_database-refresh.png"))); // NOI18N
                jLabelRefresh32.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
                jLabelRefresh32.addMouseListener(new java.awt.event.MouseAdapter() {
                        public void mouseClicked(java.awt.event.MouseEvent evt) {
                                jLabelRefresh32MouseClicked(evt);
                        }
                });
                NhanVienViewjPanel50.add(jLabelRefresh32, new org.netbeans.lib.awtextra.AbsoluteConstraints(675, 10, -1, -1));

                jLabelMaNV50.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jLabelMaNV50.setForeground(new java.awt.Color(0, 0, 0));
                jLabelMaNV50.setText("Mã NV");
                NhanVienViewjPanel50.add(jLabelMaNV50, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 70, 50, -1));

                jTextFieldMa50.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
                jTextFieldMa50.setForeground(new java.awt.Color(0, 0, 0));
                NhanVienViewjPanel50.add(jTextFieldMa50, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 60, 390, 32));

                jLabelHoDem50.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jLabelHoDem50.setForeground(new java.awt.Color(0, 0, 0));
                jLabelHoDem50.setText("Họ đệm");
                NhanVienViewjPanel50.add(jLabelHoDem50, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 110, 70, -1));

                jTextFieldHoDem50.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
                jTextFieldHoDem50.setForeground(new java.awt.Color(0, 0, 0));
                NhanVienViewjPanel50.add(jTextFieldHoDem50, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 100, 390, 32));

                jLabelGT50.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jLabelGT50.setForeground(new java.awt.Color(0, 0, 0));
                jLabelGT50.setText("Giới tính");
                NhanVienViewjPanel50.add(jLabelGT50, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 190, -1, -1));

                jComboBoxGT50.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
                jComboBoxGT50.setForeground(new java.awt.Color(0, 0, 0));
                jComboBoxGT50.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
                NhanVienViewjPanel50.add(jComboBoxGT50, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 180, 390, 32));

                jLabelSearch50.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/bx_search-alt.png"))); // NOI18N
                jLabelSearch50.setToolTipText("");
                NhanVienViewjPanel50.add(jLabelSearch50, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 305, 30, 30));

                jTextFieldSearch62.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
                jTextFieldSearch62.setPreferredSize(new java.awt.Dimension(130, 30));
                jTextFieldSearch62.addKeyListener(new java.awt.event.KeyAdapter() {
                        public void keyReleased(java.awt.event.KeyEvent evt) {
                                jTextFieldSearch62KeyReleased(evt);
                        }
                });
                NhanVienViewjPanel50.add(jTextFieldSearch62, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 305, 160, 30));

                jButtonThem50.setBackground(new java.awt.Color(255, 153, 0));
                jButtonThem50.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jButtonThem50.setForeground(new java.awt.Color(255, 255, 255));
                jButtonThem50.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Add.png"))); // NOI18N
                jButtonThem50.setText("Thêm");
                jButtonThem50.setBorder(null);
                jButtonThem50.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
                jButtonThem50.setFocusPainted(false);
                jButtonThem50.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                jButtonThem50ActionPerformed(evt);
                        }
                });
                NhanVienViewjPanel50.add(jButtonThem50, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 305, 90, 30));

                jButtonLuu50.setBackground(new java.awt.Color(255, 153, 0));
                jButtonLuu50.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jButtonLuu50.setForeground(new java.awt.Color(255, 255, 255));
                jButtonLuu50.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Save.png"))); // NOI18N
                jButtonLuu50.setText("Lưu");
                jButtonLuu50.setBorder(null);
                jButtonLuu50.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
                jButtonLuu50.setFocusPainted(false);
                jButtonLuu50.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                jButtonLuu50ActionPerformed(evt);
                        }
                });
                NhanVienViewjPanel50.add(jButtonLuu50, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 305, 90, 30));

                jButtonXoa50.setBackground(new java.awt.Color(255, 153, 0));
                jButtonXoa50.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jButtonXoa50.setForeground(new java.awt.Color(255, 255, 255));
                jButtonXoa50.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Delete.png"))); // NOI18N
                jButtonXoa50.setText("Xóa");
                jButtonXoa50.setBorder(null);
                jButtonXoa50.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
                jButtonXoa50.setFocusPainted(false);
                jButtonXoa50.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                jButtonXoa50ActionPerformed(evt);
                        }
                });
                NhanVienViewjPanel50.add(jButtonXoa50, new org.netbeans.lib.awtextra.AbsoluteConstraints(710, 305, 90, 30));

                jScrollPane32.setPreferredSize(new java.awt.Dimension(1200, 320));

                jTableAll50.setFont(new java.awt.Font("Tahoma", 0, 13)); // NOI18N
                jTableAll50.setModel(new javax.swing.table.DefaultTableModel(
                        new Object [][] {
                                {},
                                {},
                                {},
                                {}
                        },
                        new String [] {

                        }
                ));
                jTableAll50.setGridColor(new java.awt.Color(128, 128, 128));
                jTableAll50.setPreferredSize(new java.awt.Dimension(1200, 0));
                jTableAll50.setRowHeight(25);
                jTableAll50.setSelectionBackground(new java.awt.Color(50, 154, 114));
                jTableAll50.setSelectionForeground(new java.awt.Color(255, 255, 255));
                jTableAll50.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
                jTableAll50.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
                jTableAll50.setShowHorizontalLines(true);
                jTableAll50.addMouseListener(new java.awt.event.MouseAdapter() {
                        public void mouseClicked(java.awt.event.MouseEvent evt) {
                                jTableAll50MouseClicked(evt);
                        }
                });
                jScrollPane32.setViewportView(jTableAll50);

                NhanVienViewjPanel50.add(jScrollPane32, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 346, 1200, 374));

                jLabelTen50.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jLabelTen50.setForeground(new java.awt.Color(0, 0, 0));
                jLabelTen50.setText("Tên");
                NhanVienViewjPanel50.add(jLabelTen50, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 150, 70, -1));

                jTextFieldTen50.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
                jTextFieldTen50.setForeground(new java.awt.Color(0, 0, 0));
                NhanVienViewjPanel50.add(jTextFieldTen50, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 140, 390, 32));

                jLabelLoai50.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jLabelLoai50.setForeground(new java.awt.Color(0, 0, 0));
                jLabelLoai50.setText("Chức vụ");
                NhanVienViewjPanel50.add(jLabelLoai50, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 230, -1, -1));

                jComboBoxLoai50.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
                jComboBoxLoai50.setForeground(new java.awt.Color(0, 0, 0));
                jComboBoxLoai50.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
                NhanVienViewjPanel50.add(jComboBoxLoai50, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 220, 390, 32));

                jButtonThem62.setBackground(new java.awt.Color(255, 153, 0));
                jButtonThem62.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
                jButtonThem62.setForeground(new java.awt.Color(255, 255, 255));
                jButtonThem62.setBorder(null);
                jButtonThem62.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
                jButtonThem62.setFocusPainted(false);
                jButtonThem62.setLabel("+");
                jButtonThem62.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                jButtonThem62ActionPerformed(evt);
                        }
                });
                NhanVienViewjPanel50.add(jButtonThem62, new org.netbeans.lib.awtextra.AbsoluteConstraints(810, 221, 30, 30));

                javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
                this.setLayout(layout);
                layout.setHorizontalGroup(
                        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(NhanVienViewjPanel50, javax.swing.GroupLayout.PREFERRED_SIZE, 1200, javax.swing.GroupLayout.PREFERRED_SIZE)
                );
                layout.setVerticalGroup(
                        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(NhanVienViewjPanel50, javax.swing.GroupLayout.PREFERRED_SIZE, 720, javax.swing.GroupLayout.PREFERRED_SIZE)
                );
        }// </editor-fold>//GEN-END:initComponents

        private void loadDataCmbGT() {
                jComboBoxGT50.removeAllItems();
                jComboBoxGT50.addItem("Chọn giới tính");
                jComboBoxGT50.addItem("Nam");
                jComboBoxGT50.addItem("Nữ");
        }

        private void loadDataCmbQuyen() {
                phanQuyenBUS62.docDanhSachQuyen();
                ArrayList<PhanQuyen> dsq62 = phanQuyenBUS62.getListQuyen();
                jComboBoxLoai50.removeAllItems();
                jComboBoxLoai50.addItem("Chọn quyền");
                for (PhanQuyen pq62 : dsq62) {
                        jComboBoxLoai50.addItem(pq62.getQuyen());
                }
        }

        private void loadDataLenBangNhanVien() {
                dtmNhanVien62.setRowCount(0);
                ArrayList<NhanVien> dsnv62 = nhanVienBUS62.getDanhSachNhanVien();

                for (NhanVien nv62 : dsnv62) {
                        Vector vec62 = new Vector();
                        vec62.add(nv62.getMaNV());
                        vec62.add(nv62.getHo());
                        vec62.add(nv62.getTen());
                        vec62.add(nv62.getGioiTinh());
                        vec62.add(nv62.getChucVu());
                        int trangThai = taiKhoanBUS62.getTrangThai(nv62.getMaNV() + "");
                        if (trangThai == 0) {
                                vec62.add("Khoá");
                        } else if (trangThai == 1) {
                                vec62.add("Hiệu lực");
                        } else {
                                vec62.add("Chưa có");
                        }
                        dtmNhanVien62.addRow(vec62);
                }

                jTableAll50.setPreferredSize(new Dimension(jTableAll50.getWidth(), jTableAll50.getRowHeight() * jTableAll50.getRowCount()));
        }

        private void jTextFieldSearch62KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldSearch62KeyReleased
                // TODO add your handling code here:
                searchTable(jTextFieldSearch62.getText());
        }//GEN-LAST:event_jTextFieldSearch62KeyReleased

        public void searchTable(String value) {
                TableRowSorter<DefaultTableModel> trs = new TableRowSorter<>(dtmNhanVien62);
                jTableAll50.setRowSorter(trs);
                trs.setRowFilter(RowFilter.regexFilter(value));
                jTableAll50.setPreferredSize(new Dimension(jTableAll50.getWidth(), jTableAll50.getRowHeight() * jTableAll50.getRowCount()));
        }

        private void jButtonThem50ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonThem50ActionPerformed
                // TODO add your handling code here:
                xuLyThemNhanVien();
        }//GEN-LAST:event_jButtonThem50ActionPerformed

        private void xuLyThemNhanVien() {
                if (jComboBoxGT50.getSelectedIndex() == 0) {
                        new MyDialog("Hãy chọn giới tính!", MyDialog.ERROR_DIALOG);
                        return;
                }
                if (jComboBoxLoai50.getSelectedIndex() == 0) {
                        new MyDialog("Hãy chọn chức vụ!", MyDialog.ERROR_DIALOG);
                        return;
                }
                String ho62 = jTextFieldHoDem50.getText();
                String ten62 = jTextFieldTen50.getText();
                String gioiTinh62 = jComboBoxGT50.getSelectedItem() + "";
                String chucVu62 = jComboBoxLoai50.getSelectedItem() + "";
                if (nhanVienBUS62.themNhanVien(ho62, ten62, gioiTinh62, capitalizeFirstLetter(chucVu62))) {
                        nhanVienBUS62.docDanhSach();
                        xuLyRefresh();
                }
        }

        private static String capitalizeFirstLetter(String str) {
                return WordUtils.capitalizeFully(str);
        }

        private void jButtonLuu50ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonLuu50ActionPerformed
                // TODO add your handling code here:
                xuLySuaNhanVien();
        }//GEN-LAST:event_jButtonLuu50ActionPerformed

        private void xuLySuaNhanVien() {
                if (jComboBoxGT50.getSelectedIndex() == 0) {
                        new MyDialog("Hãy chọn giới tính!", MyDialog.ERROR_DIALOG);
                        return;
                }
                String ma62 = jTextFieldMa50.getText();
                String ho62 = jTextFieldHoDem50.getText();
                String ten62 = jTextFieldTen50.getText();
                String gioiTinh62 = jComboBoxGT50.getSelectedItem() + "";
                String chucVu62 = jComboBoxLoai50.getSelectedItem() + "";
                if (nhanVienBUS62.updateNhanVien(ma62, ho62, ten62, gioiTinh62, chucVu62)) {
                        nhanVienBUS62.docDanhSach();
                        xuLyRefresh();
                }
        }

        private void jButtonXoa50ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonXoa50ActionPerformed
                // TODO add your handling code here:
                xuLyXoaNhanVien();
        }//GEN-LAST:event_jButtonXoa50ActionPerformed

        private void xuLyXoaNhanVien() {
                String ma62 = jTextFieldMa50.getText();
                boolean flag62 = nhanVienBUS62.xoaNhanVien(ma62);
                if (flag62) {
                        nhanVienBUS62.docDanhSach();
                        xuLyRefresh();
                }
        }

        private void jTableAll50MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableAll50MouseClicked
                // TODO add your handling code here:
                xuLyClickTblNhanVien();
        }//GEN-LAST:event_jTableAll50MouseClicked

        private void xuLyClickTblNhanVien() {
                int row62 = jTableAll50.getSelectedRow();
                if (row62 > -1) {
                        jTextFieldMa50.setText(jTableAll50.getValueAt(row62, 0) + "");
                        jTextFieldHoDem50.setText(jTableAll50.getValueAt(row62, 1) + "");
                        jTextFieldTen50.setText(jTableAll50.getValueAt(row62, 2) + "");

                        if ((jTableAll50.getValueAt(row62, 3) + "").equals("Nam")) {
                                jComboBoxGT50.setSelectedIndex(1);
                        } else {
                                jComboBoxGT50.setSelectedIndex(2);
                        }

                        String chucvu62 = jTableAll50.getValueAt(row62, 4) + "";
                        int flag62 = 0;
                        for (int i = 0; i < jComboBoxLoai50.getItemCount(); i++) {
                                if (jComboBoxLoai50.getItemAt(i).toLowerCase().contains(chucvu62.toLowerCase())) {
                                        flag62 = i;
                                        break;
                                }
                        }
                        jComboBoxLoai50.setSelectedIndex(flag62);
                }
        }

        private void jButtonThem62ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonThem62ActionPerformed
                // TODO add your handling code here:
                xuLyThemQuyen();
        }//GEN-LAST:event_jButtonThem62ActionPerformed

        private void xuLyThemQuyen() {
                QuanLyQuyenJDialog quyenGUI62 = new QuanLyQuyenJDialog();
                quyenGUI62.setVisible(true);
                phanQuyenBUS62.docDanhSachQuyen();
                loadDataCmbQuyen();
        }

        private void jLabelRefresh32MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabelRefresh32MouseClicked
                // TODO add your handling code here:
                xuLyRefresh();
        }//GEN-LAST:event_jLabelRefresh32MouseClicked

        private void CapjButton50ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CapjButton50ActionPerformed
                // TODO add your handling code here:
                xuLyCapTaiKhoan();
        }//GEN-LAST:event_CapjButton50ActionPerformed

        private void xuLyCapTaiKhoan() {
                if (jTextFieldMa50.getText().trim().equals("")) {
                        new MyDialog("Hãy chọn nhân viên!", MyDialog.ERROR_DIALOG);
                        return;
                }
                CapTaiKhoanJDialog dialog62 = new CapTaiKhoanJDialog(jTextFieldMa50.getText());
                dialog62.setVisible(true);
                loadDataLenBangNhanVien();
        }

        private void QuyenjButton50ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_QuyenjButton50ActionPerformed
                // TODO add your handling code here:
                xuLyResetMatKhau();
        }//GEN-LAST:event_QuyenjButton50ActionPerformed

        private void xuLyResetMatKhau() {
                String maNV62 = jTextFieldMa50.getText();
                if (maNV62.trim().equals("")) {
                        new MyDialog("Hãy chọn nhân viên!", MyDialog.ERROR_DIALOG);
                        return;
                }
                Quyen_MatKhauJDialog dialog62 = new Quyen_MatKhauJDialog(maNV62);
                dialog62.setVisible(true);
        }

        private void KhoajButton50ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_KhoajButton50ActionPerformed
                // TODO add your handling code here:
                xuLyKhoaTaiKhoan();
        }//GEN-LAST:event_KhoajButton50ActionPerformed

        private void xuLyKhoaTaiKhoan() {
                TaiKhoanBUS taiKhoanBUS = new TaiKhoanBUS();
                taiKhoanBUS.khoaTaiKhoan(jTextFieldMa50.getText());
                loadDataLenBangNhanVien();
        }

        private void xuLyRefresh() {
                jTextFieldMa50.setText("");
                jTextFieldHoDem50.setText("");
                jTextFieldTen50.setText("");
                jTextFieldSearch62.setText("");
                searchTable(jTextFieldSearch62.getText());
                jComboBoxGT50.setSelectedIndex(0);
                jComboBoxLoai50.setSelectedIndex(0);
                loadDataLenBangNhanVien();
        }

        // Variables declaration - do not modify//GEN-BEGIN:variables
        private javax.swing.JButton CapjButton50;
        private javax.swing.JButton KhoajButton50;
        private javax.swing.JPanel NhanVienViewjPanel50;
        private javax.swing.JButton QuyenjButton50;
        private javax.swing.JButton jButtonLuu50;
        private javax.swing.JButton jButtonThem50;
        private javax.swing.JButton jButtonThem62;
        private javax.swing.JButton jButtonXoa50;
        private javax.swing.JComboBox<String> jComboBoxGT50;
        private javax.swing.JComboBox<String> jComboBoxLoai50;
        private javax.swing.JLabel jLabelGT50;
        private javax.swing.JLabel jLabelHoDem50;
        private javax.swing.JLabel jLabelLoai50;
        private javax.swing.JLabel jLabelMaNV50;
        private javax.swing.JLabel jLabelQLKH32;
        private javax.swing.JLabel jLabelRefresh32;
        private javax.swing.JLabel jLabelSearch50;
        private javax.swing.JLabel jLabelTen50;
        private javax.swing.JScrollPane jScrollPane32;
        private javax.swing.JTable jTableAll50;
        private javax.swing.JTextField jTextFieldHoDem50;
        private javax.swing.JTextField jTextFieldMa50;
        private javax.swing.JTextField jTextFieldSearch62;
        private javax.swing.JTextField jTextFieldTen50;
        // End of variables declaration//GEN-END:variables
}
