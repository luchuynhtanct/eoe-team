package view.NhanVien;

import bus.PhanQuyenBUS;
import custom.MyDialog;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import model.PhanQuyen;

public class QuanLyQuyenJDialog extends javax.swing.JDialog {

        private PhanQuyenBUS phanQuyenBUS62 = new PhanQuyenBUS();

        public QuanLyQuyenJDialog() {
                initComponents();

                loadDataCmbQuyen();
                this.setLocationRelativeTo(null);
        }

        private void loadDataCmbQuyen() {
                phanQuyenBUS62.docDanhSachQuyen();
                ArrayList<PhanQuyen> dsq62 = phanQuyenBUS62.getListQuyen();
                jComboBoxLoai50.removeAllItems();
                jComboBoxLoai50.addItem("Chọn quyền");
                for (PhanQuyen pq62 : dsq62) {
                        jComboBoxLoai50.addItem(pq62.getQuyen());
                }
        }

        private void xuLyXoaQuyen() {
                if (jComboBoxLoai50.getSelectedIndex() < 1) {
                        new MyDialog("Chưa chọn nhóm quyền để xoá!", MyDialog.ERROR_DIALOG);
                        return;
                }
                MyDialog dlg62 = new MyDialog("Bạn có chắc chắn muốn xoá?", MyDialog.WARNING_DIALOG);
                if (dlg62.getAction() == MyDialog.CANCEL_OPTION) {
                        return;
                }
                String tenQuyen62 = jComboBoxLoai50.getSelectedItem() + "";
                boolean flag62 = phanQuyenBUS62.xoaQuyen(tenQuyen62);
                if (flag62) {
                        loadDataCmbQuyen();
                }
        }

        private void xuLyThemQuyen() {
                String tenQuyen62 = JOptionPane.showInputDialog("Nhập tên quyền");

                boolean flag62 = phanQuyenBUS62.themQuyen(tenQuyen62);
                if (flag62) {
                        loadDataCmbQuyen();
                }
        }

        private void xuLySuaQuyen() {
                if (jComboBoxLoai50.getSelectedIndex() < 1) {
                        new MyDialog("Chưa chọn nhóm quyền để sửa!", MyDialog.ERROR_DIALOG);
                        return;
                }
                String tenQuyen62 = jComboBoxLoai50.getSelectedItem() + "";
                int sanPham62 = jCheckBoxQLSP50.isSelected() ? 1 : 0;
                int nhanVien62 = jCheckBoxQLNV50.isSelected() ? 1 : 0;
                int khachHang62 = jCheckBoxQLKH50.isSelected() ? 1 : 0;
                int thongKe62 = jCheckBoxQLTK50.isSelected() ? 1 : 0;

                boolean flag62 = phanQuyenBUS62.suaQuyen(tenQuyen62, sanPham62, nhanVien62, khachHang62, thongKe62);
                if (flag62) {
                        loadDataCmbQuyen();
                }
        }

        private void xuLyHienThiChiTietQuyen() {
                ArrayList<PhanQuyen> dsq62 = phanQuyenBUS62.getListQuyen();
                PhanQuyen phanQuyen62 = new PhanQuyen();
                for (PhanQuyen pq62 : dsq62) {
                        if (pq62.getQuyen().equals(jComboBoxLoai50.getSelectedItem())) {
                                phanQuyen62.setQuyen(pq62.getQuyen());
                                phanQuyen62.setQlSanPham(pq62.getQlSanPham());
                                phanQuyen62.setQlNhanVien(pq62.getQlNhanVien());
                                phanQuyen62.setQlKhachHang(pq62.getQlKhachHang());
                                phanQuyen62.setThongKe(pq62.getThongKe());
                                break;
                        }
                }
                jCheckBoxQLSP50.setSelected(false);
                jCheckBoxQLNV50.setSelected(false);
                jCheckBoxQLKH50.setSelected(false);
                jCheckBoxQLTK50.setSelected(false);
                if (phanQuyen62.getQlSanPham() == 1) {
                        jCheckBoxQLSP50.setSelected(true);
                }
                if (phanQuyen62.getQlNhanVien() == 1) {
                        jCheckBoxQLNV50.setSelected(true);
                }
                if (phanQuyen62.getQlKhachHang() == 1) {
                        jCheckBoxQLKH50.setSelected(true);
                }
                if (phanQuyen62.getThongKe() == 1) {
                        jCheckBoxQLTK50.setSelected(true);
                }
        }

        @SuppressWarnings("unchecked")
        // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
        private void initComponents() {

                jPanelTT50 = new javax.swing.JPanel();
                jLabelQLLoai50 = new javax.swing.JLabel();
                pnTable50 = new javax.swing.JPanel();
                jLabelNhomQuyen50 = new javax.swing.JLabel();
                jComboBoxLoai50 = new javax.swing.JComboBox<>();
                jCheckBoxQLSP50 = new javax.swing.JCheckBox();
                jCheckBoxQLNV50 = new javax.swing.JCheckBox();
                jCheckBoxQLKH50 = new javax.swing.JCheckBox();
                jCheckBoxQLTK50 = new javax.swing.JCheckBox();
                jPanelForm50 = new javax.swing.JPanel();
                pnButton50 = new javax.swing.JPanel();
                btnThem50 = new javax.swing.JButton();
                btnSua50 = new javax.swing.JButton();
                btnXoa50 = new javax.swing.JButton();

                setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
                setLocation(new java.awt.Point(0, 0));
                setModal(true);

                jLabelQLLoai50.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
                jLabelQLLoai50.setForeground(new java.awt.Color(255, 153, 0));
                jLabelQLLoai50.setText("QUẢN LÝ PHÂN QUYỀN");
                jPanelTT50.add(jLabelQLLoai50);

                jLabelNhomQuyen50.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                jLabelNhomQuyen50.setForeground(new java.awt.Color(0, 0, 0));
                jLabelNhomQuyen50.setText("Nhóm quyền");

                jComboBoxLoai50.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
                jComboBoxLoai50.setForeground(new java.awt.Color(0, 0, 0));
                jComboBoxLoai50.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
                jComboBoxLoai50.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                jComboBoxLoai50ActionPerformed(evt);
                        }
                });

                jCheckBoxQLSP50.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
                jCheckBoxQLSP50.setForeground(new java.awt.Color(0, 0, 0));
                jCheckBoxQLSP50.setText("Quản lý sản phẩm");

                jCheckBoxQLNV50.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
                jCheckBoxQLNV50.setForeground(new java.awt.Color(0, 0, 0));
                jCheckBoxQLNV50.setText("Quản lý nhân viên");

                jCheckBoxQLKH50.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
                jCheckBoxQLKH50.setForeground(new java.awt.Color(0, 0, 0));
                jCheckBoxQLKH50.setText("Quản lý khách hàng");

                jCheckBoxQLTK50.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
                jCheckBoxQLTK50.setForeground(new java.awt.Color(0, 0, 0));
                jCheckBoxQLTK50.setText("Quản lý thống kê");

                javax.swing.GroupLayout pnTable50Layout = new javax.swing.GroupLayout(pnTable50);
                pnTable50.setLayout(pnTable50Layout);
                pnTable50Layout.setHorizontalGroup(
                        pnTable50Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(pnTable50Layout.createSequentialGroup()
                                .addGap(98, 98, 98)
                                .addComponent(jLabelNhomQuyen50)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jComboBoxLoai50, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnTable50Layout.createSequentialGroup()
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(pnTable50Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jCheckBoxQLSP50)
                                        .addComponent(jCheckBoxQLNV50)
                                        .addComponent(jCheckBoxQLKH50)
                                        .addComponent(jCheckBoxQLTK50))
                                .addGap(141, 141, 141))
                );
                pnTable50Layout.setVerticalGroup(
                        pnTable50Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(pnTable50Layout.createSequentialGroup()
                                .addGap(19, 19, 19)
                                .addGroup(pnTable50Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabelNhomQuyen50)
                                        .addComponent(jComboBoxLoai50, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addComponent(jCheckBoxQLSP50)
                                .addGap(18, 18, 18)
                                .addComponent(jCheckBoxQLNV50)
                                .addGap(18, 18, 18)
                                .addComponent(jCheckBoxQLKH50)
                                .addGap(18, 18, 18)
                                .addComponent(jCheckBoxQLTK50)
                                .addContainerGap(26, Short.MAX_VALUE))
                );

                jPanelForm50.setLayout(new javax.swing.BoxLayout(jPanelForm50, javax.swing.BoxLayout.Y_AXIS));

                btnThem50.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
                btnThem50.setText("Thêm");
                btnThem50.setPreferredSize(new java.awt.Dimension(80, 35));
                btnThem50.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                btnThem50ActionPerformed(evt);
                        }
                });
                pnButton50.add(btnThem50);

                btnSua50.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
                btnSua50.setText("Sửa");
                btnSua50.setPreferredSize(new java.awt.Dimension(80, 35));
                btnSua50.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                btnSua50ActionPerformed(evt);
                        }
                });
                pnButton50.add(btnSua50);

                btnXoa50.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
                btnXoa50.setText("Xoá");
                btnXoa50.setPreferredSize(new java.awt.Dimension(80, 35));
                btnXoa50.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                btnXoa50ActionPerformed(evt);
                        }
                });
                pnButton50.add(btnXoa50);

                javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
                getContentPane().setLayout(layout);
                layout.setHorizontalGroup(
                        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jPanelTT50, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanelForm50, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(pnTable50, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(pnButton50, javax.swing.GroupLayout.PREFERRED_SIZE, 446, javax.swing.GroupLayout.PREFERRED_SIZE))
                );
                layout.setVerticalGroup(
                        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addComponent(jPanelTT50, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(pnTable50, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jPanelForm50, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(pnButton50, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap())
                );

                pack();
        }// </editor-fold>//GEN-END:initComponents

    private void btnThem50ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnThem50ActionPerformed
            // TODO add your handling code here:
            xuLyThemQuyen();
    }//GEN-LAST:event_btnThem50ActionPerformed

    private void btnXoa50ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnXoa50ActionPerformed
            xuLyXoaQuyen();
    }//GEN-LAST:event_btnXoa50ActionPerformed

    private void btnSua50ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSua50ActionPerformed
            xuLySuaQuyen();
    }//GEN-LAST:event_btnSua50ActionPerformed

        private void jComboBoxLoai50ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxLoai50ActionPerformed
                // TODO add your handling code here:
                xuLyHienThiChiTietQuyen();
        }//GEN-LAST:event_jComboBoxLoai50ActionPerformed

        // Variables declaration - do not modify//GEN-BEGIN:variables
        private javax.swing.JButton btnSua50;
        private javax.swing.JButton btnThem50;
        private javax.swing.JButton btnXoa50;
        private javax.swing.JCheckBox jCheckBoxQLKH50;
        private javax.swing.JCheckBox jCheckBoxQLNV50;
        private javax.swing.JCheckBox jCheckBoxQLSP50;
        private javax.swing.JCheckBox jCheckBoxQLTK50;
        private javax.swing.JComboBox<String> jComboBoxLoai50;
        private javax.swing.JLabel jLabelNhomQuyen50;
        private javax.swing.JLabel jLabelQLLoai50;
        private javax.swing.JPanel jPanelForm50;
        private javax.swing.JPanel jPanelTT50;
        private javax.swing.JPanel pnButton50;
        private javax.swing.JPanel pnTable50;
        // End of variables declaration//GEN-END:variables
}
