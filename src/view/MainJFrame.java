package view;

import bean.DanhMucBean;
import bus.PhanQuyenBUS;
import controller.ChuyenManHinhController;
import custom.MyDialog;
import java.awt.Image;
import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.List;
import model.PhanQuyen;

public class MainJFrame extends javax.swing.JFrame {

        public MainJFrame() {
                initComponents();

                setTitle("QUẢN LÝ QUÁN ĂN");

                ChuyenManHinhController controller62 = new ChuyenManHinhController(jPanelView62);
                controller62.setView(jPanelBanHang62, jLabelBanHang62);

                List<DanhMucBean> listItem62 = new ArrayList<>();
                listItem62.add(new DanhMucBean("BanHang", jPanelBanHang62, jLabelBanHang62));
                PhanQuyen quyen = PhanQuyenBUS.quyen50TK50;
                jPanelSanPham62.setVisible(false);
                jPanelNhanVien62.setVisible(false);
                jPanelKhachHang62.setVisible(false);
                jPanelThongKe62.setVisible(false);
                if (quyen.getQlSanPham() == 1) {
                        listItem62.add(new DanhMucBean("SanPham", jPanelSanPham62, jLabelSanPham62));
                        jPanelSanPham62.setVisible(true);
                }

                if (quyen.getQlNhanVien() == 1) {
                        listItem62.add(new DanhMucBean("NhanVien", jPanelNhanVien62, jLabelNhanVien62));
                        jPanelNhanVien62.setVisible(true);
                }

                if (quyen.getQlKhachHang() == 1) {
                        listItem62.add(new DanhMucBean("KhachHang", jPanelKhachHang62, jLabelKhachHang62));
                        jPanelKhachHang62.setVisible(true);
                }

                if (quyen.getThongKe() == 1) {
                        listItem62.add(new DanhMucBean("ThongKeMenu", jPanelThongKe62, jLabelThongKe62));
                        jPanelThongKe62.setVisible(true);
                }

                controller62.setEvent(listItem62);
        }

        @SuppressWarnings("unchecked")
        // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
        private void initComponents() {
                java.awt.GridBagConstraints gridBagConstraints;

                jPanelRoot62 = new javax.swing.JPanel();
                jPanelMenu62 = new javax.swing.JPanel();
                jPanelLOGO62 = new javax.swing.JPanel();
                jLabelLogo62 = new javax.swing.JLabel();
                jPanelBanHang62 = new javax.swing.JPanel();
                jLabelBanHang62 = new javax.swing.JLabel();
                jPanelSanPham62 = new javax.swing.JPanel();
                jLabelSanPham62 = new javax.swing.JLabel();
                jPanelNhanVien62 = new javax.swing.JPanel();
                jLabelNhanVien62 = new javax.swing.JLabel();
                jPanelKhachHang62 = new javax.swing.JPanel();
                jLabelKhachHang62 = new javax.swing.JLabel();
                jPanelThongKe62 = new javax.swing.JPanel();
                jLabelThongKe62 = new javax.swing.JLabel();
                jPanelDangXuat62 = new javax.swing.JPanel();
                jLabelDangXuat62 = new javax.swing.JLabel();
                jPanelView62 = new javax.swing.JPanel();

                setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
                setResizable(false);
                setSize(new java.awt.Dimension(1440, 720));

                jPanelRoot62.setBackground(new java.awt.Color(50, 53, 55));
                jPanelRoot62.setPreferredSize(new java.awt.Dimension(1440, 720));
                jPanelRoot62.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

                jPanelMenu62.setBackground(new java.awt.Color(255, 153, 0));
                jPanelMenu62.setPreferredSize(new java.awt.Dimension(193, 960));
                jPanelMenu62.setLayout(new java.awt.GridBagLayout());

                jPanelLOGO62.setBackground(new java.awt.Color(255, 153, 0));
                jPanelLOGO62.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

                jLabelLogo62.setFont(new java.awt.Font("Arial Rounded MT Bold", 1, 28)); // NOI18N
                jLabelLogo62.setForeground(new java.awt.Color(255, 255, 255));
                jLabelLogo62.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
                jLabelLogo62.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/heroLogo-small.png"))); // NOI18N
                jPanelLOGO62.add(jLabelLogo62, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 240, 120));

                gridBagConstraints = new java.awt.GridBagConstraints();
                gridBagConstraints.gridx = 0;
                gridBagConstraints.gridy = 0;
                gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
                gridBagConstraints.insets = new java.awt.Insets(10, 0, 0, 0);
                jPanelMenu62.add(jPanelLOGO62, gridBagConstraints);

                jPanelBanHang62.setBackground(new java.awt.Color(255, 153, 0));
                jPanelBanHang62.setPreferredSize(new java.awt.Dimension(240, 80));

                jLabelBanHang62.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
                jLabelBanHang62.setForeground(new java.awt.Color(255, 255, 255));
                jLabelBanHang62.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
                jLabelBanHang62.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Shopping-cart.png"))); // NOI18N
                jLabelBanHang62.setText("Bán Hàng");
                jLabelBanHang62.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

                javax.swing.GroupLayout jPanelBanHang62Layout = new javax.swing.GroupLayout(jPanelBanHang62);
                jPanelBanHang62.setLayout(jPanelBanHang62Layout);
                jPanelBanHang62Layout.setHorizontalGroup(
                        jPanelBanHang62Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabelBanHang62, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                );
                jPanelBanHang62Layout.setVerticalGroup(
                        jPanelBanHang62Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelBanHang62Layout.createSequentialGroup()
                                .addComponent(jLabelBanHang62, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                );

                gridBagConstraints = new java.awt.GridBagConstraints();
                gridBagConstraints.gridx = 0;
                gridBagConstraints.gridy = 1;
                gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
                gridBagConstraints.insets = new java.awt.Insets(10, 0, 0, 0);
                jPanelMenu62.add(jPanelBanHang62, gridBagConstraints);

                jPanelSanPham62.setBackground(new java.awt.Color(255, 153, 0));
                jPanelSanPham62.setPreferredSize(new java.awt.Dimension(240, 80));

                jLabelSanPham62.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
                jLabelSanPham62.setForeground(new java.awt.Color(255, 255, 255));
                jLabelSanPham62.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
                jLabelSanPham62.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Palette.png"))); // NOI18N
                jLabelSanPham62.setText("Sản Phẩm");
                jLabelSanPham62.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

                javax.swing.GroupLayout jPanelSanPham62Layout = new javax.swing.GroupLayout(jPanelSanPham62);
                jPanelSanPham62.setLayout(jPanelSanPham62Layout);
                jPanelSanPham62Layout.setHorizontalGroup(
                        jPanelSanPham62Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabelSanPham62, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                );
                jPanelSanPham62Layout.setVerticalGroup(
                        jPanelSanPham62Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabelSanPham62, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                );

                gridBagConstraints = new java.awt.GridBagConstraints();
                gridBagConstraints.gridx = 0;
                gridBagConstraints.gridy = 2;
                gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
                jPanelMenu62.add(jPanelSanPham62, gridBagConstraints);

                jPanelNhanVien62.setBackground(new java.awt.Color(255, 153, 0));
                jPanelNhanVien62.setPreferredSize(new java.awt.Dimension(240, 80));

                jLabelNhanVien62.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
                jLabelNhanVien62.setForeground(new java.awt.Color(255, 255, 255));
                jLabelNhanVien62.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
                jLabelNhanVien62.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Contacts.png"))); // NOI18N
                jLabelNhanVien62.setText("Nhân Viên");
                jLabelNhanVien62.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

                javax.swing.GroupLayout jPanelNhanVien62Layout = new javax.swing.GroupLayout(jPanelNhanVien62);
                jPanelNhanVien62.setLayout(jPanelNhanVien62Layout);
                jPanelNhanVien62Layout.setHorizontalGroup(
                        jPanelNhanVien62Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabelNhanVien62, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                );
                jPanelNhanVien62Layout.setVerticalGroup(
                        jPanelNhanVien62Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabelNhanVien62, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                );

                gridBagConstraints = new java.awt.GridBagConstraints();
                gridBagConstraints.gridx = 0;
                gridBagConstraints.gridy = 3;
                gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
                jPanelMenu62.add(jPanelNhanVien62, gridBagConstraints);

                jPanelKhachHang62.setBackground(new java.awt.Color(255, 153, 0));
                jPanelKhachHang62.setPreferredSize(new java.awt.Dimension(240, 80));

                jLabelKhachHang62.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
                jLabelKhachHang62.setForeground(new java.awt.Color(255, 255, 255));
                jLabelKhachHang62.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
                jLabelKhachHang62.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/User.png"))); // NOI18N
                jLabelKhachHang62.setText("Khách Hàng");
                jLabelKhachHang62.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

                javax.swing.GroupLayout jPanelKhachHang62Layout = new javax.swing.GroupLayout(jPanelKhachHang62);
                jPanelKhachHang62.setLayout(jPanelKhachHang62Layout);
                jPanelKhachHang62Layout.setHorizontalGroup(
                        jPanelKhachHang62Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabelKhachHang62, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                );
                jPanelKhachHang62Layout.setVerticalGroup(
                        jPanelKhachHang62Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabelKhachHang62, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                );

                gridBagConstraints = new java.awt.GridBagConstraints();
                gridBagConstraints.gridx = 0;
                gridBagConstraints.gridy = 4;
                gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
                jPanelMenu62.add(jPanelKhachHang62, gridBagConstraints);

                jPanelThongKe62.setBackground(new java.awt.Color(255, 153, 0));
                jPanelThongKe62.setPreferredSize(new java.awt.Dimension(240, 80));

                jLabelThongKe62.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
                jLabelThongKe62.setForeground(new java.awt.Color(255, 255, 255));
                jLabelThongKe62.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
                jLabelThongKe62.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Chart-pie.png"))); // NOI18N
                jLabelThongKe62.setText("Thống Kê");
                jLabelThongKe62.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

                javax.swing.GroupLayout jPanelThongKe62Layout = new javax.swing.GroupLayout(jPanelThongKe62);
                jPanelThongKe62.setLayout(jPanelThongKe62Layout);
                jPanelThongKe62Layout.setHorizontalGroup(
                        jPanelThongKe62Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabelThongKe62, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                );
                jPanelThongKe62Layout.setVerticalGroup(
                        jPanelThongKe62Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabelThongKe62, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                );

                gridBagConstraints = new java.awt.GridBagConstraints();
                gridBagConstraints.gridx = 0;
                gridBagConstraints.gridy = 5;
                gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
                jPanelMenu62.add(jPanelThongKe62, gridBagConstraints);

                jPanelDangXuat62.setBackground(new java.awt.Color(255, 153, 0));
                jPanelDangXuat62.setPreferredSize(new java.awt.Dimension(240, 80));

                jLabelDangXuat62.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
                jLabelDangXuat62.setForeground(new java.awt.Color(219, 58, 58));
                jLabelDangXuat62.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
                jLabelDangXuat62.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Logout.png"))); // NOI18N
                jLabelDangXuat62.setText("Đăng Xuất");
                jLabelDangXuat62.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
                jLabelDangXuat62.addMouseListener(new java.awt.event.MouseAdapter() {
                        public void mouseClicked(java.awt.event.MouseEvent evt) {
                                jLabelDangXuat62MouseClicked(evt);
                        }
                });

                javax.swing.GroupLayout jPanelDangXuat62Layout = new javax.swing.GroupLayout(jPanelDangXuat62);
                jPanelDangXuat62.setLayout(jPanelDangXuat62Layout);
                jPanelDangXuat62Layout.setHorizontalGroup(
                        jPanelDangXuat62Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabelDangXuat62, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                );
                jPanelDangXuat62Layout.setVerticalGroup(
                        jPanelDangXuat62Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabelDangXuat62, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                );

                gridBagConstraints = new java.awt.GridBagConstraints();
                gridBagConstraints.gridx = 0;
                gridBagConstraints.gridy = 6;
                gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
                gridBagConstraints.insets = new java.awt.Insets(0, 0, 100, 0);
                jPanelMenu62.add(jPanelDangXuat62, gridBagConstraints);

                jPanelRoot62.add(jPanelMenu62, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 240, 720));

                jPanelView62.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
                jPanelRoot62.add(jPanelView62, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 0, 1200, 720));

                javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
                getContentPane().setLayout(layout);
                layout.setHorizontalGroup(
                        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jPanelRoot62, javax.swing.GroupLayout.PREFERRED_SIZE, 1440, javax.swing.GroupLayout.PREFERRED_SIZE)
                );
                layout.setVerticalGroup(
                        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jPanelRoot62, javax.swing.GroupLayout.PREFERRED_SIZE, 720, javax.swing.GroupLayout.PREFERRED_SIZE)
                );

                pack();
                setLocationRelativeTo(null);
        }// </editor-fold>//GEN-END:initComponents

        private void jLabelDangXuat62MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabelDangXuat62MouseClicked
                // TODO add your handling code here:
                MyDialog dlg62 = new MyDialog("Bạn có chắc chắn muốn đăng xuất?", MyDialog.WARNING_DIALOG);
                if (dlg62.OK_OPTION == dlg62.getAction()) {
                        DangNhapJFrame login62 = new DangNhapJFrame();
                        login62.showWindow();
                        this.dispose();
                }
        }//GEN-LAST:event_jLabelDangXuat62MouseClicked

        public void showWindow() {
                Image icon62 = Toolkit.getDefaultToolkit().getImage("src/images/icon-app.png");
                this.setIconImage(icon62);
                this.setVisible(true);
        }

        // Variables declaration - do not modify//GEN-BEGIN:variables
        private javax.swing.JLabel jLabelBanHang62;
        private javax.swing.JLabel jLabelDangXuat62;
        private javax.swing.JLabel jLabelKhachHang62;
        private javax.swing.JLabel jLabelLogo62;
        private javax.swing.JLabel jLabelNhanVien62;
        private javax.swing.JLabel jLabelSanPham62;
        private javax.swing.JLabel jLabelThongKe62;
        private javax.swing.JPanel jPanelBanHang62;
        private javax.swing.JPanel jPanelDangXuat62;
        private javax.swing.JPanel jPanelKhachHang62;
        private javax.swing.JPanel jPanelLOGO62;
        private javax.swing.JPanel jPanelMenu62;
        private javax.swing.JPanel jPanelNhanVien62;
        private javax.swing.JPanel jPanelRoot62;
        private javax.swing.JPanel jPanelSanPham62;
        private javax.swing.JPanel jPanelThongKe62;
        private javax.swing.JPanel jPanelView62;
        // End of variables declaration//GEN-END:variables
}
