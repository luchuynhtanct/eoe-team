package custom;

import javax.swing.*;
import java.awt.*;

public class ImagePanel extends JPanel {

        private Image img62;

        public ImagePanel(String img62) {
                this(new ImageIcon(img62).getImage());
        }

        public ImagePanel(Image img62) {
                this.img62 = img62;
                Dimension size62 = new Dimension(img62.getWidth(null), img62.getHeight(null));
                this.setPreferredSize(size62);
                this.setMinimumSize(size62);
                this.setMaximumSize(size62);
                this.setSize(size62);
                this.setLayout(null);
        }

        @Override
        public void paintComponent(Graphics g) {
                g.drawImage(img62, 0, 0, null);
        }
}
