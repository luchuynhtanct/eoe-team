package custom;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;

public class MyTable {

        public MyTable(JTable tbl62) {
                tbl62.setIntercellSpacing(new Dimension(0, 0));
                JTableHeader header62 = tbl62.getTableHeader();
                header62.setBackground(new Color(242, 153, 74));
                header62.setFont(new Font("Tahoma", Font.BOLD, 14));
                header62.setOpaque(false);
                header62.setForeground(Color.WHITE);
                header62.setReorderingAllowed(false);
                ((DefaultTableCellRenderer) header62.getDefaultRenderer()).setHorizontalAlignment(JLabel.CENTER);
                tbl62.setRowHeight(25);
        }
}
