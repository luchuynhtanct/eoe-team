package custom;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import main.Main;

public class MyDialog extends JDialog {

        private String content62;
        private int type62;
        public final static int ERROR_DIALOG = 1;
        public final static int SUCCESS_DIALOG = 2;
        public final static int INFO_DIALOG = 3;
        public final static int WARNING_DIALOG = 4;

        public MyDialog(String content62, int type62) {
                Main.changLNF("Windows");
                this.content62 = content62;
                this.type62 = type62;
                addControls();
                addEvents();
                showWindow();
        }

        JPanel pnMain62, pnTop62, pnBottom62, pnButton62;
        JLabel lblIcon62, lblContent62, lblClose62;
        JButton btnOK62, btnCancel62;
        final ImageIcon iconError62 = new ImageIcon("src/images/icons8_cancel_70px.png");
        final ImageIcon iconSuccess62 = new ImageIcon("src/images/icons8_checkmark_70px.png");
        final ImageIcon iconInfo62 = new ImageIcon("src/images/icons8_info_70px.png");
        final ImageIcon iconWarning62 = new ImageIcon("src/images/icons8_warning_shield_70px.png");

        private void addControls() {
                Container con = getContentPane();

                pnMain62 = new JPanel();
                pnTop62 = new JPanel();
                pnBottom62 = new JPanel();
                pnButton62 = new JPanel();
                lblIcon62 = new JLabel();
                lblContent62 = new JLabel(content62);
                btnOK62 = new JButton("OK");
                btnCancel62 = new JButton("Cancel");

                pnMain62.setLayout(new BoxLayout(pnMain62, BoxLayout.Y_AXIS));
                pnTop62.setLayout(new FlowLayout());
                pnBottom62.setLayout(new FlowLayout());
                pnButton62.setLayout(new FlowLayout());

                pnMain62.setBackground(Color.WHITE);
                pnTop62.setBackground(Color.WHITE);
                pnBottom62.setBackground(Color.WHITE);
                pnButton62.setBackground(Color.WHITE);

                lblContent62.setFont(new Font("", Font.PLAIN, 18));
                lblContent62.setHorizontalAlignment(JTextField.CENTER);
                lblContent62.setForeground(Color.BLACK);
                lblContent62.setText("<html>"
                        + "<div style='text-align: center; width:300px'>"
                        + content62
                        + "</div></html>");

                btnOK62.setPreferredSize(new Dimension(60, 30));
                btnCancel62.setPreferredSize(btnOK62.getPreferredSize());

                pnTop62.add(lblIcon62, BorderLayout.CENTER);
                pnBottom62.add(lblContent62);
                pnButton62.add(btnOK62);

                JPanel pnHeader62 = new JPanel();
                pnHeader62.setLayout(new FlowLayout(FlowLayout.RIGHT));
                pnHeader62.setPreferredSize(new Dimension(400, 25));
                lblClose62 = new JLabel(new ImageIcon("src/images/icons8_x_24px.png"));
                lblClose62.setCursor(new Cursor(Cursor.HAND_CURSOR));
                pnHeader62.add(lblClose62);

                pnMain62.add(pnHeader62);
                pnMain62.add(pnTop62);
                pnMain62.add(pnBottom62);
                pnMain62.add(pnButton62);

                JPanel pnFooter62 = new JPanel();
                pnFooter62.setPreferredSize(new Dimension(400, 20));
                pnMain62.add(pnFooter62);

                con.add(pnMain62);

                Color backgroundHeader62 = new Color(0);
                switch (type62) {
                        case ERROR_DIALOG:
                                backgroundHeader62 = new Color(220, 53, 69);
                                lblIcon62.setIcon(iconError62);
                                break;
                        case SUCCESS_DIALOG:
                                backgroundHeader62 = new Color(40, 167, 69);
                                lblIcon62.setIcon(iconSuccess62);
                                break;
                        case INFO_DIALOG:
                                backgroundHeader62 = new Color(0, 123, 255);
                                lblIcon62.setIcon(iconInfo62);
                                break;
                        case WARNING_DIALOG:
                                backgroundHeader62 = new Color(255, 193, 7);
                                lblIcon62.setIcon(iconWarning62);
                                pnButton62.add(btnCancel62);
                                break;
                }

                pnMain62.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.WHITE));

                btnOK62.setPreferredSize(new Dimension(80, 30));
                btnCancel62.setPreferredSize(btnOK62.getPreferredSize());
                pnHeader62.setBackground(backgroundHeader62);
        }

        private void addEvents() {
                lblClose62.addMouseListener(new MouseListener() {
                        @Override
                        public void mouseClicked(MouseEvent e) {
                                closeDialog();
                        }

                        @Override
                        public void mousePressed(MouseEvent e) {
                        }

                        @Override
                        public void mouseReleased(MouseEvent e) {
                        }

                        @Override
                        public void mouseEntered(MouseEvent e) {
                        }

                        @Override
                        public void mouseExited(MouseEvent e) {
                        }
                });
                btnOK62.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                                closeDialog();
                                action = OK_OPTION;
                        }
                });
                btnCancel62.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                                closeDialog();
                                action = CANCEL_OPTION;
                        }
                });
                pnMain62.addMouseMotionListener(new MouseMotionListener() {
                        @Override
                        public void mouseDragged(MouseEvent e) {
                                int x = e.getXOnScreen();
                                int y = e.getYOnScreen();
                                Move(x, y);
                        }

                        @Override
                        public void mouseMoved(MouseEvent e) {
                                xMouse = e.getX();
                                yMouse = e.getY();
                        }
                });
        }

        int xMouse, yMouse;

        private void Move(int x, int y) {
                this.setLocation(x - xMouse, y - yMouse);
        }

        private int action;
        public final static int OK_OPTION = 1;
        public final static int CANCEL_OPTION = 2;

        private void closeDialog() {
                this.setVisible(false);
        }

        public int getAction() {
                return action;
        }

        private void showWindow() {
                this.setUndecorated(true);
                this.setSize(400, 250);
                this.setLocationRelativeTo(null);
                this.setAlwaysOnTop(true);
                this.setModal(true);
                this.setBackground(Color.WHITE);
                this.setVisible(true);
                getRootPane().setDefaultButton(btnOK62);
        }
}
