# Project Lập Trình Java Nâng Cao - Quản lý quán ăn
## Thành viên nhóm 6 - EOE:
|STT  |MSV            |Họ và tên       |
|:---:|:-------------:|----------------|
|1    |2050531200262  |[Lê Anh Quốc](https://www.facebook.com/qqpoopqq/)|
|2    |2050531200230  |[Huỳnh Tấn Lực](https://www.facebook.com/luchuynhtanct)|
|3    |2050531200232  |[Trần Đức Mạnh](https://www.facebook.com/profile.php?id=100029490675767)|
|4    |2050531200236  |[Trần Thị Kim Ngân](https://www.facebook.com/ngnn.carrot)|
|5    |2050531200250  |[Hồ Quang Phúc](https://www.facebook.com/quang.phuc.980967)|
### Hướng dẫn:
1) Mở SSMS ./src/database/EOEquanlyquanan.sql và Execute Query vào SQL Server.
2) Thay đổi các thông tin về SQL Server theo máy trong tệp ./src/ConnectVariable.txt lần lượt với 'Tên Máy' \n 'Tài khoản SQL' \n 'Mật khẩu SQL'
3) Nếu ở bản JDK < 17 thì cần Resolve Project Problems để hạ xuống JDK đang sử dụng.
4) Nếu có vấn đề về thư việc thì cũng Resolve Project Problems sau đó Import các thư viện liên quan trong thư mục ./src/lib/
### Phần mềm chạy tốt nhất trên Netbeans và Intellij với JDK16-17 và màn hình với độ phân giải > 1280x720
Mọi lỗi liên quan đến hiển thị là do màn hình máy có độ phân giải màn hình quá thấp.
## Cảm ơn đã đọc!
